package router

import (
	"fmt"
	"runtime/debug"
	"time"

	"admin/controller"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
)

var (
	ApiTimeoutMsg = `{"status": "false","data":"服务器响应超时，请稍后重试"}`
	ApiTimeout    = time.Second * 30
	route         *router.Router
	buildInfo     BuildInfo
)

type BuildInfo struct {
	GitReversion   string
	BuildTime      string
	BuildGoVersion string
}

func apiServerPanic(ctx *fasthttp.RequestCtx, rcv interface{}) {

	err := rcv.(error)
	fmt.Println(err)
	debug.PrintStack()

	if r := recover(); r != nil {
		fmt.Println("recovered failed", r)
	}

	ctx.SetStatusCode(500)
	return
}

func Version(ctx *fasthttp.RequestCtx) {

	ctx.SetContentType("text/html; charset=utf-8")
	fmt.Fprintf(ctx, "reportApi<br />Git reversion = %s<br />Build Time = %s<br />Go version = %s<br />System Time = %s<br />",
		buildInfo.GitReversion, buildInfo.BuildTime, buildInfo.BuildGoVersion, ctx.Time())

	//ctx.Request.Header.VisitAll(func (key, value []byte) {
	//	fmt.Fprintf(ctx, "%s: %s<br/>", string(key), string(value))
	//})
}

// SetupRouter 设置路由列表
func SetupRouter(b BuildInfo) *router.Router {

	route = router.New()
	route.PanicHandler = apiServerPanic

	buildInfo = b

	// app 版本管理
	appUpCtl := new(controller.AppUpgradeController)

	//会员管理
	memberCtl := new(controller.MemberController)
	// 游戏标签
	tagCtl := new(controller.TagsController)
	// 分组管理
	groupCtl := new(controller.GroupController)
	// 权限管理
	privCtl := new(controller.PrivController)
	// 用户管理
	adminCtl := new(controller.AdminController)
	// 会员配置管理
	memberVipCtl := new(controller.MemberVipController)
	// 后台白名单
	whiteListCtl := new(controller.WhiteListController)
	// 签到活动
	promoSignCtl := new(controller.PromoSignController)
	// 宝箱活动
	promoTreasureCtl := new(controller.PromoTreasureController)
	//公告
	noticeCtl := new(controller.NoticeController)
	// 轮播图
	bannerCtl := new(controller.BannerController)
	// 存款管理
	depositCtl := new(controller.DepositController)
	// 提款管理
	wdCtl := new(controller.WithdrawController)
	// 手续费管理
	rateCtl := new(controller.RateController)
	// 场馆列表
	platformCtl := new(controller.PlatformController)
	// 游戏列表
	gameCtl := new(controller.GameController)
	//短信验证码管理
	smsCtl := new(controller.SmsRecordController)
	//支付通道
	pfCtl := new(controller.PayFactoryController)
	// 存款活动
	promoDepsitCtl := new(controller.PromoDepositController)
	// 周投注活动
	promoWeeklyCtl := new(controller.PromoWeeklyConfigController)
	// 打码配置
	flowConfigCtl := new(controller.FlowConfigController)
	// 打码配置
	promoInspectionCtl := new(controller.PromoInspectionController)
	// 邀请配置
	bonusConfigCtl := new(controller.BonusConfigController)
	// 代理渠道配置
	agentChannelConfigCtl := new(controller.ChannelConfigController)
	// 帐变类型
	transTypesCtl := new(controller.TransTypeController)
	// 账变记录
	btransCtl := new(controller.BalanceTransferController)
	//记录管理
	recordCtl := new(controller.RecordController)
	//客服链接管理
	csCtl := new(controller.CsController)
	//报表
	reportCtl := new(controller.ReportController)
	//银行卡
	bankCtl := new(controller.BankCardController)
	//调整
	adjustCtl := new(controller.AdjustController)
	// 邀请配置
	gameConfigCtl := new(controller.GameConfigController)
	// 业务员配置
	proxyInfoCtl := new(controller.ProxyInfoController)
	//转盘管理
	turnTableCtl := new(controller.TurnTableController)

	//日况报表统计，游戏日况统计
	reportDailyCtl := new(controller.ReportDailyController)
	//api费用统计
	reportFeeCtl := new(controller.ReportApiFeeController)

	//rpctest
	rpcTestCtl := new(controller.RpcTestController)

	tenantGroup := route.Group("/tenant")
	merchantGroup := route.Group("/merchant")
	businessGroup := route.Group("/business")

	// 渠道用户登录
	post(merchantGroup, "/login", agentChannelConfigCtl.Login)

	//账变记录
	post(merchantGroup, "/member/balance/transaction", btransCtl.MerchantList)
	// 渠道银行卡-列表
	get(merchantGroup, "/member/bankcard/list", bankCtl.MerchantList)
	// 渠道银行卡-新增
	post(merchantGroup, "/member/bankcard/insert", bankCtl.MerchantInsert)
	// 投注管理-投注记录
	post(merchantGroup, "/game/record", recordCtl.MerchantRecordGame)
	// 宝箱活动记录-列表
	get(merchantGroup, "/treasure/record/list", promoTreasureCtl.MerchantRecordList)
	// 签到活动记录-列表
	get(merchantGroup, "/sign/record/list", promoSignCtl.MerchantRecordList)
	// 周投注活动记录
	get(merchantGroup, "/weekly/record/list", promoWeeklyCtl.MerchantRecordList)
	// 风控管理-打码记录
	get(merchantGroup, "/flow/record/list", promoInspectionCtl.MerchantList)
	// 财务管理-总存款-历史记录
	post(merchantGroup, "/deposit/history", depositCtl.MerchantHistory)
	// 财务管理-提款管理-提款历史记录
	post(merchantGroup, "/withdraw/history/list", wdCtl.MerchantHistoryList)
	// 邀请管理-邀请列表
	get(merchantGroup, "/report/user", reportCtl.MerchantList)
	// 游戏报表
	get(merchantGroup, "/report/game", reportCtl.MerchantGameReport)
	// 平台报表
	get(merchantGroup, "/report/platform", reportCtl.MerchantPlatformList)
	// 数据概览
	get(merchantGroup, "/report/overview", reportCtl.MerchantOverview)

	post(merchantGroup, "/member/getmembertestlist", memberCtl.GetMemberTestList)

	post(merchantGroup, "/member/addmembertest", memberCtl.AddMemberTest)

	post(merchantGroup, "/quotemanage/getoperatorquotelist", memberCtl.GetOperatorQuoteList)

	post(merchantGroup, "/quotemanage/addoperatorquote", memberCtl.AddOperatorQuote)

	// 业务员登录
	post(businessGroup, "/login", proxyInfoCtl.Login)

	//账变记录
	post(businessGroup, "/member/balance/transaction", btransCtl.BusinessList)
	// 业务员银行卡-列表
	get(businessGroup, "/member/bankcard/list", bankCtl.BusinessList)
	// 业务员银行卡-新增
	post(businessGroup, "/member/bankcard/insert", bankCtl.BusinessInsert)
	// 投注管理-投注记录
	post(businessGroup, "/game/record", recordCtl.BusinessRecordGame)
	// 宝箱活动记录-列表
	get(businessGroup, "/treasure/record/list", promoTreasureCtl.BusinessRecordList)
	// 签到活动记录-列表
	get(businessGroup, "/sign/record/list", promoSignCtl.BusinessRecordList)
	// 周投注活动记录
	get(businessGroup, "/weekly/record/list", promoWeeklyCtl.BusinessRecordList)
	// 风控管理-打码记录
	get(businessGroup, "/flow/record/list", promoInspectionCtl.BusinessList)
	// 财务管理-总存款-历史记录
	post(businessGroup, "/deposit/history", depositCtl.BusinessHistory)
	// 财务管理-提款管理-提款历史记录
	post(businessGroup, "/withdraw/history/list", wdCtl.BusinessHistoryList)
	// 邀请管理-邀请列表
	get(businessGroup, "/report/user", reportCtl.BusinessList)
	// 游戏报表
	get(businessGroup, "/report/game", reportCtl.BusinessGameReport)
	// 平台报表
	get(businessGroup, "/report/platform", reportCtl.BusinessPlatformList)
	// 数据概览
	get(businessGroup, "/report/overview", reportCtl.BusinessOverview)

	// 会员管理
	// 后台用户登录
	post(tenantGroup, "/admin/login", adminCtl.Login)
	// 权限管理-后台账号管理-后台账号列表
	get(tenantGroup, "/admin/list", adminCtl.List)
	// 权限管理-后台账号管理-新建账号
	post(tenantGroup, "/admin/insert", adminCtl.Insert)
	// 权限管理-后台账号管理-编辑更新账号
	post(tenantGroup, "/admin/update", adminCtl.Update)
	// 权限管理-后台账号管理-启用
	// 权限管理-后台账号管理-禁用
	get(tenantGroup, "/admin/update/state", adminCtl.UpdateState)
	// 会员列表
	post(tenantGroup, "/member/list", memberCtl.List)
	// 添加账号
	post(tenantGroup, "/member/insert", memberCtl.Insert)
	// 修改会员发放奖金权限
	post(tenantGroup, "/member/update/bonus", memberCtl.UpdateMemberBonus)
	// 修改会员发放返现权限
	post(tenantGroup, "/member/update/rebate", memberCtl.UpdateMemberRebate)
	// 修改会员状态
	post(tenantGroup, "/member/update/state", memberCtl.UpdateMemberState)
	// 修改会员个人输赢打码系数
	post(tenantGroup, "/member/update/flowMultiple", memberCtl.UpdateMemberFlowMultiple)
	// 绑定代理渠道
	post(tenantGroup, "/member/update/agent/channel", memberCtl.UpdateMemberAgentChannel)
	// 手动彩金
	post(tenantGroup, "/member/add/bonus", memberCtl.AddMemberBonus)
	// 是否博主
	post(tenantGroup, "/member/update/isbloger", memberCtl.UpdateMemberBloger)
	post(merchantGroup, "/member/update/isbloger", memberCtl.UpdateMemberBloger)
	post(businessGroup, "/member/update/isbloger", memberCtl.UpdateMemberBloger)
	// 修改密码
	post(tenantGroup, "/member/update/changepwd", memberCtl.UpdateMemberPassword)
	post(merchantGroup, "/member/update/changepwd", memberCtl.UpdateMemberPassword)
	post(businessGroup, "/member/update/changepwd", memberCtl.UpdateMemberPassword)
	//玩家控制
	post(tenantGroup, "/member/controller", memberCtl.MemberController)
	post(merchantGroup, "/member/controller", memberCtl.MemberController)
	post(businessGroup, "/member/controller", memberCtl.MemberController)
	post(tenantGroup, "/member/controller/update", memberCtl.UpdateMemberController)
	post(merchantGroup, "/member/controller/update", memberCtl.UpdateMemberController)
	post(businessGroup, "/member/controller/update", memberCtl.UpdateMemberController)
	//登陆日志
	post(tenantGroup, "/member/loginlog", memberCtl.MemberLoginLog)
	post(merchantGroup, "/member/loginlog", memberCtl.MemberLoginLog)
	post(businessGroup, "/member/loginlog", memberCtl.MemberLoginLog)

	//转盘管理
	// 手动增加拼多多奖励金额
	post(tenantGroup, "/turntable/add/reward", memberCtl.AddRewardAmount)
	// 转盘奖励金额领取审核列表
	get(tenantGroup, "/turntable/list", turnTableCtl.List)
	// 转盘奖励金额领取审核
	post(tenantGroup, "/turntable/review", turnTableCtl.Review)
	// 转盘每笔奖励增加的明细表
	get(tenantGroup, "/turntable/history", turnTableCtl.History)
	// 转盘统计表
	get(tenantGroup, "/turntable/report", turnTableCtl.Report)

	// 上下分申请
	post(tenantGroup, "/member/adjust/apply", adjustCtl.Insert)
	// 上下分记录列表
	get(tenantGroup, "/member/adjust/list", adjustCtl.List)
	// 上下方审核
	post(tenantGroup, "/member/adjust/review", adjustCtl.Review)
	// 帐变类型列表
	get(tenantGroup, "/trans/types", transTypesCtl.List)
	//账变记录
	post(tenantGroup, "/member/balance/transaction", btransCtl.List)
	// 会员等级配置列表
	get(tenantGroup, "/member/vip/list", memberVipCtl.List)
	// 会员等级配置新增
	post(tenantGroup, "/member/vip/insert", memberVipCtl.Insert)
	// 会员等级配置修改
	post(tenantGroup, "/member/vip/update", memberVipCtl.Update)
	// 银行卡-列表
	get(tenantGroup, "/member/bankcard/list", bankCtl.List)
	// 银行卡-新增
	post(tenantGroup, "/member/bankcard/insert", bankCtl.Insert)
	// 银行类型-列表
	get(tenantGroup, "/bank/type/list", bankCtl.BankTypeList)
	// 银行类型-新增
	post(tenantGroup, "/bank/type/insert", bankCtl.InsertBankType)
	// 验证码查询-短信验证码列表
	get(tenantGroup, "/member/sms/list", smsCtl.List)
	// 验证码查询-邮箱验证码列表
	get(tenantGroup, "/member/mail/list", smsCtl.EmailList)
	// 会员配置-游戏配置列表
	get(tenantGroup, "/game/config/list", gameConfigCtl.List)
	// 会员配置-游戏配置更新
	post(tenantGroup, "/game/config/update", gameConfigCtl.Update)

	// 投注管理
	// 投注记录
	post(tenantGroup, "/game/record", recordCtl.RecordGame)

	// 活动管理
	// 宝箱活动配置-列表
	get(tenantGroup, "/treasure/config/list", promoTreasureCtl.ConfigList)
	// 宝箱活动配置-新增
	post(tenantGroup, "/treasure/config/insert", promoTreasureCtl.ConfigInsert)
	// 宝箱活动配置-更新
	post(tenantGroup, "/treasure/config/update", promoTreasureCtl.ConfigUpdate)
	// 宝箱活动记录-列表
	get(tenantGroup, "/treasure/record/list", promoTreasureCtl.RecordList)
	// 签到活动配置-列表
	get(tenantGroup, "/sign/config/list", promoSignCtl.ConfigList)
	// 签到活动配置-新增
	post(tenantGroup, "/sign/config/insert", promoSignCtl.ConfigInsert)
	// 签到活动配置-更新
	post(tenantGroup, "/sign/config/update", promoSignCtl.ConfigUpdate)
	// 签到活动记录-列表
	get(tenantGroup, "/sign/record/list", promoSignCtl.RecordList)
	// 签到活动申请记录-列表
	get(tenantGroup, "/sign/reward/record/list", promoSignCtl.RewardRecordList)
	// 存款活动配置-列表
	get(tenantGroup, "/deposit/config/list", promoDepsitCtl.ConfigList)
	// 存款活动配置-新增
	post(tenantGroup, "/deposit/config/insert", promoDepsitCtl.ConfigInsert)
	// 存款活动配置-更新
	post(tenantGroup, "/deposit/config/update", promoDepsitCtl.ConfigUpdate)
	// 周投注活动配置-列表
	get(tenantGroup, "/weekly/config/list", promoWeeklyCtl.ConfigList)
	// 周投注活动配置-新增
	post(tenantGroup, "/weekly/config/insert", promoWeeklyCtl.ConfigInsert)
	// 周投注活动配置-更新
	post(tenantGroup, "/weekly/config/update", promoWeeklyCtl.ConfigUpdate)
	// 周投注活动记录
	get(tenantGroup, "/weekly/record/list", promoWeeklyCtl.RecordList)

	// 运营管理
	// 运营管理-广告管理-列表
	get(tenantGroup, "/banner/list", bannerCtl.List)
	// 运营管理-广告管理-删除
	get(tenantGroup, "/banner/delete", bannerCtl.Delete)
	// 运营管理-广告管理-更新
	post(tenantGroup, "/banner/update", bannerCtl.Update)
	// 运营管理-广告管理-插入
	post(tenantGroup, "/banner/insert", bannerCtl.Insert)
	// 运营管理-广告管理-上传图片
	post(tenantGroup, "/banner/upload", bannerCtl.Upload)
	// 运营管理-广告管理-更新状态
	get(tenantGroup, "/banner/update/state", bannerCtl.UpdateState)
	// 公告管理
	// 运营管理-公告管理-列表
	get(tenantGroup, "/notice/list", noticeCtl.List)
	// 运营管理-公告管理-删除
	get(tenantGroup, "/notice/delete", noticeCtl.Delete)
	// 运营管理-公告管理-更新
	post(tenantGroup, "/notice/update", noticeCtl.Update)
	// 运营管理-公告管理-新增
	post(tenantGroup, "/notice/insert", noticeCtl.Insert)
	// 运营管理-公告管理-更新状态
	get(tenantGroup, "/notice/update/state", noticeCtl.UpdateState)

	// 风控管理
	//财务 提现风控审核待领取
	post(tenantGroup, "/withdraw/wait/receive", wdCtl.RiskWaitConfirmList)
	//财务风控管理-提款审核-待审核列表
	post(tenantGroup, "/withdraw/wait/review", wdCtl.RiskReviewList)
	//财务风控管理-提款审核-待审核列表-通过
	post(tenantGroup, "/withdraw/review/pass", wdCtl.RiskReview)
	//财务风控管理-提款审核-待审核列表-拒绝
	post(tenantGroup, "/withdraw/review/reject", wdCtl.RiskReviewReject)
	//财务风控管理-提款审核-待审核列表-挂起
	post(tenantGroup, "/withdraw/hangup", wdCtl.HangUp)
	//财务风控管理-提款审核-挂起列表
	post(tenantGroup, "/withdraw/hangup/list", wdCtl.HangUpList)
	//财务风控管理-提款审核-待审核列表-修改领取人
	post(tenantGroup, "/withdraw/receive/update", wdCtl.ConfirmNameUpdate)
	//财务风控管理-提款审核-挂起列表-领取
	post(tenantGroup, "/withdraw/receive", wdCtl.Receive)
	//财务风控管理-提款审核-历史记录列表
	post(tenantGroup, "/withdraw/risk/history", wdCtl.RiskHistory)
	// 打码记录
	// 打码配置-列表
	get(tenantGroup, "/flow/config/list", flowConfigCtl.ConfigList)
	// 打码配置-更新
	post(tenantGroup, "/flow/config/update", flowConfigCtl.ConfigUpdate)
	// 打码记录
	get(tenantGroup, "/flow/record/list", promoInspectionCtl.List)
	// 后台白名单
	// 后台白名单-列表
	get(tenantGroup, "/whitelist/list", whiteListCtl.List)
	// 后台白名单-删除
	get(tenantGroup, "/whitelist/delete", whiteListCtl.Delete)
	// 后台白名单-新增
	post(tenantGroup, "/whitelist/insert", whiteListCtl.Insert)

	// 财务管理
	//提现回调
	post(tenantGroup, "/callback/ew", wdCtl.CallbackEpayW)
	//财务渠道列表
	get(tenantGroup, "/factory/list", pfCtl.List)
	//财务渠道更新
	post(tenantGroup, "/factory/update", pfCtl.Update)
	//财务管理-存款管理-入款订单列表/补单审核列表
	post(tenantGroup, "/deposit/list", depositCtl.List)
	//财务管理-存款管理-历史记录
	post(tenantGroup, "/deposit/history", depositCtl.History)
	//财务管理-存款管理-补单
	post(tenantGroup, "/deposit/update", depositCtl.Update)
	// 财务管理-提款管理-提款列表
	post(tenantGroup, "/withdraw/finance/list", wdCtl.FinanceReviewList)
	// 财务管理-提款管理-提款历史记录
	post(tenantGroup, "/withdraw/history/list", wdCtl.HistoryList)
	// 财务管理-提款管理-拒绝
	post(tenantGroup, "/withdraw/reject", wdCtl.ReviewReject)
	// 财务管理-提款管理-出款
	post(tenantGroup, "/withdraw/review", wdCtl.Review)
	// 财务管理-提款管理-代付失败
	post(tenantGroup, "/withdraw/automatic/failed", wdCtl.AutomaticFailed)
	// 财务管理-手续费管理-列表
	post(tenantGroup, "/rate/list", rateCtl.List)
	// 财务管理-手续费管理-修改
	post(tenantGroup, "/rate/update", rateCtl.Update)

	// 站点管理
	// 站点管理-场馆管理-列表
	get(tenantGroup, "/platform/list", platformCtl.List)
	// 站点管理-场馆开启/关闭 维护/解除维护 修改排序
	post(tenantGroup, "/platform/update", platformCtl.Update)
	// 站点管理-场馆管理-游戏列表-批量修改热门
	get(tenantGroup, "/game/update/batch/hot", gameCtl.UpdateBatchHot)
	// 站点管理-场馆管理-游戏列表-批量修改推荐
	get(tenantGroup, "/game/update/batch/rec", gameCtl.UpdateBatchRec)
	// 站点管理-场馆管理-游戏列表-批量修改收藏
	get(tenantGroup, "/game/update/batch/fav", gameCtl.UpdateBatchFav)
	// 站点管理-场馆管理-游戏列表(搜索)
	get(tenantGroup, "/game/search", gameCtl.Search)
	// 站点管理-场馆管理-游戏列表
	get(tenantGroup, "/game/list", gameCtl.List)
	// 站点管理-场馆管理-游戏列表-编辑游戏
	post(tenantGroup, "/game/update", gameCtl.Update)
	// 站点管理-场馆管理-游戏列表-上传游戏图标
	post(tenantGroup, "/game/upload", gameCtl.Upload)
	// 站点管理-场馆管理-游戏列表-上线下线
	get(tenantGroup, "/game/updatestate", gameCtl.UpdateState)
	// App 升级配置更新
	post(tenantGroup, "/app/update", appUpCtl.Update)
	// App 升级配置列表
	get(tenantGroup, "/app/list", appUpCtl.List)
	// 客服链接-更新
	post(tenantGroup, "/cs/update", csCtl.Update)
	// 客服链接-列表
	get(tenantGroup, "/cs/list", csCtl.List)
	// 游戏标签-新增
	post(tenantGroup, "/tag/insert", tagCtl.Insert)
	// 游戏标签-更新
	post(tenantGroup, "/tag/update", tagCtl.Update)
	// 游戏标签-删除
	get(tenantGroup, "/tag/delete", tagCtl.Delete)
	// 游戏标签-置顶
	get(tenantGroup, "/tag/pin/update", tagCtl.Pin)
	// 游戏标签-删除置顶
	get(tenantGroup, "/tag/pin/delete", tagCtl.UnPin)
	// 游戏标签-列表
	get(tenantGroup, "/tag/list", tagCtl.List)

	// 报表管理
	// 用户报表
	get(tenantGroup, "/report/user", reportCtl.List)
	// 游戏报表
	get(tenantGroup, "/report/game", reportCtl.GameList)
	// 平台报表
	get(tenantGroup, "/report/platform", reportCtl.PlatformList)
	// 数据概览
	get(tenantGroup, "/report/overview", reportCtl.Overview)

	//输赢报表
	get(tenantGroup, "/report/gamewinlost", reportCtl.Overview)

	// 邀请管理
	// 邀请管理-邀请配置列表
	get(tenantGroup, "/bonus/config/list", bonusConfigCtl.ConfigList)
	// 邀请管理-邀请配置更新
	post(tenantGroup, "/bonus/config/update", bonusConfigCtl.ConfigUpdate)
	// 邀请管理-代理渠道配置列表
	get(tenantGroup, "/agent/channel/config/list", agentChannelConfigCtl.ConfigList)
	// 邀请管理-代理渠道配置添加
	post(tenantGroup, "/agent/channel/config/insert", agentChannelConfigCtl.ConfigInsert)
	// 邀请管理-代理渠道配置修改
	post(tenantGroup, "/agent/channel/config/update", agentChannelConfigCtl.ConfigUpdate)
	// 邀请管理-代理渠道api费率更新
	post(tenantGroup, "/agent/channel/apifee/update", agentChannelConfigCtl.ApifeeUpdate)
	// 邀请管理-业务员列表
	get(tenantGroup, "/proxy/info/list", proxyInfoCtl.ConfigList)
	// 邀请管理-业务员添加
	post(tenantGroup, "/proxy/info/insert", proxyInfoCtl.ConfigInsert)
	// 邀请管理-业务员修改
	post(tenantGroup, "/proxy/info/update", proxyInfoCtl.ConfigUpdate)

	// 系统管理
	// 权限管理-用户组管理-新增分组
	post(tenantGroup, "/group/insert", groupCtl.Insert)
	// 权限管理-用户组管理-修改分组
	post(tenantGroup, "/group/update", groupCtl.Update)
	// 权限管理-用户组管理列表
	get(tenantGroup, "/group/list", groupCtl.List)
	// 权限管理-权限列表
	get(tenantGroup, "/priv/list", privCtl.List)

	//玩家日况数据统计
	// 日况列表
	get(tenantGroup, "/member/rpc/query", rpcTestCtl.RpcQuery)
	get(tenantGroup, "/member/rpc", rpcTestCtl.RpcTest)
	// 玩家日况列表
	post(tenantGroup, "/report/userdaily", reportDailyCtl.UserList)   //总后台
	post(merchantGroup, "/report/userdaily", reportDailyCtl.UserList) //渠道后台
	post(businessGroup, "/report/userdaily", reportDailyCtl.UserList) //业务员后台

	//总后台日况,总后台查询渠道，业务员日况
	post(tenantGroup, "/report/platformdaily", reportDailyCtl.PlatformList)

	//渠道后台日况，渠道后台查询业务员日况
	post(merchantGroup, "/report/platformdaily", reportDailyCtl.PlatformList)

	//业务员后台日况
	post(businessGroup, "/report/platformdaily", reportDailyCtl.PlatformList)

	//总后台游戏日况
	post(tenantGroup, "/report/gamedaily", reportDailyCtl.GameList)

	//渠道后台游戏日况
	post(merchantGroup, "/report/gamedaily", reportDailyCtl.GameList)

	//业务员后台游戏日况
	post(businessGroup, "/report/gamedaily", reportDailyCtl.GameList)

	// 日况数据修复
	get(tenantGroup, "/report/fixdailydata", reportDailyCtl.FixDaily)

	//总后台API费用
	post(tenantGroup, "/report/apifee", reportFeeCtl.ApiFee)
	post(tenantGroup, "/report/test", reportFeeCtl.Test)
	//渠道后台API费用
	post(merchantGroup, "/report/apifee", reportFeeCtl.ApiFee)

	//业务员后台API费用
	post(businessGroup, "/report/apifee", reportFeeCtl.ApiFee)

	//总后台代理日况
	post(tenantGroup, "/report/proxydaily", reportDailyCtl.ProxyDailyList)

	//渠道后台代理日况
	post(merchantGroup, "/report/proxydaily", reportDailyCtl.ProxyDailyList)

	//业务员后台代理日况
	post(businessGroup, "/report/proxydaily", reportDailyCtl.ProxyDailyList)

	return route
}

// get is a shortcut for router.GET(path string, handle fasthttp.RequestHandler)
func get(g *router.Group, path string, handle fasthttp.RequestHandler) {
	g.GET(path, fasthttp.TimeoutHandler(handle, ApiTimeout, ApiTimeoutMsg))
}

// post is a shortcut for router.POST(path string, handle fasthttp.RequestHandler)
func post(g *router.Group, path string, handle fasthttp.RequestHandler) {
	g.POST(path, fasthttp.TimeoutHandler(handle, ApiTimeout, ApiTimeoutMsg))
}
