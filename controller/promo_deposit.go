package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"net/url"
)

type PromoDepositController struct{}

// 充值配置新增
func (that PromoDepositController) ConfigInsert(ctx *fasthttp.RequestCtx) {

	param := model.PromoDepositConfig{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	param.Name, _ = url.PathUnescape(param.Name)

	err = model.PromoDepositConfigInsert(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 充值配置列表
func (that PromoDepositController) ConfigList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	id := string(ctx.QueryArgs().Peek("id"))
	ex := g.Ex{}
	if id != "" {
		ex["id"] = id
	}

	s, err := model.PromoDepositConfigList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 充值配置更新
func (that PromoDepositController) ConfigUpdate(ctx *fasthttp.RequestCtx) {

	param := model.PromoDepositConfig{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	param.Name, _ = url.PathUnescape(param.Name)

	err = model.PromoDepositConfigUpdate(param.Id, param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
