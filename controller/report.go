package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	"fmt"
	"github.com/valyala/fasthttp"
	"net/url"
)

type ReportController struct{}

// Member 报表中心-会员报表
func (that ReportController) List(ctx *fasthttp.RequestCtx) {

	uid := string(ctx.QueryArgs().Peek("uid"))               // 用户id
	userName := string(ctx.QueryArgs().Peek("username"))     // 会员帐号
	sStartTime := string(ctx.QueryArgs().Peek("start_time")) // 开始时间
	sEndTime := string(ctx.QueryArgs().Peek("end_time"))     // 结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")            //页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")   //一页多少条
	fmt.Println("start_time:", sStartTime)
	fmt.Println("end_time:", sEndTime)
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if pageSize < 10 || pageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	userName, _ = url.QueryUnescape(userName)

	data, err := model.UserReport(page, pageSize, uid, userName, sStartTime, sEndTime)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, data)

}

// MerchantList 邀请管理-邀请列表
func (that ReportController) MerchantList(ctx *fasthttp.RequestCtx) {
	uid := string(ctx.QueryArgs().Peek("uid"))               // 用户id
	userName := string(ctx.QueryArgs().Peek("username"))     // 会员帐号
	sStartTime := string(ctx.QueryArgs().Peek("start_time")) // 开始时间
	sEndTime := string(ctx.QueryArgs().Peek("end_time"))     // 结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")            //页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")   //一页多少条
	operatorId := string(ctx.QueryArgs().Peek("operator_id"))
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if pageSize < 10 || pageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if operatorId == "" {
		helper.Print(ctx, false, helper.OperatorIdErr)
		return
	}

	userName, _ = url.QueryUnescape(userName)

	data, err := model.MerchantUserReport(page, pageSize, operatorId, uid, userName, sStartTime, sEndTime)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, data)

}

// BusinessList 邀请管理-邀请列表
func (that ReportController) BusinessList(ctx *fasthttp.RequestCtx) {
	uid := string(ctx.QueryArgs().Peek("uid"))               // 用户id
	userName := string(ctx.QueryArgs().Peek("username"))     // 会员帐号
	sStartTime := string(ctx.QueryArgs().Peek("start_time")) // 开始时间
	sEndTime := string(ctx.QueryArgs().Peek("end_time"))     // 结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")            //页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")   //一页多少条
	businessId := string(ctx.QueryArgs().Peek("business_id"))
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if pageSize < 10 || pageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if businessId == "" {
		helper.Print(ctx, false, helper.BusinessIdErr)
		return
	}

	userName, _ = url.QueryUnescape(userName)

	data, err := model.BusinessUserReport(page, pageSize, businessId, uid, userName, sStartTime, sEndTime)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, data)

}

// GameList 游戏报表
func (that *ReportController) GameList(ctx *fasthttp.RequestCtx) {

	ids := string(ctx.QueryArgs().Peek("ids"))              //游戏id,多个用逗号分隔
	startDate := string(ctx.QueryArgs().Peek("start_time")) //开始时间
	endDate := string(ctx.QueryArgs().Peek("end_time"))     //结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")           //页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")  //一页多少条
	if !validator.CheckStringCommaDigit(ids) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	fmt.Println("start_time:", startDate)
	fmt.Println("end_time:", endDate)
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if pageSize < 10 || pageSize > 200 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	data, err := model.GameReport(1, startDate, endDate, ids, page, pageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// 平台报表
func (that *ReportController) PlatformList(ctx *fasthttp.RequestCtx) {

	startDate := string(ctx.QueryArgs().Peek("start_time")) //开始时间
	endDate := string(ctx.QueryArgs().Peek("end_time"))     //结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")           //页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")  //一页多少条

	fmt.Println("start_time:", startDate)
	fmt.Println("end_time:", endDate)
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if pageSize < 10 || pageSize > 200 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	data, err := model.PlatformReport(page, pageSize, startDate, endDate)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *ReportController) Overview(ctx *fasthttp.RequestCtx) {

	startDate := string(ctx.QueryArgs().Peek("start_time")) //开始时间
	endDate := string(ctx.QueryArgs().Peek("end_time"))     //结束时间
	data, err := model.PlatformOverview(startDate, endDate)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// MerchantGameReport 渠道游戏报表
func (that *ReportController) MerchantGameReport(ctx *fasthttp.RequestCtx) {

	ids := string(ctx.QueryArgs().Peek("ids"))                //游戏id,多个用逗号分隔
	startDate := string(ctx.QueryArgs().Peek("start_time"))   //开始时间
	endDate := string(ctx.QueryArgs().Peek("end_time"))       //结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")             //页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")    //一页多少条
	operatorId := string(ctx.QueryArgs().Peek("operator_id")) //渠道id
	if !validator.CheckStringCommaDigit(ids) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	fmt.Println("start_time:", startDate)
	fmt.Println("end_time:", endDate)
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if pageSize < 10 || pageSize > 200 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	data, err := model.MerchantGameReport(operatorId, startDate, endDate, ids, page, pageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// MerchantPlatformList 渠道平台报表
func (that *ReportController) MerchantPlatformList(ctx *fasthttp.RequestCtx) {

	startDate := string(ctx.QueryArgs().Peek("start_time"))   //开始时间
	endDate := string(ctx.QueryArgs().Peek("end_time"))       //结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")             //页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")    //一页多少条
	operatorId := string(ctx.QueryArgs().Peek("operator_id")) //渠道id

	fmt.Println("start_time:", startDate)
	fmt.Println("end_time:", endDate)
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if pageSize < 10 || pageSize > 200 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	data, err := model.MerchantPlatformReport(page, pageSize, startDate, endDate, operatorId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// MerchantOverview 渠道统计面板
func (that *ReportController) MerchantOverview(ctx *fasthttp.RequestCtx) {

	startDate := string(ctx.QueryArgs().Peek("start_time"))   //开始时间
	endDate := string(ctx.QueryArgs().Peek("end_time"))       //结束时间
	operatorId := string(ctx.QueryArgs().Peek("operator_id")) //渠道id
	data, err := model.MerchantPlatformOverview(startDate, endDate, operatorId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// BusinessGameReport 业务员游戏报表
func (that *ReportController) BusinessGameReport(ctx *fasthttp.RequestCtx) {

	ids := string(ctx.QueryArgs().Peek("ids"))                //游戏id,多个用逗号分隔
	startDate := string(ctx.QueryArgs().Peek("start_time"))   //开始时间
	endDate := string(ctx.QueryArgs().Peek("end_time"))       //结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")             //页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")    //一页多少条
	businessId := string(ctx.QueryArgs().Peek("business_id")) //业务员id
	if !validator.CheckStringCommaDigit(ids) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	fmt.Println("start_time:", startDate)
	fmt.Println("end_time:", endDate)
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if pageSize < 10 || pageSize > 200 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	data, err := model.BusinessGameReport(businessId, startDate, endDate, ids, page, pageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// BusinessPlatformList 业务员平台报表
func (that *ReportController) BusinessPlatformList(ctx *fasthttp.RequestCtx) {

	startDate := string(ctx.QueryArgs().Peek("start_time"))   //开始时间
	endDate := string(ctx.QueryArgs().Peek("end_time"))       //结束时间
	page := ctx.QueryArgs().GetUintOrZero("page")             //页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")    //一页多少条
	businessId := string(ctx.QueryArgs().Peek("business_id")) //业务员id

	fmt.Println("start_time:", startDate)
	fmt.Println("end_time:", endDate)
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if pageSize < 10 || pageSize > 200 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	data, err := model.BusinessPlatformReport(page, pageSize, startDate, endDate, businessId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// BusinessOverview 业务员统计面板
func (that *ReportController) BusinessOverview(ctx *fasthttp.RequestCtx) {

	startDate := string(ctx.QueryArgs().Peek("start_time"))   //开始时间
	endDate := string(ctx.QueryArgs().Peek("end_time"))       //结束时间
	businessId := string(ctx.QueryArgs().Peek("business_id")) //业务员id
	data, err := model.BusinessPlatformOverview(startDate, endDate, businessId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}
