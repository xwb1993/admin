package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
)

type PromoWeeklyConfigController struct{}

// 周投注活动配置新增
func (that PromoWeeklyConfigController) ConfigInsert(ctx *fasthttp.RequestCtx) {

	param := model.TblPromoWeekBetConfig{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}
	param.UpdatedName = admin["name"]
	param.UpdatedAt = ctx.Time().Unix()
	err = model.PromoWeeklyConfigInsert(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 周投注活动配置列表
func (that PromoWeeklyConfigController) ConfigList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	id := string(ctx.QueryArgs().Peek("id"))
	ex := g.Ex{}
	if id != "" {
		ex["id"] = id
	}

	s, err := model.PromoWeeklyConfigList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 周投注活动配置更新
func (that PromoWeeklyConfigController) ConfigUpdate(ctx *fasthttp.RequestCtx) {

	param := model.TblPromoWeekBetConfig{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}
	param.UpdatedName = admin["name"]
	param.UpdatedAt = ctx.Time().Unix()
	err = model.PromoWeeklyConfigUpdate(param.Id, param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 周投注活动记录
func (that PromoWeeklyConfigController) RecordList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	uid := string(ctx.QueryArgs().Peek("uid"))
	ex := g.Ex{}
	if uid != "" {
		ex["uid"] = uid
	}

	s, err := model.PromoWeekbetRecordList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 渠道周投注活动记录
func (that PromoWeeklyConfigController) MerchantRecordList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	uid := string(ctx.QueryArgs().Peek("uid"))
	operatorId := string(ctx.QueryArgs().Peek("operator_id"))

	if operatorId == "" {
		helper.Print(ctx, false, helper.OperatorIdErr)
		return
	}

	s, err := model.MerchantPromoWeekbetRecordList(uint(page), uint(pageSize), uid, operatorId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 业务员周投注活动记录
func (that PromoWeeklyConfigController) BusinessRecordList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	uid := string(ctx.QueryArgs().Peek("uid"))
	businessId := string(ctx.QueryArgs().Peek("business_id")) // 渠道id

	if businessId == "" {
		helper.Print(ctx, false, helper.BusinessIdErr)
		return
	}

	s, err := model.BusinessPromoWeekbetRecordList(uint(page), uint(pageSize), uid, businessId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}
