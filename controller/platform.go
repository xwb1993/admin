package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
)

type PlatformController struct{}

// List 场馆列表
func (that *PlatformController) List(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	state := ctx.QueryArgs().GetUintOrZero("state")
	maintained := ctx.QueryArgs().GetUintOrZero("maintained")
	gameType := ctx.QueryArgs().GetUintOrZero("game_type")
	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")

	if page < 1 {
		page = 1
	}
	if pageSize < 10 || pageSize > 100 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	ex := g.Ex{}
	if id != "" {
		ex["id"] = id
	} else {
		if state > 0 {
			if state > 2 {
				helper.Print(ctx, false, helper.StateParamErr)
				return
			}

			ex["state"] = state
		}

		if maintained > 0 {
			if maintained > 2 {
				helper.Print(ctx, false, helper.StateParamErr)
				return
			}

			ex["maintained"] = maintained
		}

		if gameType > 0 {
			if gameType > 10 {
				helper.Print(ctx, false, helper.GameTypeErr)
				return
			}

			ex["game_type"] = gameType
		}
	}

	data, err := model.PlatformList(ex, page, pageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

type platformUpdate struct {
	ID         string `json:"id"`
	State      int    `json:"state"`
	Maintained int    `json:"maintained"`
	Seq        int    `json:"seq"`
}

// Update 场馆更新
func (that *PlatformController) Update(ctx *fasthttp.RequestCtx) {

	param := platformUpdate{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	err = model.PlatformUpdate(admin["id"], admin["name"], param.ID, param.State, param.Maintained, param.Seq)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
