package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	myredis "common/redis"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"strings"
	"unicode/utf8"
)

type MemberController struct{}

type memberListParam struct {
	Uid        string `json:"uid" cbor:"uid"`
	Username   string `json:"username" cbor:"username"`
	Phone      string `json:"phone" cbor:"phone"`
	Email      string `json:"email" cbor:"email"`
	Agent      string `json:"agent" cbor:"agent"`
	StartTime  string `json:"startTime" cbor:"start_time"`
	EndTime    string `json:"endTime" cbor:"end_time"`
	Page       int    `json:"page" cbor:"page"`
	PageSize   int    `json:"pageSize" cbor:"page_size"`
	Tester     string `json:"tester" cbor:"tester"`
	OperatorId string `json:"operator_id" cbor:"operator_id"`
}

type insertMemberParam struct {
	Username   string `json:"username"`    //注册账号
	ParentId   string `json:"parent_id"`   //上級 id
	Password   string `json:"password"`    //登录密碼
	Tester     int    `json:"tester"`      //1 正式账号 2 模拟账号 3代理
	OperatorId string `json:"operator_id"` //渠道id
	BusinessId string `json:"business_id"` //业务员id
}

type memberTestListParam struct {
	Uid        int    `json:"uid" cbor:"uid"`
	StartTime  string `json:"startTime" cbor:"start_time"`
	EndTime    string `json:"endTime" cbor:"end_time"`
	Page       int    `json:"page" cbor:"page"`
	PageSize   int    `json:"pageSize" cbor:"page_size"`
	OperatorId int    `json:"operator_id" cbor:"operator_id"`
}

type membetTestAddParam struct {
	Total      int     `json:"total" cbor:"total"`
	Amount     float64 `json:"Amount" cbor:"Amount"`
	OperatorId int     `json:"OperatorId" cbor:"OperatorId"`
}

func (that *MemberController) List(ctx *fasthttp.RequestCtx) {

	param := memberListParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Page < 1 {
		param.Page = 1
	}

	if param.PageSize < 10 || param.PageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	ex := g.Ex{}
	if param.Tester == "1" {
		ex["tm.tester"] = 1
	} else if param.Tester == "2" {
		ex["tm.tester"] = 2
	}

	if param.Uid != "" {
		ex["tm.uid"] = param.Uid
	}

	if param.OperatorId != "" {
		ex["tm.operator_id"] = param.OperatorId
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	if param.Username != "" {

		// 多个会员名用,分隔
		param.Username = strings.ToLower(param.Username)
		sName := strings.Split(param.Username, ",")
		var usernames []string
		for _, name := range sName {
			if !validator.CheckUName(name, 5, 14) {
				helper.Print(ctx, false, helper.UsernameErr)
				return
			}

			usernames = append(usernames, name)
		}

		if len(usernames) > 200 {
			ex["tm.username"] = usernames[:200]
		} else {
			ex["tm.username"] = usernames
		}

		data, err := model.MemberList(param.Page, param.PageSize, "", "", param.Agent, admin["id"], ex)
		if err != nil {
			helper.Print(ctx, false, err.Error())
			return
		}
		for key, value := range data.D {
			//redis 取余额
			(data.D)[key].Brl = myredis.GetUserFieldFloat64(value.Uid, "tbl_member_balance:brl")
		}
		helper.Print(ctx, true, data)
		return
	}

	if param.Agent != "" {
		ex["tm.parent_name"] = param.Agent
	}

	if param.Phone != "" {
		ex["tm.phone"] = param.Phone
	}

	if param.Email != "" {
		if !strings.Contains(param.Email, "@") {
			helper.Print(ctx, false, helper.EmailFMTErr)
			return
		}

		ex["tm.email"] = param.Email
	}

	data, err := model.MemberList(param.Page, param.PageSize, param.StartTime, param.EndTime, param.Agent, admin["id"], ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	for key, value := range data.D {
		//redis 取余额
		(data.D)[key].Brl = myredis.GetUserFieldFloat64(value.Uid, "tbl_member_balance:brl")
	}
	helper.Print(ctx, true, data)
}

// 会员配置新增
func (that MemberController) Insert(ctx *fasthttp.RequestCtx) {

	param := insertMemberParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if !validator.CtypeDigit(param.Username) && !strings.Contains(param.Username, "@") {
		helper.Print(ctx, false, helper.UsernameErr)
		return
	}
	if param.ParentId != "" && !validator.CtypeDigit(param.ParentId) {
		helper.Print(ctx, false, helper.ParentAgencyCanNotUse)
		return
	}

	if param.Tester < 0 || param.Tester > 4 {
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	prefix := string(ctx.Request.Header.Peek("X-Prefix"))
	err = model.MemberInsert(param.Username, param.Password, param.ParentId, param.OperatorId, param.BusinessId, param.Tester, prefix)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type updateMemberBonusParam struct {
	Uid      string `json:"uid"`
	CanBonus int    `json:"can_bonus"`
}

func (that MemberController) UpdateMemberBonus(ctx *fasthttp.RequestCtx) {

	param := updateMemberBonusParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CtypeDigit(param.Uid) {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}
	if param.CanBonus != 1 && param.CanBonus != 2 {
		helper.Print(ctx, false, helper.StateParamErr)
		return
	}

	err = model.MemberUpdateBonus(param.Uid, param.CanBonus)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type updateMemberBlogerParam struct {
	Uid      string `json:"uid"`
	IsBloger int    `json:"is_bloger"`
}

func (that MemberController) UpdateMemberBloger(ctx *fasthttp.RequestCtx) {

	param := updateMemberBlogerParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CtypeDigit(param.Uid) {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}
	if param.IsBloger != 1 && param.IsBloger != 0 {
		helper.Print(ctx, false, helper.StateParamErr)
		return
	}

	err = model.MemberUpdateBloger(param.Uid, param.IsBloger)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type updateMemberPasswordParam struct {
	Uid      string `json:"uid"`
	Password string `json:"password"`
}

func (that MemberController) UpdateMemberPassword(ctx *fasthttp.RequestCtx) {

	param := updateMemberPasswordParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CtypeDigit(param.Uid) {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}

	err = model.MemberUpdatePassword(param.Uid, param.Password)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type updateMemberRebateParam struct {
	Uid       string `json:"uid"`
	CanRebate int    `json:"can_rebate"`
}

func (that MemberController) UpdateMemberRebate(ctx *fasthttp.RequestCtx) {

	param := updateMemberRebateParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CtypeDigit(param.Uid) {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}
	if param.CanRebate != 1 && param.CanRebate != 2 {
		helper.Print(ctx, false, helper.StateParamErr)
		return
	}

	err = model.MemberUpdateRebate(param.Uid, param.CanRebate)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type updateMemberStateParam struct {
	Uid    string `json:"uid"`
	State  int    `json:"state"`
	Remake string `json:"remake"`
}

func (that MemberController) UpdateMemberState(ctx *fasthttp.RequestCtx) {

	param := updateMemberStateParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CtypeDigit(param.Uid) {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}
	if param.State > 4 || param.State < 0 {
		helper.Print(ctx, false, helper.StateParamErr)
		return
	}
	if utf8.RuneCountInString(param.Remake) > 100 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	err = model.MemberUpdateState(param.Uid, param.State, param.Remake)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type updateMemberAgentChannelParam struct {
	Uid string `json:"uid"`
	Cid string `json:"cid"`
}

func (that MemberController) UpdateMemberAgentChannel(ctx *fasthttp.RequestCtx) {

	param := updateMemberAgentChannelParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CtypeDigit(param.Uid) {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}

	err = model.MemberUpdateAgentChannel(param.Uid, param.Cid)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type UpdateMemberFlowMultipleParam struct {
	Uid              string `json:"uid"`
	WinFlowMultiple  string `json:"win_flow_multiple"`
	LoseFlowMultiple string `json:"lose_flow_multiple"`
}

func (that MemberController) UpdateMemberFlowMultiple(ctx *fasthttp.RequestCtx) {

	param := UpdateMemberFlowMultipleParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CtypeDigit(param.Uid) {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}

	err = model.MemberUpdateFlowMultiple(param.Uid, param.WinFlowMultiple, param.LoseFlowMultiple)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type addMemberBonusParam struct {
	Uid      string `json:"uid"`
	Username string `json:"username"`
	Amount   string `json:"amount"`
}

func (that MemberController) AddMemberBonus(ctx *fasthttp.RequestCtx) {
	param := addMemberBonusParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CtypeDigit(param.Uid) {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}

	err = model.MemberAddBonus(param.Uid, param.Username, param.Amount)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type addRewardAmount struct {
	Uid    string  `json:"uid"`
	Amount float64 `json:"amount"`
}

func (that MemberController) AddRewardAmount(ctx *fasthttp.RequestCtx) {
	param := addRewardAmount{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if !validator.CtypeDigit(param.Uid) {
		helper.Print(ctx, false, helper.UIDErr)
		return
	}

	err = model.MemberAddReward(param.Uid, param.Amount)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 获取测试号列表
func (that MemberController) GetMemberTestList(ctx *fasthttp.RequestCtx) {
	param := memberTestListParam{}
	fmt.Println(param)
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Page < 1 {
		param.Page = 1
	}

	if param.PageSize < 10 || param.PageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	ex := g.Ex{}

	if param.Uid > 0 {
		ex["uid"] = param.Uid
	}

	if param.OperatorId > 0 {
		ex["operator_id"] = param.OperatorId
	}

	data, err := model.MemberTestList(param.Page, param.PageSize, param.StartTime, param.EndTime, admin["id"], ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that MemberController) AddMemberTest(ctx *fasthttp.RequestCtx) {
	param := membetTestAddParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Total == 0 {
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	err = model.GetMembetTestMaxId(param.Amount, param.Total, param.OperatorId)
	if err != nil {
		helper.Print(ctx, false, helper.DBErr)
		return
	}
	helper.Print(ctx, true, nil)
}

func (that MemberController) MemberController(ctx *fasthttp.RequestCtx) {
	param := model.MemberControllerButton{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	s, err := model.GetMemberController(param.Uid, admin["id"])
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, s)
}

func (that MemberController) UpdateMemberController(ctx *fasthttp.RequestCtx) {
	param := model.MemberControllerButton{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	err = model.UpdateMemberController(param, admin["id"])
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, "")
}

// 登录日志
func (that MemberController) MemberLoginLog(ctx *fasthttp.RequestCtx) {
	param := model.MemberLoginLogParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	s, err := model.MemberLoginLog(param, admin["id"])
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, s)
}
