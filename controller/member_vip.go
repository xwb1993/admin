package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"strconv"
)

type MemberVipController struct{}

type vipParam struct {
	Vip                  int     `json:"vip"`
	Name                 string  `json:"name"`
	DepositAmount        int64   `json:"deposit_amount"`
	Flow                 int64   `json:"flow"`
	KeepFlow             int64   `json:"keep_flow"`
	Amount               string  `json:"amount"`                                               //升级奖励金额
	FreeWithdrawNum      int     `json:"free_withdraw_num"`                                    //免费提款次数
	WithdrawLimit        int     `json:"withdraw_limit"`                                       //提款限额
	RebateRate           string  `json:"rebate_rate"`                                          //返水比例
	Props                int     `json:"props"`                                                //道具
	WeekAward            float64 `json:"week_award" db:"week_award"`                           //周奖励金额
	MonthAward           float64 `json:"month_award" db:"month_award"`                         //月奖励金额
	WithdrawRate         float64 `json:"withdraw_rate" db:"withdraw_rate"`                     //提现手续费
	WithdrawSingleLimit  int     `json:"withdraw_single_limit" db:"withdraw_single_limit"`     //提现单笔限额
	ChargeExtraAwardRate float64 `json:"charge_extra_award_rate" db:"charge_extra_award_rate"` //每笔充值赠送
}

// 会员配置新增
func (that MemberVipController) Insert(ctx *fasthttp.RequestCtx) {

	param := vipParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	data := model.MemberVip{
		Vip:                  param.Vip,
		Name:                 param.Name,
		DepositAmount:        param.DepositAmount,
		Flow:                 param.Flow,
		KeepFlow:             param.KeepFlow,
		Amount:               param.Amount,
		WeekAward:            param.WeekAward,
		MonthAward:           param.MonthAward,
		WithdrawRate:         param.WithdrawRate,
		FreeWithdrawNum:      param.FreeWithdrawNum,
		WithdrawSingleLimit:  param.WithdrawSingleLimit,
		WithdrawLimit:        param.WithdrawLimit,
		ChargeExtraAwardRate: param.ChargeExtraAwardRate,
		RebateRate:           param.RebateRate,
		Props:                param.Props,
		CreatedAt:            ctx.Time().Unix(),
		UpdatedAt:            0,
	}
	err = model.MemberVipInsert(data)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 会员配置列表
func (that MemberVipController) List(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	sVip := string(ctx.QueryArgs().Peek("vip"))
	ex := g.Ex{}
	if sVip != "" {
		vip, err := strconv.Atoi(sVip)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}

		ex["vip"] = vip
	}

	s, err := model.MemberVipList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 会员配置更新
func (that MemberVipController) Update(ctx *fasthttp.RequestCtx) {

	param := model.MemberVip{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	err = model.MemberVipValidate(param.Vip, param.DepositAmount, param.Flow)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	//record := g.Record{}
	//if param.DepositAmount != 0 {
	//	record["deposit_amount"] = param.DepositAmount
	//}
	//if param.Flow != 0 {
	//	record["flow"] = param.Flow
	//}
	//if param.Flow != 0 {
	//	record["keep_flow"] = param.KeepFlow
	//}
	//if param.Name != "" {
	//	record["name"] = param.Name
	//}
	//if param.Amount != "" {
	//	if _, err := decimal.NewFromString(param.Amount); err != nil {
	//		helper.Print(ctx, false, helper.ParamErr)
	//		return
	//	}
	//
	//	record["amount"] = param.Amount
	//}
	//if param.FreeWithdrawNum != 0 {
	//	record["free_withdraw_num"] = param.FreeWithdrawNum
	//}
	//if param.WithdrawLimit != 0 {
	//	record["withdraw_limit"] = param.WithdrawLimit
	//}
	//if param.RebateRate != "" {
	//	rebateRate, err := decimal.NewFromString(param.RebateRate)
	//	if err != nil {
	//		helper.Print(ctx, false, helper.ParamErr)
	//		return
	//	}
	//
	//	if rebateRate.GreaterThanOrEqual(decimal.NewFromInt(100)) ||
	//		decimal.NewFromInt(0).GreaterThan(rebateRate) {
	//		helper.Print(ctx, false, helper.ParamErr)
	//		return
	//	}
	//
	//	record["rebate_rate"] = param.RebateRate
	//}
	//if param.Props != 0 {
	//	record["props"] = param.Props
	//}
	//if param.WithdrawSingleLimit != 0 {
	//	record["withdraw_single_limit"] = param.WithdrawSingleLimit
	//}
	//if param.WeekAward != 0 {
	//	record["week_award"] = param.WeekAward
	//}
	//if param.MonthAward != 0 {
	//	record["month_award"] = param.MonthAward
	//}
	//if param.ChargeExtraAwardRate != 0 {
	//	record["charge_extra_award_rate"] = param.ChargeExtraAwardRate
	//}
	//
	//if len(record) == 0 {
	//	helper.Print(ctx, false, helper.NoDataUpdate)
	//	return
	//}

	err = model.MemberVipUpdate(param.Vip, param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
