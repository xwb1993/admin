package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
)

type ChannelConfigController struct{}

type ApiFee struct {
	GameId   string  `json:"game_id" db:"game_id"`
	GameName string  `json:"game_name" db:"game_name"`
	Rate     float64 `json:"rate" db:"rate"`
}

// ConfigList 代理渠道配置列表
func (that ChannelConfigController) ConfigList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	id := string(ctx.QueryArgs().Peek("id"))
	operatorName := string(ctx.QueryArgs().Peek("operator_name"))
	state := string(ctx.QueryArgs().Peek("state"))

	ex := g.Ex{}
	if id != "" {
		ex["id"] = id
	}
	if operatorName != "" {
		ex["operator_name"] = operatorName
	}
	if state != "" {
		ex["state"] = state
	}
	s, err := model.ChannelConfigList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	//获取三方游戏列表
	platform, err := model.PlatformList(nil, 1, 999)
	platformList := map[string]model.Platform{}
	for _, value := range platform.D {
		platformList[value.ID] = value
	}
	for key, value := range s.D {
		apiFeeRate := map[string]float64{}
		json.Unmarshal([]byte(value.ApiFee), &apiFeeRate)
		fmt.Println(apiFeeRate)
		newApiFee := []ApiFee{}
		for _, v := range platform.D {
			newApiFee = append(newApiFee, ApiFee{
				GameId:   v.ID,
				GameName: v.Name,
				Rate:     apiFeeRate[v.ID],
			})
		}
		jsonBytes, _ := json.Marshal(newApiFee)
		(s.D)[key].ApiFee = string(jsonBytes)
	}
	helper.Print(ctx, true, s)
}

// ConfigInsert 代理渠道配置新增
func (that *ChannelConfigController) ConfigInsert(ctx *fasthttp.RequestCtx) {

	param := model.TblOperatorInfo{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	err = model.ChannelConfigInsert(admin["name"], admin["id"], param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, "succeed")
}

// ConfigUpdate 代理渠道配置更新
func (that ChannelConfigController) ConfigUpdate(ctx *fasthttp.RequestCtx) {

	param := model.TblOperatorInfo{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	err = model.ChannelConfigUpdate(admin["name"], admin["id"], param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// ApifeeUpdate 代理渠道api费率更新
func (that ChannelConfigController) ApifeeUpdate(ctx *fasthttp.RequestCtx) {

	param := model.TblOperatorInfo{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}
	fmt.Println(param.ApiFee)
	newFeeRate := []ApiFee{}
	json.Unmarshal([]byte(param.ApiFee), &newFeeRate)
	apiFeeRate := map[string]float64{}
	for _, v := range newFeeRate {
		apiFeeRate[v.GameId] = v.Rate

	}
	jsonBytes, _ := json.Marshal(apiFeeRate)
	param.ApiFee = string(jsonBytes)
	err = model.ChannelApifeeUpdate(admin["name"], admin["id"], param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type merchantLogin struct {
	OperatorName string `json:"operator_name" cbor:"operator_name"`
	Password     string `json:"password" cbor:"password"`
}

func (that *ChannelConfigController) Login(ctx *fasthttp.RequestCtx) {
	deviceNo := string(ctx.Request.Header.Peek("no"))
	param := merchantLogin{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	resp, err := model.MerchantLogin(deviceNo, param.OperatorName, param.Password)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, resp)
}
