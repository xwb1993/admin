package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"net/url"
)

type PromoTreasureController struct{}

// 会员配置新增
func (that PromoTreasureController) ConfigInsert(ctx *fasthttp.RequestCtx) {

	param := model.PromoTreasureConfig{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	err = model.PromoTreasureConfigInsert(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 会员配置列表
func (that PromoTreasureController) ConfigList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")

	s, err := model.PromoTreasureConfigList(uint(page), uint(pageSize))
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 会员配置更新
func (that PromoTreasureController) ConfigUpdate(ctx *fasthttp.RequestCtx) {

	param := model.PromoTreasureConfig{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	err = model.PromoTreasureConfigUpdate(param.ID, param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 会员配置列表
func (that PromoTreasureController) RecordList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	username := string(ctx.QueryArgs().Peek("username"))

	ex := g.Ex{}

	username, _ = url.QueryUnescape(username)
	if username != "" {
		if !validator.CheckUName(username, 5, 50) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}

		ex["username"] = username
	}

	s, err := model.PromoTreasureRecordList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 渠道宝箱活动-活动记录列表
func (that PromoTreasureController) MerchantRecordList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	username := string(ctx.QueryArgs().Peek("username"))
	operatorId := string(ctx.QueryArgs().Peek("operator_id")) // 渠道id

	if operatorId == "" {
		helper.Print(ctx, false, helper.OperatorIdErr)
		return
	}

	s, err := model.MerchantPromoTreasureRecordList(uint(page), uint(pageSize), username, operatorId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 业务员宝箱活动-活动记录列表
func (that PromoTreasureController) BusinessRecordList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	username := string(ctx.QueryArgs().Peek("username"))
	businessId := string(ctx.QueryArgs().Peek("business_id")) // 渠道id

	if businessId == "" {
		helper.Print(ctx, false, helper.BusinessIdErr)
		return
	}

	s, err := model.BusinessPromoTreasureRecordList(uint(page), uint(pageSize), username, businessId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}
