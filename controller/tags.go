package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"github.com/valyala/fasthttp"
	"net/url"
	"strings"
)

type TagsController struct{}

func (that *TagsController) UnPin(ctx *fasthttp.RequestCtx) {

	gameId := string(ctx.QueryArgs().Peek("game_id"))
	if !helper.CtypeDigit(gameId) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}
	tagId := string(ctx.QueryArgs().Peek("tag_id"))
	if !helper.CtypeDigit(tagId) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	err := model.TagUnPin(tagId, gameId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *TagsController) Pin(ctx *fasthttp.RequestCtx) {

	gameId := string(ctx.QueryArgs().Peek("gameid"))
	tagId := string(ctx.QueryArgs().Peek("tagid"))

	if !helper.CtypeCommaDigit(tagId) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}
	if !helper.CtypeCommaDigit(tagId) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	gameIds := strings.SplitN(gameId, ",", 50)
	tagIds := strings.SplitN(tagId, ",", 50)

	err := model.TagPin(gameIds, tagIds)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *TagsController) List(ctx *fasthttp.RequestCtx) {

	name := string(ctx.QueryArgs().Peek("name"))
	game_type := ctx.QueryArgs().GetUintOrZero("game_type")
	state := ctx.QueryArgs().GetUintOrZero("state")

	if name != "" {
		decodedValue, err := url.QueryUnescape(name)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		name = decodedValue
	}
	param, err := model.TagList(name, game_type, state)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, param)
}

func (that *TagsController) Insert(ctx *fasthttp.RequestCtx) {

	param := model.Tag_t{}
	body := ctx.PostBody()

	err := json.Unmarshal(body, &param)
	if err != nil {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if !model.CheckLen(param.Name, 2, 30) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.Name != "" {
		decodedValue, err := url.QueryUnescape(param.Name)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		param.Name = decodedValue
	}
	param.Tid = helper.GenId()
	//param.Name = model.FilterInjection(param.Name)
	//param.State = "1"
	param.CreatedAt = uint32(ctx.Time().Unix())

	err = model.TagInsert(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *TagsController) Update(ctx *fasthttp.RequestCtx) {

	param := model.Tag_t{}
	body := ctx.PostBody()

	state := map[string]bool{
		"0": true,
		"1": true,
	}
	err := json.Unmarshal(body, &param)
	if err != nil {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !helper.CtypeDigit(param.Tid) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !model.CheckLen(param.Name, 2, 30) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if _, ok := state[param.State]; !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if param.Name != "" {
		decodedValue, err := url.QueryUnescape(param.Name)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
		param.Name = decodedValue
	}
	//param.Name = model.FilterInjection(param.Name)

	//fmt.Println("Update = ", param.GameType)
	err = model.TagUpdate(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *TagsController) Delete(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	err := model.TagDelete(id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
