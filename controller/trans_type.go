package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"github.com/valyala/fasthttp"
)

type TransTypeController struct{}

func (that *TransTypeController) List(ctx *fasthttp.RequestCtx) {

	resp, err := model.TransTypes()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	ctx.SetBody(resp)
}
