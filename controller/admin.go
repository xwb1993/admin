package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	"encoding/base32"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
)

type AdminController struct{}

type adminInsert struct {
	GroupID  string `json:"group_id" cbor:"group_id"`
	Name     string `json:"name" cbor:"name"`
	Password string `json:"password" cbor:"password"`
	Seamo    string `json:"seamo" cbor:"seamo"`
	State    int    `json:"state" cbor:"state"`
}

func (that *AdminController) Insert(ctx *fasthttp.RequestCtx) {

	param := adminInsert{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	// 判断用户名是否已经存在
	if model.AdminExist(g.Ex{"name": param.Name}) {
		helper.Print(ctx, false, helper.UsernameExist)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	now := uint32(ctx.Time().Unix())
	record := model.Admin{
		GroupID:     param.GroupID,
		Name:        param.Name,
		Pwd:         param.Password,
		Seamo:       param.Seamo,
		State:       param.State,
		CreateAt:    now,
		CreatedUid:  admin["id"],
		CreatedName: admin["name"],
		UpdatedAt:   now,
		UpdatedUid:  admin["id"],
		UpdatedName: admin["name"],
	}
	err = model.AdminInsert(record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, "succeed")
}

type adminUpdate struct {
	ID       string `json:"id" cbor:"id"`
	GroupID  string `json:"group_id" cbor:"group_id"`
	Seamo    string `json:"seamo" cbor:"seamo"`
	Password string `json:"password" cbor:"password"`
	State    int    `json:"state" cbor:"state"`
}

func (that *AdminController) Update(ctx *fasthttp.RequestCtx) {

	param := adminUpdate{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.State != 0 && param.State != 1 {
		helper.Print(ctx, false, helper.StateParamErr)
		return
	}

	if !validator.CtypeDigit(param.ID) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	if !validator.CtypeDigit(param.GroupID) {
		helper.Print(ctx, false, helper.GroupIDErr)
		return
	}

	if len(param.Password) > 0 {
		if !validator.CheckAPassword(param.Password, 5, 20) {
			helper.Print(ctx, false, helper.PasswordFMTErr)
			return
		}
	}

	record := g.Record{}
	if param.Seamo != "" {
		_, err := base32.StdEncoding.DecodeString(param.Seamo)
		if err != nil {
			helper.Print(ctx, false, helper.SeamoErr)
			return
		}

		record["seamo"] = param.Seamo
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	record["state"] = param.State
	record["group_id"] = param.GroupID
	record["updated_at"] = ctx.Time().Unix()
	record["updated_uid"] = admin["id"]
	record["updated_name"] = admin["name"]
	err = model.AdminUpdate(admin["id"], admin["name"], param.ID, param.Password, param.GroupID, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *AdminController) UpdateState(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	state := string(ctx.QueryArgs().Peek("state"))

	s := map[string]bool{
		"0": true,
		"1": true,
	}
	if _, ok := s[state]; !ok {
		helper.Print(ctx, false, helper.StateParamErr)
		return
	}

	if !validator.CtypeDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	err = model.AdminUpdateState(admin["id"], admin["name"], id, state)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *AdminController) List(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	name := string(ctx.QueryArgs().Peek("name"))
	state := string(ctx.QueryArgs().Peek("state"))
	groupID := string(ctx.QueryArgs().Peek("groupid"))

	if page < 1 {
		page = 1
	}
	ex := g.Ex{}

	if name != "" {
		if !validator.CheckAName(name, 5, 20) {
			helper.Print(ctx, false, helper.AdminNameErr)
			return
		}

		ex["name"] = name
	}

	if state == "0" || state == "1" {
		ex["state"] = state
	}

	if groupID != "" {
		if !validator.CtypeDigit(groupID) {
			helper.Print(ctx, false, helper.GroupIDErr)
			return
		}

		ex["group_id"] = groupID
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	data, err := model.AdminList(admin["group_id"], uint(page), 10, ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

type adminLogin struct {
	Username string `json:"username" cbor:"username"`
	Pwd      string `json:"pwd" cbor:"pwd"`
	Seamo    string `json:"seamo" cbor:"seamo"`
}

func (that *AdminController) Login(ctx *fasthttp.RequestCtx) {
	deviceNo := string(ctx.Request.Header.Peek("no"))
	param := adminLogin{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	//err := cbor.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if !validator.CheckAName(param.Username, 5, 20) {
		helper.Print(ctx, false, helper.AdminNameErr)
		return
	}

	if !validator.CheckAPassword(param.Pwd, 5, 20) {
		helper.Print(ctx, false, helper.UsernameOrPasswordErr)
		return
	}

	ip := helper.FromRequest(ctx)
	/*
		if !model.WhiteListCheck(ip) {
			helper.Print(ctx, false, helper.IPErr)
			return
		}
	*/
	resp, err := model.AdminLogin(deviceNo, param.Username, param.Pwd, param.Seamo, ip, ctx.Time())
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, resp)
}
