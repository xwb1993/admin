package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"net/url"
)

type NoticeController struct{}

func (that *NoticeController) Insert(ctx *fasthttp.RequestCtx) {

	data := model.TblNotices{}
	body := ctx.PostBody()

	err := json.Unmarshal(body, &data)
	if err != nil {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	data.Id = helper.GenId()
	data.State = "1"
	data.CreatedAt = uint32(ctx.Time().Unix())

	data.Title = url.PathEscape(data.Title)
	data.Content = url.PathEscape(data.Content)

	data.CreatedUid = admin["id"]
	data.CreatedName = admin["name"]

	err = model.NoticeInsert(data)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 系统公告列表
func (that *NoticeController) List(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	state := string(ctx.QueryArgs().Peek("state"))
	createdName := string(ctx.QueryArgs().Peek("created_name"))
	startTime := string(ctx.QueryArgs().Peek("start_time"))
	endTime := string(ctx.QueryArgs().Peek("end_time"))

	if page < 1 {
		page = 1
	}
	if pageSize < 10 {
		pageSize = 10
	}
	if pageSize > 200 {
		pageSize = 200
	}
	ex := g.Ex{}

	if state != "" && (state == "1" || state == "0") {
		ex["state"] = state
	}

	if createdName != "" {
		ex["created_name"] = createdName
	}

	data, err := model.NoticeList(uint(page), uint(pageSize), startTime, endTime, ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// 系统公告编辑
func (that *NoticeController) Update(ctx *fasthttp.RequestCtx) {

	data := model.TblNotices{}
	body := ctx.PostBody()

	err := json.Unmarshal(body, &data)
	if err != nil {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	record := g.Record{}
	if data.Title != "" {
		record["title"], _ = url.PathUnescape(data.Title)
	}

	if data.Content != "" {
		record["content"], _ = url.PathUnescape(data.Content)
	}

	if len(record) == 0 {
		helper.Print(ctx, false, helper.NoDataUpdate)
		return
	}

	err = model.NoticeUpdate(data.Id, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 系统停用 启用 系统审核
func (that *NoticeController) UpdateState(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	state := ctx.QueryArgs().GetUintOrZero("state")
	s := map[int]bool{
		1: true, //停用
		2: true, //启用
	}
	if _, ok := s[state]; !ok {
		helper.Print(ctx, false, helper.StateParamErr)
		return
	}

	err := model.NoticeUpdateState(id, state)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 系统公告删除
func (that *NoticeController) Delete(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	if !helper.CtypeDigit(id) {
		helper.Print(ctx, false, helper.IDErr)
		return
	}

	err := model.NoticeDelete(id)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
