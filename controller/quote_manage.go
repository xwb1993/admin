package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
)

type QuoteManage struct{}

type operator_quote_data struct {
	OperatorId          int     `json:"operator_id" cbor:"operator_id"`
	BalanceQuota        float64 `json:"balance_quota" cbor:"balance_quota"`
	BalanceUsed         float64 `json:"balance_used" cbor:"balance_used"`
	CommissionQuota     float64 `json:"commission_quota" cbor:"commission_quota"`
	TestMemberQuota     float64 `json:"test_member_quota" cbor:"test_member_quota"`
	TestMemberUsed      float64 `json:"test_member_used" cbor:"test_member_used"`
	AutoWithdrawalLimit float64 `json:"auto_withdrawal_limit" cbor:"auto_withdrawal_limit"`
}

type operatorQuoteParam struct {
	OperatorId int `json:"operator_id" cbor:"operator_id"`
	Page       int `json:"page" cbor:"page"`
	PageSize   int `json:"page_size" cbor:"page_size"`
}

// /获取运营商额度列表
func (that MemberController) GetOperatorQuoteList(ctx *fasthttp.RequestCtx) {
	param := operatorQuoteParam{}
	fmt.Println(param)
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if param.Page < 1 {
		param.Page = 1
	}
	if param.PageSize < 10 || param.PageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	ex := g.Ex{}
	if param.OperatorId > 0 {
		ex["operator_id"] = param.OperatorId
	}
	data, err := model.GetOpeartorQuoteList(param.Page, param.PageSize, ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// /添加额度明细
func (that MemberController) AddOperatorQuote(ctx *fasthttp.RequestCtx) {
	param := model.AddOperatorQuoteParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	err = model.AddOperatorQuote(param)
	if err != nil {
		helper.Print(ctx, false, helper.DBErr)
	}
	helper.Print(ctx, true, helper.Success)
}
