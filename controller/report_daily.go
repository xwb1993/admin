package controller

//各类日统计报表，游戏日报表，盈亏报表
import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
)

type ReportDailyController struct{}

// 日况数据修正
func (that *ReportDailyController) FixDaily(ctx *fasthttp.RequestCtx) {
	from := string(ctx.QueryArgs().Peek("from")) //开始时间
	to := string(ctx.QueryArgs().Peek("to"))     //结束时间
	res := model.FixDailyData(from, to)
	if res {
		helper.Print(ctx, true, "success")
	} else {
		helper.Print(ctx, true, "error")
	}
}

func (this *ReportDailyController) UserList(ctx *fasthttp.RequestCtx) {
	param := model.ReportDailyParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Page < 1 {
		param.Page = 1
	}

	if param.PageSize < 10 || param.PageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	s, err := model.ReportUserDailyList(param, admin["id"])
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

func (this *ReportDailyController) PlatformList(ctx *fasthttp.RequestCtx) {
	param := model.ReportDailyParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	param.IsOperator = ctx.QueryArgs().GetUintOrZero("is_operator")
	param.IsBusiness = ctx.QueryArgs().GetUintOrZero("is_business")

	if param.Page < 1 {
		param.Page = 1
	}

	if param.PageSize < 10 || param.PageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	s, err := model.ReportPlatformDailyList(param, admin["id"])
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, s)
}

func (this *ReportDailyController) GameList(ctx *fasthttp.RequestCtx) {
	param := model.GameDailyParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	param.IsOperator = ctx.QueryArgs().GetUintOrZero("is_operator")
	param.IsBusiness = ctx.QueryArgs().GetUintOrZero("is_business")
	if param.Page < 1 {
		param.Page = 1
	}

	if param.PageSize < 10 || param.PageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	s, err := model.ReportGameDailyList(param, admin["id"])
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, s)
}

func (this *ReportDailyController) ProxyDailyList(ctx *fasthttp.RequestCtx) {
	param := model.ReportProxyDailyParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Page < 1 {
		param.Page = 1
	}

	if param.PageSize < 10 || param.PageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	s, err := model.ProxyDailyList(param, admin["id"])
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, s)
}
