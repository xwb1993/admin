package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"net/url"
	"strconv"
)

type PromoSignController struct{}

// 会员配置新增
func (that PromoSignController) ConfigInsert(ctx *fasthttp.RequestCtx) {

	param := model.PromoSignConfig{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	err = model.PromoSignConfigInsert(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 会员配置列表
func (that PromoSignController) ConfigList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	sVip := string(ctx.QueryArgs().Peek("vip"))
	ex := g.Ex{}
	if sVip != "" {
		vip, err := strconv.Atoi(sVip)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}

		ex["vip"] = vip
	}

	s, err := model.PromoSignConfigList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 会员配置更新
func (that PromoSignController) ConfigUpdate(ctx *fasthttp.RequestCtx) {

	param := model.PromoSignConfig{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	err = model.PromoSignConfigUpdate(param.Vip, param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 会员配置列表
func (that PromoSignController) RecordList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	sVip := string(ctx.QueryArgs().Peek("vip"))
	username := string(ctx.QueryArgs().Peek("username"))

	ex := g.Ex{}
	if sVip != "" {
		vip, err := strconv.Atoi(sVip)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}

		ex["vip"] = vip
	}

	username, _ = url.QueryUnescape(username)
	if username != "" {
		if !validator.CheckUName(username, 5, 50) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}

		ex["username"] = username
	}

	s, err := model.PromoSignRecordList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 会员配置列表
func (that PromoSignController) RewardRecordList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	sVip := string(ctx.QueryArgs().Peek("vip"))
	day := ctx.QueryArgs().GetUintOrZero("day")
	username := string(ctx.QueryArgs().Peek("username"))

	ex := g.Ex{}
	if sVip != "" {
		vip, err := strconv.Atoi(sVip)
		if err != nil {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}

		ex["vip"] = vip
	}

	if day > 0 {
		ex["day"] = day
	}

	username, _ = url.QueryUnescape(username)
	if username != "" {
		if !validator.CheckUName(username, 5, 50) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}

		ex["username"] = username
	}

	s, err := model.PromoSignRewardRecordList(uint(page), uint(pageSize), ex)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 渠道签到活动-活动记录列表
func (that PromoSignController) MerchantRecordList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	username := string(ctx.QueryArgs().Peek("username"))
	operatorId := string(ctx.QueryArgs().Peek("operator_id"))

	if operatorId == "" {
		helper.Print(ctx, false, helper.OperatorIdErr)
		return
	}

	s, err := model.MerchantPromoSignRecordList(uint(page), uint(pageSize), username, operatorId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 业务员签到活动-活动记录列表
func (that PromoSignController) BusinessRecordList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	username := string(ctx.QueryArgs().Peek("username"))
	businessId := string(ctx.QueryArgs().Peek("business_id"))

	if businessId == "" {
		helper.Print(ctx, false, helper.BusinessIdErr)
		return
	}

	s, err := model.BusinessPromoSignRecordList(uint(page), uint(pageSize), username, businessId)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}
