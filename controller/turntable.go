package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"net/url"
	"strconv"
)

type TurnTableController struct{}

func (that *TurnTableController) List(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	uid := string(ctx.QueryArgs().Peek("uid"))
	username := string(ctx.QueryArgs().Peek("username"))      // 会员账号
	reviewName := string(ctx.QueryArgs().Peek("review_name")) // 审核人
	state := ctx.QueryArgs().GetUintOrZero("state")           // 审核状态
	timeType := ctx.QueryArgs().GetUintOrZero("time_type")    // 查询时间类型 1 申请时间 2 通过时间
	startTime := string(ctx.QueryArgs().Peek("start_time"))   //开始时间
	endTime := string(ctx.QueryArgs().Peek("end_time"))       //结束时间
	sPage := string(ctx.QueryArgs().Peek("page"))             //当前页
	sPageSize := string(ctx.QueryArgs().Peek("page_size"))    //一页多少条

	if !validator.CheckStringDigit(sPage) || !validator.CheckIntScope(sPageSize, 10, 200) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	page, _ := strconv.Atoi(sPage)
	pageSize, _ := strconv.Atoi(sPageSize)

	ex := g.Ex{}
	if id != "" {
		if !validator.CheckStringDigit(id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex["id"] = id
	}

	if uid != "" {
		ex["uid"] = uid
	}
	if username != "" {
		ex["username"], _ = url.PathUnescape(username)
	}

	if reviewName != "" {

		if !validator.CheckAName(reviewName, 5, 20) {
			helper.Print(ctx, false, helper.ReviewNameErr)
			return
		}

		ex["review_name"], _ = url.PathUnescape(reviewName)
	}

	if state > 0 {
		s := map[int]bool{
			1: true, //Reviewing
			2: true, //ReviewPass
			3: true, //ReviewReject
		}
		if _, ok := s[state]; !ok {
			helper.Print(ctx, false, helper.ReviewStateErr)
			return
		}

		ex["state"] = state
	}

	data, err := model.TurnTableReviewList(startTime, endTime, ex, timeType, page, pageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *TurnTableController) Review(ctx *fasthttp.RequestCtx) {

	param := reviewMemberAdjustParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	if param.State != "2" && param.State != "3" {
		helper.Print(ctx, false, helper.StateParamErr)
		return
	}

	if param.ReviewRemark != "" {
		param.ReviewRemark, _ = url.PathUnescape(param.ReviewRemark)
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	record := g.Record{
		"id":            param.Id,
		"review_remark": param.ReviewRemark,
		"state":         param.State,
		"review_at":     ctx.Time().Unix(),
		"review_uid":    admin["id"],
		"review_name":   admin["name"],
	}
	err = model.TurnTableReview(param.State, record)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

func (that *TurnTableController) History(ctx *fasthttp.RequestCtx) {

	id := string(ctx.QueryArgs().Peek("id"))
	uid := string(ctx.QueryArgs().Peek("uid"))
	username := string(ctx.QueryArgs().Peek("username"))
	addType := ctx.QueryArgs().GetUintOrZero("type")        // 增加类型
	startTime := string(ctx.QueryArgs().Peek("start_time")) //开始时间
	endTime := string(ctx.QueryArgs().Peek("end_time"))     //结束时间
	sPage := string(ctx.QueryArgs().Peek("page"))           //当前页
	sPageSize := string(ctx.QueryArgs().Peek("page_size"))  //一页多少条

	if !validator.CheckStringDigit(sPage) || !validator.CheckIntScope(sPageSize, 10, 200) {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	page, _ := strconv.Atoi(sPage)
	pageSize, _ := strconv.Atoi(sPageSize)

	ex := g.Ex{}
	if id != "" {
		if !validator.CheckStringDigit(id) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex["id"] = id
	}

	if uid != "" {
		ex["uid"] = uid
	}
	if username != "" {
		ex["username"], _ = url.PathUnescape(username)
	}

	if addType > 0 {
		s := map[int]bool{
			1: true, //大随机金额
			2: true, //小随机金额
			3: true, //1000元
			4: true, //50元
			5: true, //1元
			6: true, //获得随机次数
			7: true, //直接领奖
			8: true, //无奖励
			9: true, //后台增加
		}
		if _, ok := s[addType]; !ok {
			helper.Print(ctx, false, helper.AddTypeErr)
			return
		}

		ex["type"] = addType
	}

	data, err := model.TurnTableHistoryList(startTime, endTime, ex, page, pageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *TurnTableController) Report(ctx *fasthttp.RequestCtx) {

	uid := string(ctx.QueryArgs().Peek("uid"))             // 用户id
	userName := string(ctx.QueryArgs().Peek("username"))   // 会员帐号
	page := ctx.QueryArgs().GetUintOrZero("page")          //页码
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size") //一页多少条
	if page < 1 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if pageSize < 10 || pageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	userName, _ = url.QueryUnescape(userName)

	data, err := model.TurnTableReport(page, pageSize, uid, userName)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}
	helper.Print(ctx, true, data)

}
