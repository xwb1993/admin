package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
)

type ReportApiFeeController struct{}

func (this *ReportApiFeeController) ApiFee(ctx *fasthttp.RequestCtx) {
	param := model.ReportApiFeeParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}
	param.IsOperator = ctx.QueryArgs().GetUintOrZero("is_operator")
	param.IsBusiness = ctx.QueryArgs().GetUintOrZero("is_business")

	if param.Page < 1 {
		param.Page = 1
	}

	if param.PageSize < 10 || param.PageSize > 3000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	s, err := model.ReportApiFeeList(param, admin["id"])
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

func (this *ReportApiFeeController) Test(ctx *fasthttp.RequestCtx) {
	helper.Print(ctx, true, helper.Success)
}
