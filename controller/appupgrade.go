package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	"encoding/json"
	"github.com/valyala/fasthttp"
)

type AppUpgradeController struct{}

var (
	apps = map[string]bool{
		"ios":     true,
		"android": true,
	}
)

// 修改
func (that *AppUpgradeController) Update(ctx *fasthttp.RequestCtx) {

	var param model.AppUpgrade

	b := ctx.PostBody()
	err := json.Unmarshal(b, &param)
	if err != nil {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}
	if _, ok := apps[param.Platform]; !ok {
		helper.Print(ctx, false, helper.DeviceErr)
		return
	}

	if len(param.Content) > 255 {
		helper.Print(ctx, false, helper.ContentLengthErr)
		return
	}

	if !validator.CheckUrl(param.URL) {
		helper.Print(ctx, false, helper.URLErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	param.Content = model.FilterInjection(param.Content)
	param.UpdatedAt = uint32(ctx.Time().Unix())
	param.UpdatedName = admin["name"]
	param.UpdatedUid = admin["id"]

	err = model.AppUpgradeUpdate(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 版本管理列表
func (that *AppUpgradeController) List(ctx *fasthttp.RequestCtx) {

	data, err := model.AppUpgradeList()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}
