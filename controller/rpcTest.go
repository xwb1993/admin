package controller

import (
	"admin/model"
	"github.com/valyala/fasthttp"
)

type RpcTestController struct{}

func (this *RpcTestController) RpcQuery(ctx *fasthttp.RequestCtx) {
	uid := string(ctx.QueryArgs().Peek("uid"))
	model.MemberRpcQuery(uid)
	//model.MemberRpcInset(uid)
	//model.MemberRpcDel(uid)
}

func (this *RpcTestController) RpcTest(ctx *fasthttp.RequestCtx) {
	uid := string(ctx.QueryArgs().Peek("uid"))
	model.MemberRpcUpdate(uid)
	//model.MemberRpcInset(uid)
	//model.MemberRpcDel(uid)
}
