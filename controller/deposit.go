package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	"encoding/json"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/valyala/fasthttp"
	"strconv"
)

type DepositController struct{}

// 订单列表
type depositHistoryParam struct {
	ID         string `json:"id"`
	Uid        string `json:"uid"`
	Username   string `json:"username"`
	ParentName string `json:"parent_name"`
	State      int    `json:"state"`
	OID        string `json:"oid"`
	Fid        string `json:"fid"`
	TimeFlag   uint8  `json:"time_flag"`  // 时间类型  1:创建时间 0:完成时间
	StartTime  string `json:"start_time"` // 查询开始时间
	EndTime    string `json:"end_time"`   // 查询结束时间
	MinAmount  string `json:"min_amount"` //
	MaxAmount  string `json:"max_amount"` //
	Page       int    `json:"page"`       // 页码
	PageSize   int    `json:"page_size"`  // 页大小
	Dty        int    `json:"dty"`
	SortField  string `json:"sort_field"` //排序字段
	IsAsc      int    `json:"is_asc"`
	OperatorId string `json:"operator_id"` //渠道id
	BusinessId string `json:"business_id"` //业务员id
}

type depositListParam struct {
	ID        string `json:"id"`
	Uid       string `json:"uid"`
	Username  string `json:"username"`
	State     int    `json:"state"`
	OID       string `json:"oid"`
	Fid       string `json:"fid"`
	StartTime string `json:"start_time"` // 查询开始时间
	EndTime   string `json:"end_time"`   // 查询结束时间
	MinAmount string `json:"min_amount"` //
	MaxAmount string `json:"max_amount"` //
	Page      int    `json:"page"`       // 页码
	PageSize  int    `json:"page_size"`  // 页大小
	ChannelId string `json:"channel_id"` // 通道id
}

type tblDeposit struct {
	Id           string  `json:"id" db:"id" cbor:"id"`
	Oid          string  `json:"oid" db:"oid" cbor:"oid"`
	Uid          string  `json:"uid" db:"uid" cbor:"uid"`
	ParentId     string  `json:"parent_id" db:"parent_id" cbor:"parent_id"`
	ParentName   string  `json:"parent_name" db:"parent_name" cbor:"parent_name"`
	Username     string  `json:"username" db:"username" cbor:"username"`
	Fid          string  `json:"fid" db:"fid" cbor:"fid"`
	Fname        string  `json:"fname" db:"fname" cbor:"fname"`
	Amount       string  `json:"amount" db:"amount" cbor:"amount"`
	State        int     `json:"state" db:"state" cbor:"state"`
	CreatedAt    int64   `json:"created_at" db:"created_at" cbor:"created_at"`
	CreatedUid   string  `json:"created_uid" db:"created_uid" cbor:"created_uid"`
	CreatedName  string  `json:"created_name" db:"created_name" cbor:"created_name"`
	ConfirmAt    int64   `json:"confirm_at" db:"confirm_at" cbor:"confirm_at"`
	ConfirmUid   string  `json:"confirm_uid" db:"confirm_uid" cbor:"confirm_uid"`
	ConfirmName  string  `json:"confirm_name" db:"confirm_name" cbor:"confirm_name"`
	ReviewRemark string  `json:"review_remark" db:"review_remark" cbor:"review_remark"`
	TopId        string  `json:"top_id" db:"top_id" cbor:"top_id"`
	TopName      string  `json:"top_name" db:"top_name" cbor:"top_name"`
	Level        int     `json:"level" db:"level" cbor:"level"`
	Discount     float64 `json:"discount" db:"discount" cbor:"discount"`
	Tester       int     `json:"tester" db:"tester" cbor:"tester"`
	SuccessTime  int     `json:"success_time" db:"success_time" cbor:"success_time"`
	UsdtRate     float64 `json:"usdt_rate" db:"usdt_rate" cbor:"usdt_rate"`
	UsdtCount    float64 `json:"usdt_count" db:"usdt_count" cbor:"usdt_count"`
	Prefix       string  `json:"prefix" db:"prefix" cbor:"prefix"`
}

// List 存款列表  三方存款
func (that *DepositController) List(ctx *fasthttp.RequestCtx) {

	param := depositListParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	ex := g.Ex{}

	if param.Username != "" {
		if !validator.CheckUName(param.Username, 5, 14) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}

		mb, err := model.MemberFindByUsername(param.Username)
		if err != nil {
			helper.Print(ctx, false, helper.UserNotExist)
			return
		}

		ex["uid"] = mb.Uid
	}

	if param.ID != "" {
		if !validator.CheckStringDigit(param.ID) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex["id"] = param.ID
	}

	if param.Uid != "" {
		if !validator.CheckStringDigit(param.Uid) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}

		ex["uid"] = param.Uid
	}

	if param.OID != "" {
		if !validator.CheckStringAlnum(param.OID) {
			helper.Print(ctx, false, helper.OIDErr)
			return
		}

		ex["oid"] = param.OID
	}

	if param.Fid != "" {
		if !validator.CheckStringDigit(param.Fid) {
			helper.Print(ctx, false, helper.ChannelIDErr)
			return
		}

		ex["fid"] = param.Fid
	}
	if param.ChannelId != "" {
		if !validator.CheckStringDigit(param.ChannelId) {
			helper.Print(ctx, false, helper.ChannelIDErr)
			return
		}

		ex["fid"] = param.ChannelId
	}

	if param.State != 0 {
		if param.State < model.DepositConfirming || param.State > model.DepositReviewing {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}

		ex["state"] = param.State
	}

	if param.MinAmount != "" && param.MaxAmount != "" {
		ex["amount"] = g.Op{"between": exp.NewRangeVal(param.MinAmount, param.MaxAmount)}
	}

	data, err := model.DepositList(ex, param.StartTime, param.EndTime, param.Page, param.PageSize)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// History 存款历史记录
func (that *DepositController) History(ctx *fasthttp.RequestCtx) {

	param := depositHistoryParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Page < 1 {
		param.Page = 10
	}

	if param.Username != "" {
		if !validator.CheckUName(param.Username, 5, 14) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}
	}

	if param.ID != "" {
		if !validator.CheckStringDigit(param.ID) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}
	}

	if param.Uid != "" {
		if !validator.CheckStringDigit(param.Uid) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}
	}

	if param.OID != "" {
		if !validator.CheckStringAlnum(param.OID) {
			helper.Print(ctx, false, helper.OIDErr)
			return
		}
	}

	if param.Fid != "" {
		if !validator.CheckStringAlnum(param.Fid) {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
	}

	if param.State != 0 {
		if param.State != model.DepositConfirming && param.State != model.DepositSuccess && param.State != model.DepositCancelled && param.State != model.DepositReviewing && param.State != model.DepositRepairSuccess {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}
	}

	if param.StartTime == "" || param.EndTime == "" {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.SortField != "" {
		sortFields := map[string]string{
			"amount":     "amount",
			"discount":   "discount",
			"created_at": "created_at",
		}

		if _, ok := sortFields[param.SortField]; !ok {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}

		if !validator.CheckIntScope(strconv.Itoa(param.IsAsc), 0, 1) {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
	}

	data, err := model.DepositHistory(param.Uid, param.Username, param.ParentName, param.ID, param.Fid, param.OID, strconv.Itoa(param.State),
		param.MinAmount, param.MaxAmount, param.StartTime, param.EndTime, param.SortField, param.TimeFlag, param.Page, param.PageSize, param.Dty, param.IsAsc)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *DepositController) Update(ctx *fasthttp.RequestCtx) {

	param := depositHistoryParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	err = model.DepositUpdate(param.ID, param.State)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)

}

// MerchantHistory 渠道存款历史记录
func (that *DepositController) MerchantHistory(ctx *fasthttp.RequestCtx) {

	param := depositHistoryParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Page < 1 {
		param.Page = 10
	}

	if param.OperatorId == "" {
		helper.Print(ctx, false, helper.OperatorIdErr)
		return
	}

	if param.Username != "" {
		if !validator.CheckUName(param.Username, 5, 14) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}
	}

	if param.ID != "" {
		if !validator.CheckStringDigit(param.ID) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}
	}

	if param.Uid != "" {
		if !validator.CheckStringDigit(param.Uid) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}
	}

	if param.OID != "" {
		if !validator.CheckStringAlnum(param.OID) {
			helper.Print(ctx, false, helper.OIDErr)
			return
		}
	}

	if param.Fid != "" {
		if !validator.CheckStringAlnum(param.Fid) {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
	}

	if param.State != 0 {
		if param.State != model.DepositConfirming && param.State != model.DepositSuccess && param.State != model.DepositCancelled && param.State != model.DepositReviewing && param.State != model.DepositRepairSuccess {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}
	}

	if param.StartTime == "" || param.EndTime == "" {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.SortField != "" {
		sortFields := map[string]string{
			"amount":     "amount",
			"discount":   "discount",
			"created_at": "created_at",
		}

		if _, ok := sortFields[param.SortField]; !ok {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}

		if !validator.CheckIntScope(strconv.Itoa(param.IsAsc), 0, 1) {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
	}

	data, err := model.MerchantDepositHistory(param.OperatorId, param.Uid, param.Username, param.ParentName, param.ID, param.Fid, param.OID, strconv.Itoa(param.State),
		param.MinAmount, param.MaxAmount, param.StartTime, param.EndTime, param.SortField, param.TimeFlag, param.Page, param.PageSize, param.Dty, param.IsAsc)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

// BusinessHistory 业务员存款历史记录
func (that *DepositController) BusinessHistory(ctx *fasthttp.RequestCtx) {

	param := depositHistoryParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Page < 1 {
		param.Page = 10
	}

	if param.BusinessId == "" {
		helper.Print(ctx, false, helper.BusinessIdErr)
		return
	}

	if param.Username != "" {
		if !validator.CheckUName(param.Username, 5, 14) {
			helper.Print(ctx, false, helper.UsernameErr)
			return
		}
	}

	if param.ID != "" {
		if !validator.CheckStringDigit(param.ID) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}
	}

	if param.Uid != "" {
		if !validator.CheckStringDigit(param.Uid) {
			helper.Print(ctx, false, helper.IDErr)
			return
		}
	}

	if param.OID != "" {
		if !validator.CheckStringAlnum(param.OID) {
			helper.Print(ctx, false, helper.OIDErr)
			return
		}
	}

	if param.Fid != "" {
		if !validator.CheckStringAlnum(param.Fid) {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
	}

	if param.State != 0 {
		if param.State != model.DepositConfirming && param.State != model.DepositSuccess && param.State != model.DepositCancelled && param.State != model.DepositReviewing && param.State != model.DepositRepairSuccess {
			helper.Print(ctx, false, helper.StateParamErr)
			return
		}
	}

	if param.StartTime == "" || param.EndTime == "" {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.SortField != "" {
		sortFields := map[string]string{
			"amount":     "amount",
			"discount":   "discount",
			"created_at": "created_at",
		}

		if _, ok := sortFields[param.SortField]; !ok {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}

		if !validator.CheckIntScope(strconv.Itoa(param.IsAsc), 0, 1) {
			helper.Print(ctx, false, helper.ParamErr)
			return
		}
	}

	data, err := model.BusinessDepositHistory(param.BusinessId, param.Uid, param.Username, param.ParentName, param.ID, param.Fid, param.OID, strconv.Itoa(param.State),
		param.MinAmount, param.MaxAmount, param.StartTime, param.EndTime, param.SortField, param.TimeFlag, param.Page, param.PageSize, param.Dty, param.IsAsc)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}
