package controller

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"admin/model"
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
	"net/url"
)

type RecordController struct{}

type recordGameParam struct {
	Uid        string `json:"uid"`
	Pid        string `json:"pid"`
	PlatType   string `json:"plat_type"`
	GameName   string `json:"game_name"`
	Username   string `json:"username"`
	ParentName string `json:"parent_name"`
	BillNo     string `json:"bill_no"`
	Flag       string `json:"flag"`
	GameNo     string `json:"game_no"`
	Presettle  string `json:"presettle"`
	Resettle   string `json:"resettle"`
	BetMin     string `json:"bet_min"`
	BetMax     string `json:"bet_max"`
	TimeFlag   string `json:"time_flag"`
	StartTime  string `json:"start_time"`
	EndTime    string `json:"end_time"`
	Page       int    `json:"page"`
	PageSize   int    `json:"page_size"`
	WinLose    string `json:"win_lose"`
	MainBillNo string `json:"main_bill_no"`
	OperatorId string `json:"operator_id"`
	BusinessId string `json:"business_id"`
}

// 有效投注查询/会员游戏记录详情列表/投注管理列表
func (that *RecordController) RecordGame(ctx *fasthttp.RequestCtx) {

	param := recordGameParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Page == 0 {
		param.Page = 1
	}

	if param.PageSize < 10 || param.PageSize > 10000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	tf := map[string]bool{
		"1": true,
		"2": true,
		"3": true,
		"4": true,
		"5": true,
		"6": true,
	}
	if _, ok := tf[param.TimeFlag]; !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.BetMin != "" && param.BetMax != "" {
		if !validator.CheckMoney(param.BetMin) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		if !validator.CheckMoney(param.BetMax) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}
	}

	if param.Presettle != "" {
		if !validator.CtypeDigit(param.Presettle) {
			helper.Print(ctx, false, helper.PresettleFlagErr)
			return
		}
	}

	if param.Resettle != "" {
		if !validator.CtypeDigit(param.Resettle) {
			helper.Print(ctx, false, helper.PresettleFlagErr)
			return
		}
	}

	if param.Username != "" {
		param.Username, _ = url.PathUnescape(param.Username)
	}

	p := map[string]string{
		"uid":          param.Uid,
		"pid":          param.Pid,
		"plat_type":    param.PlatType,
		"game_name":    param.GameName,
		"username":     param.Username,
		"parent_name":  param.ParentName,
		"bill_no":      param.BillNo,
		"flag":         param.Flag,
		"time_flag":    param.TimeFlag,
		"start_time":   param.StartTime,
		"end_time":     param.EndTime,
		"game_no":      param.GameNo,
		"pre_settle":   param.Presettle,
		"resettle":     param.Resettle,
		"win_lose":     param.WinLose,
		"main_bill_no": param.MainBillNo,
	}

	if param.BetMax != "" && param.BetMin != "" {
		p["bet_min"] = param.BetMin
		p["bet_max"] = param.BetMax
	}

	data, err := model.GameRecord(uint(param.PageSize), uint(param.Page), p)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
	return

}

// 渠道有效投注查询/会员游戏记录详情列表/投注管理列表
func (that *RecordController) MerchantRecordGame(ctx *fasthttp.RequestCtx) {

	param := recordGameParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Page == 0 {
		param.Page = 1
	}

	if param.PageSize < 10 || param.PageSize > 10000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.OperatorId == "" {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	tf := map[string]bool{
		"1": true,
		"2": true,
		"3": true,
		"4": true,
		"5": true,
		"6": true,
	}
	if _, ok := tf[param.TimeFlag]; !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.BetMin != "" && param.BetMax != "" {
		if !validator.CheckMoney(param.BetMin) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		if !validator.CheckMoney(param.BetMax) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}
	}

	if param.Presettle != "" {
		if !validator.CtypeDigit(param.Presettle) {
			helper.Print(ctx, false, helper.PresettleFlagErr)
			return
		}
	}

	if param.Resettle != "" {
		if !validator.CtypeDigit(param.Resettle) {
			helper.Print(ctx, false, helper.PresettleFlagErr)
			return
		}
	}

	if param.Username != "" {
		param.Username, _ = url.PathUnescape(param.Username)
	}

	p := map[string]string{
		"uid":          param.Uid,
		"pid":          param.Pid,
		"plat_type":    param.PlatType,
		"game_name":    param.GameName,
		"username":     param.Username,
		"parent_name":  param.ParentName,
		"bill_no":      param.BillNo,
		"flag":         param.Flag,
		"time_flag":    param.TimeFlag,
		"start_time":   param.StartTime,
		"end_time":     param.EndTime,
		"game_no":      param.GameNo,
		"pre_settle":   param.Presettle,
		"resettle":     param.Resettle,
		"win_lose":     param.WinLose,
		"main_bill_no": param.MainBillNo,
		"operator_id":  param.OperatorId,
	}

	if param.BetMax != "" && param.BetMin != "" {
		p["bet_min"] = param.BetMin
		p["bet_max"] = param.BetMax
	}

	data, err := model.MerchantGameRecord(uint(param.PageSize), uint(param.Page), p)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
	return

}

// 渠道有效投注查询/会员游戏记录详情列表/投注管理列表
func (that *RecordController) BusinessRecordGame(ctx *fasthttp.RequestCtx) {

	param := recordGameParam{}
	err := json.Unmarshal(ctx.PostBody(), &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.FormatErr)
		return
	}

	if param.Page == 0 {
		param.Page = 1
	}

	if param.PageSize < 10 || param.PageSize > 10000 {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.BusinessId == "" {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	tf := map[string]bool{
		"1": true,
		"2": true,
		"3": true,
		"4": true,
		"5": true,
		"6": true,
	}
	if _, ok := tf[param.TimeFlag]; !ok {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if param.BetMin != "" && param.BetMax != "" {
		if !validator.CheckMoney(param.BetMin) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}

		if !validator.CheckMoney(param.BetMax) {
			helper.Print(ctx, false, helper.AmountErr)
			return
		}
	}

	if param.Presettle != "" {
		if !validator.CtypeDigit(param.Presettle) {
			helper.Print(ctx, false, helper.PresettleFlagErr)
			return
		}
	}

	if param.Resettle != "" {
		if !validator.CtypeDigit(param.Resettle) {
			helper.Print(ctx, false, helper.PresettleFlagErr)
			return
		}
	}

	if param.Username != "" {
		param.Username, _ = url.PathUnescape(param.Username)
	}

	p := map[string]string{
		"uid":          param.Uid,
		"pid":          param.Pid,
		"plat_type":    param.PlatType,
		"game_name":    param.GameName,
		"username":     param.Username,
		"parent_name":  param.ParentName,
		"bill_no":      param.BillNo,
		"flag":         param.Flag,
		"time_flag":    param.TimeFlag,
		"start_time":   param.StartTime,
		"end_time":     param.EndTime,
		"game_no":      param.GameNo,
		"pre_settle":   param.Presettle,
		"resettle":     param.Resettle,
		"win_lose":     param.WinLose,
		"main_bill_no": param.MainBillNo,
		"operator_id":  param.OperatorId,
		"business_id":  param.BusinessId,
	}

	if param.BetMax != "" && param.BetMin != "" {
		p["bet_min"] = param.BetMin
		p["bet_max"] = param.BetMax
	}

	data, err := model.BusinessGameRecord(uint(param.PageSize), uint(param.Page), p)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
	return

}
