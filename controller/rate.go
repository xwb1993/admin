package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"github.com/valyala/fasthttp"
)

type RateController struct{}

type rateForm_t struct {
	Id   int     `json:"id"`
	Ty   uint32  `json:"ty"`
	Rate float64 `json:"rate"`
	Min  float64 `json:"min"`
	Max  float64 `json:"max"`
}

func (that *RateController) List(ctx *fasthttp.RequestCtx) {

	data, err := model.RateListList()
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, data)
}

func (that *RateController) Update(ctx *fasthttp.RequestCtx) {

	param := rateForm_t{}
	body := ctx.PostBody()

	err := json.Unmarshal(body, &param)
	if err != nil {
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	admin, err := model.AdminToken(ctx)
	if err != nil {
		helper.Print(ctx, false, helper.AccessTokenExpires)
		return
	}

	err = model.RateListUpdate(admin["name"], param.Id, param.Ty, param.Rate, param.Min, param.Max)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
