package controller

import (
	"admin/contrib/helper"
	"admin/model"
	"encoding/json"
	"fmt"
	"github.com/valyala/fasthttp"
)

type BankCardController struct{}

// 会员银行卡列表
func (that BankCardController) List(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	uid := string(ctx.QueryArgs().Peek("uid"))
	username := string(ctx.QueryArgs().Peek("username"))

	s, err := model.MemberCardList(uint(page), uint(pageSize), uid, username)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 银行类型列表
func (that BankCardController) BankTypeList(ctx *fasthttp.RequestCtx) {
	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")

	s, err := model.BankTypeList(uint(page), uint(pageSize))
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

type insertBankParam struct {
	PixId    string `json:"pix_id"`
	Username string `json:"username"`
	BankCode string `json:"bankcode"`
}

// 绑定银行卡
func (that *BankCardController) Insert(ctx *fasthttp.RequestCtx) {

	param := insertBankParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !helper.CtypeDigit(param.PixId) {
		helper.Print(ctx, false, helper.BankcardIDErr)
		return
	}

	// 用户绑定银行卡
	err = model.BankcardInsert(param.Username, param.PixId, param.BankCode)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

type insertBankType struct {
	Bankname string `json:"bankname"`
	Bankcode string `json:"bankcode"`
}

func (that *BankCardController) InsertBankType(ctx *fasthttp.RequestCtx) {

	param := insertBankType{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	// 用户绑定银行卡
	err = model.InsertBankType(param.Bankcode, param.Bankname)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 绑定渠道银行卡
func (that *BankCardController) MerchantInsert(ctx *fasthttp.RequestCtx) {

	param := model.InsertMerchantBankParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !helper.CtypeDigit(param.PixId) {
		helper.Print(ctx, false, helper.BankcardIDErr)
		return
	}

	// 用户绑定银行卡
	err = model.MerchantBankcardInsert(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}

// 渠道会员银行卡列表
func (that BankCardController) MerchantList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	operatorId := string(ctx.QueryArgs().Peek("operator_id"))
	username := string(ctx.QueryArgs().Peek("username"))

	s, err := model.MerchantMemberCardList(uint(page), uint(pageSize), operatorId, username)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 业务员会员银行卡列表
func (that BankCardController) BusinessList(ctx *fasthttp.RequestCtx) {

	page := ctx.QueryArgs().GetUintOrZero("page")
	pageSize := ctx.QueryArgs().GetUintOrZero("page_size")
	businessId := string(ctx.QueryArgs().Peek("business_id"))
	username := string(ctx.QueryArgs().Peek("username"))

	s, err := model.BusinessMemberCardList(uint(page), uint(pageSize), businessId, username)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, s)
}

// 绑定业务员银行卡
func (that *BankCardController) BusinessInsert(ctx *fasthttp.RequestCtx) {

	param := model.InsertBusinessBankParam{}
	data := ctx.PostBody()
	err := json.Unmarshal(data, &param)
	if err != nil {
		fmt.Println("json.Unmarshal err = ", err.Error())
		helper.Print(ctx, false, helper.ParamErr)
		return
	}

	if !helper.CtypeDigit(param.PixId) {
		helper.Print(ctx, false, helper.BankcardIDErr)
		return
	}

	// 用户绑定银行卡
	err = model.BusinessBankcardInsert(param)
	if err != nil {
		helper.Print(ctx, false, err.Error())
		return
	}

	helper.Print(ctx, true, helper.Success)
}
