package model

import (
	"admin/contrib/helper"
	"admin/contrib/session"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"time"
)

// MerchantLoginResp 渠道用户登录返回数据结构体
type MerchantLoginResp struct {
	Id           string `json:"id" cbor:"id"`                       // 渠道id
	Token        string `json:"token" cbor:"token"`                 // 用户token
	OperatorName string `json:"operator_name" cbor:"operator_name"` // 渠道名
}

func ChannelConfigList(page, pageSize uint, ex g.Ex) (OperatorInfoData, error) {

	data := OperatorInfoData{}
	t := dialect.From("tbl_operator_info")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsAgentChannel...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("created_at").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func ChannelConfigInsert(adminName, adminId string, data TblOperatorInfo) error {

	record := TblOperatorInfo{
		Id:                 helper.GenId(),
		OperatorName:       data.OperatorName,
		State:              data.State, //0未启用 1启用
		Password:           data.Password,
		ProxyExtendLink:    data.ProxyExtendLink,
		WithdrawFee:        data.WithdrawFee,
		RechargeFee:        data.RechargeFee,
		ApiFee:             "{}",
		SingleUrl:          data.SingleUrl,
		YoutubeShareLink:   data.YoutubeShareLink,
		TwitterShareLink:   data.TwitterShareLink,
		TelegramShareLink:  data.TelegramShareLink,
		InstagramShareLink: data.InstagramShareLink,
		CreatedAt:          time.Now().Unix(),
		CreatedUid:         adminId,
		CreatedName:        adminName,
		UpdatedAt:          time.Now().Unix(),
		UpdatedUid:         adminId,
		UpdatedName:        adminName,
	}

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	query, _, _ := dialect.Insert("tbl_operator_info").Rows(record).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	return err
}

func ChannelConfigUpdate(adminName, adminId string, data TblOperatorInfo) error {

	record := g.Record{
		"operator_name":     data.OperatorName,
		"state":             data.State, //0未启用 1启用
		"password":          data.Password,
		"proxy_extend_link": data.ProxyExtendLink,
		"withdraw_remain":   data.WithdrawRemain,
		"withdraw_fee":      data.WithdrawFee,
		"recharge_fee":      data.RechargeFee,
		//"api_fee":              data.ApiFee,
		"single_url":           data.SingleUrl,
		"youtube_share_link":   data.YoutubeShareLink,
		"twitter_share_link":   data.TwitterShareLink,
		"telegram_share_link":  data.TelegramShareLink,
		"instagram_share_link": data.InstagramShareLink,
		"updated_at":           time.Now().Unix(),
		"updated_uid":          adminId,
		"updated_name":         adminName,
	}
	query, _, _ := dialect.Update("tbl_operator_info").Set(&record).Where(g.Ex{"id": data.Id}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func ChannelApifeeUpdate(adminName, adminId string, data TblOperatorInfo) error {

	record := g.Record{
		"api_fee":      data.ApiFee,
		"updated_at":   time.Now().Unix(),
		"updated_uid":  adminId,
		"updated_name": adminName,
	}
	query, _, _ := dialect.Update("tbl_operator_info").Set(&record).Where(g.Ex{"id": data.Id}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MerchantLogin(deviceNo, operatorName, password string) (MerchantLoginResp, error) {

	rsp := MerchantLoginResp{}
	data := TblOperatorInfo{}
	t := dialect.From("tbl_operator_info")
	query, _, _ := t.Select(colsAgentChannel...).Where(g.Ex{"operator_name": operatorName}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return rsp, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	// 账号不存在提示
	if err == sql.ErrNoRows {
		return rsp, errors.New(helper.UserNotExist)
	}

	if password != data.Password {
		return rsp, errors.New(helper.UsernameOrPasswordErr)
	}

	if data.State == "0" {
		return rsp, errors.New(helper.NotEnabled)
	}

	token := fmt.Sprintf("admin:token:%s", data.Id)
	b, _ := helper.JsonMarshal(data)
	sid, err := session.AdminSet(b, token, deviceNo)
	if err != nil {
		fmt.Println("AdminSet = ", err)
		return rsp, errors.New(helper.SessionErr)
	}

	rsp.Token = sid
	rsp.OperatorName = operatorName
	rsp.Id = data.Id

	return rsp, nil
}
