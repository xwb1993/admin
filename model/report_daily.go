package model

import (
	"admin/contrib/helper"
	"database/sql"
	"fmt"
	"strconv"
)

func ReportUserDailyList(param ReportDailyParam, adminid string) (ReportUserDailyRetData, error) {

	where := "1=1"
	oderBy := "created_at"
	oderType := "desc"
	if param.Uid > 0 {
		where += " and a.uid=" + strconv.Itoa(param.Uid)
	}
	if param.StartTime != "" {
		where += " and a.created_at>'" + param.StartTime + "'"
	}
	if param.EndTime != "" {
		where += " and a.created_at<'" + param.EndTime + "'"
	}
	if param.OperatorId != "" {
		where += " and c.id=" + param.OperatorId
	}
	if param.BusinessId != "" {
		where += " and d.id=" + param.BusinessId
	}

	loginUser := GetLoginUser(adminid)
	if loginUser.Operator != "" {
		where += " and c.id=" + loginUser.Operator
	}
	if loginUser.Businsess != "" {
		where += " and d.id=" + loginUser.Businsess
	}
	if param.OderBY != "" {
		oderBy = param.OderBY
	}
	if param.OderType != "" {
		oderType = param.OderType
	}
	order := oderBy + " " + oderType
	join := " left join tbl_member_base as b on b.uid=a.uid"          //玩家表
	join += " left join tbl_operator_info as c on c.id=b.operator_id" //渠道表
	join += " left join tbl_business_info as d on d.id=b.business_id" //业务员表

	data := ReportUserDailyRetData{}
	offset := (param.Page - 1) * param.PageSize
	if param.Page == 1 {
		count := "select count(1) from tbl_report_user_daily as a " + join + " where " + where
		fmt.Println(count)
		err := meta.MerchantDB.Get(&data.T, count)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), count), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}
	field := "a.*," +
		"ifnull(b.username,'') username," +
		"ifnull(c.id,'') operator_id," +
		"ifnull(c.operator_name,'') operator_name," +
		"ifnull(d.id,'') business_id," +
		"ifnull(d.account_name,'') as business_name"
	query := "select " + field + " from tbl_report_user_daily  as a " + join + " where " + where + " order by " + order + " limit " + strconv.Itoa(param.PageSize) + " offset " + strconv.Itoa(offset)
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	//统计
	sfield := "ifnull(sum(deposit),0) deposit," +
		"ifnull(sum(withdraw),0) withdraw," +
		"ifnull(sum(running),0) running," +
		"ifnull(sum(game_round),0) game_round," +
		"ifnull(sum(game_tax),0) game_tax," +
		"ifnull(sum(game_winlost),0) game_winlost," +
		"ifnull(sum(turntable_bonus),0) turntable_bonus," +
		"ifnull(sum(proxy_invite_bonus),0) proxy_invite_bonus"
	query = "select " + sfield + " from tbl_report_user_daily  as a " + join + " where " + where
	fmt.Println(query)
	err = meta.MerchantDB.Get(&data.S, query)

	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	return data, nil
}

func ReportPlatformDailyList(param ReportDailyParam, adminid string) (ReportDailyRetData, error) {
	data := ReportDailyRetData{}
	where := "1=1"
	oderBy := "create_date"
	oderType := "desc"
	if param.Uid > 0 {
		where += " and a.uid=" + strconv.Itoa(param.Uid)
	}
	if param.StartTime != "" {
		where += " and a.create_date>'" + param.StartTime + "'"
	}
	if param.EndTime != "" {
		where += " and a.create_date<'" + param.EndTime + "'"
	}

	if param.OperatorId != "" {
		if param.IsBusiness == 1 {
			where += " and a.operator_id=" + param.OperatorId
		} else {
			where += " and a.business_id=0 and a.operator_id=" + param.OperatorId
		}
	}
	if param.BusinessId != "" {
		where += " and a.business_id=" + param.BusinessId
	}
	if param.OderBY != "" {
		oderBy = param.OderBY
	}
	if param.OderType != "" {
		oderType = param.OderType
	}
	order := oderBy + " " + oderType
	table := "tbl_report_daily"

	field := "a.*,ifnull(c.operator_name,'') operator_name,ifnull(d.account_name,'') business_name"
	join := " left join tbl_operator_info as c on c.id=a.operator_id" //渠道表
	join += " left join tbl_business_info as d on d.id=a.business_id" //业务员表

	loginUser := GetLoginUser(adminid)
	if loginUser.Operator != "" {
		if param.IsBusiness == 1 {
			//渠道查询业务员日况
			where += " and a.business_id>0 and a.operator_id=" + loginUser.Operator
		} else {
			where += " and a.business_id=0 and a.operator_id=" + loginUser.Operator
		}
	} else if loginUser.Businsess != "" {
		where += " and a.business_id in(" + GetBusinessTeam(loginUser.Businsess) + ")"
	} else {
		//总后台的日况，是所有渠道相加
		if param.IsOperator == 0 && param.IsBusiness == 0 {
			sumfield := "create_date,ifnull(sum(deposit_num),0) deposit_num," +
				"ifnull(sum(deposit),0) deposit," +
				"ifnull(sum(first_deposit_num),0) first_deposit_num," +
				"ifnull(sum(first_deposit),0) first_deposit," +
				"ifnull(sum(withdraw_num),0) withdraw_num," +
				"ifnull(sum(withdraw),0) withdraw," +
				"ifnull(sum(running),0) running," +
				"ifnull(sum(game_round),0) game_round," +
				"ifnull(sum(game_tax),0) game_tax," +
				"ifnull(sum(game_winlost),0) game_winlost," +
				"ifnull(sum(turntable_bonus),0) turntable_bonus," +
				"ifnull(sum(proxy_invite_bonus),0) proxy_invite_bonus"
			table = "(select " + sumfield + " from tbl_report_daily where business_id=0 and operator_id>0 group by create_date)"
			join = ""
			field = "a.*"
		}

		if param.IsOperator != 0 {
			//总后台查询渠道日况
			where += " and a.business_id=0 and a.operator_id>0"
		}
		if param.IsBusiness != 0 {
			//总后台查询业务员日况
			where += " and a.business_id>0"
		}
	}

	offset := (param.Page - 1) * param.PageSize
	if param.Page == 1 {
		count := "select count(1) from " + table + " as a  where " + where
		fmt.Println(count)
		err := meta.MerchantDB.Get(&data.T, count)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), count), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	query := "select " + field + " from " + table + " as a " + join + " where " + where + " order by " + order + " limit " + strconv.Itoa(param.PageSize) + " offset " + strconv.Itoa(offset)
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)

	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	//统计
	sfield := "ifnull(sum(deposit_num),0) deposit_num," +
		"ifnull(sum(deposit),0) deposit," +
		"ifnull(sum(first_deposit_num),0) first_deposit_num," +
		"ifnull(sum(first_deposit),0) first_deposit," +
		"ifnull(sum(withdraw_num),0) withdraw_num," +
		"ifnull(sum(withdraw),0) withdraw," +
		"ifnull(sum(running),0) running," +
		"ifnull(sum(game_round),0) game_round," +
		"ifnull(sum(game_tax),0) game_tax," +
		"ifnull(sum(game_winlost),0) game_winlost," +
		"ifnull(sum(turntable_bonus),0) turntable_bonus," +
		"ifnull(sum(proxy_invite_bonus),0) proxy_invite_bonus"
	query = "select " + sfield + " from " + table + " as a where " + where
	fmt.Println(query)
	err = meta.MerchantDB.Get(&data.S, query)

	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	return data, nil
}

func ReportGameDailyList(param GameDailyParam, adminid string) (ReportGameDailyRetData, error) {
	where := "1=1"
	oderBy := "create_date"
	oderType := "desc"
	if param.GameId > 0 {
		where += " and a.game_id=" + strconv.Itoa(param.GameId)
	}
	if param.StartTime != "" {
		where += " and a.create_date>'" + param.StartTime + "'"
	}
	if param.EndTime != "" {
		where += " and a.create_date<'" + param.EndTime + "'"
	}
	if param.OperatorId != "" {
		if param.IsBusiness == 1 {
			where += " and a.operator_id=" + param.OperatorId
		} else {
			where += " and a.business_id=0 and a.operator_id=" + param.OperatorId
		}
	}
	if param.BusinessId != "" {
		where += " and a.business_id=" + param.BusinessId
	}
	if param.OderBY != "" {
		oderBy = param.OderBY
	}
	if param.OderType != "" {
		oderType = param.OderType
	}
	table := "tbl_report_game_daily"
	loginUser := GetLoginUser(adminid)
	order := oderBy + " " + oderType
	field := "a.*,(1-(a.game_tax + a.game_winlost)/a.running) as rate,ifnull(b.name,'') game_name,ifnull(c.operator_name,'') operator_name,ifnull(d.account_name,'') business_name"

	join := " left join tbl_platforms as b on b.id=a.game_id"         //游戏表
	join += " left join tbl_operator_info as c on c.id=a.operator_id" //渠道表
	join += " left join tbl_business_info as d on d.id=a.business_id" //业务员表
	if loginUser.Operator != "" {
		if param.IsBusiness == 1 {
			//渠道查询业务员游戏日报
			where += " and a.business_id>0 and a.operator_id=" + loginUser.Operator
		} else {
			where += " and a.business_id=0 and a.operator_id=" + loginUser.Operator
		}
	} else if loginUser.Businsess != "" {
		where += " and a.business_id in(" + GetBusinessTeam(loginUser.Businsess) + ")"
	} else {
		//总后台的游戏日报
		if param.IsOperator == 0 && param.IsBusiness == 0 {
			sumfield := "game_id,create_date," +
				"ifnull(sum(running),0) running," +
				"ifnull(sum(game_round),0) game_round," +
				"ifnull(sum(game_tax),0) game_tax," +
				"ifnull(sum(game_winlost),0) game_winlost"
			table = "(select " + sumfield + " from tbl_report_game_daily where business_id=0 and operator_id>0 group by game_id,create_date)"
			join = "left join tbl_game_lists as b on b.id=a.game_id"
			field = "a.*,ifnull(b.name,'') game_name"
		}

		if param.IsOperator != 0 {
			//总后台查询渠道游戏日报
			where += " and a.business_id=0 and a.operator_id>0"
		}
		if param.IsBusiness != 0 {
			//总后台查询业务员游戏日报
			where += " and a.business_id>0"
		}
	}

	data := ReportGameDailyRetData{}
	offset := (param.Page - 1) * param.PageSize
	if param.Page == 1 {
		count := "select count(1) from " + table + " as a  where " + where
		fmt.Println(count)
		err := meta.MerchantDB.Get(&data.T, count)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), count), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	query := "select " + field + " from " + table + " as a " + join + " where " + where + " order by " + order + " limit " + strconv.Itoa(param.PageSize) + " offset " + strconv.Itoa(offset)
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)

	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	//统计
	sfield := "ifnull(sum(running),0) running," +
		"ifnull(sum(game_round),0) game_round," +
		"ifnull(sum(game_tax),0) game_tax," +
		"ifnull(sum(game_winlost),0) game_winlost"
	query = "select " + sfield + " from " + table + " as a where " + where
	fmt.Println(query)
	err = meta.MerchantDB.Get(&data.S, query)

	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	return data, nil
}

func ProxyDailyList(param ReportProxyDailyParam, adminid string) (ReportProxyDailyRetData, error) {
	data := ReportProxyDailyRetData{}
	where := "1=1"
	oderBy := "create_date"
	oderType := "desc"
	if param.ProxyId != "" {
		where += " and a.proxy_id=" + param.ProxyId
	}
	if param.ParentId > 0 {
		where += " and b.parent_id=" + strconv.Itoa(param.ParentId)
	}
	if param.StartTime != "" {
		where += " and a.create_date>'" + param.StartTime + "'"
	}
	if param.EndTime != "" {
		where += " and a.create_date<'" + param.EndTime + "'"
	}
	if param.OperatorId != "" {
		where += " and c.id=" + param.OperatorId
	}
	if param.BusinessId != "" {
		where += " and d.id=" + param.BusinessId
	}
	if param.OderBY != "" {
		oderBy = param.OderBY
	}
	if param.OderType != "" {
		oderType = param.OderType
	}

	loginUser := GetLoginUser(adminid)
	if loginUser.Operator != "" {
		where += " and c.id=" + loginUser.Operator
	}
	if loginUser.Businsess != "" {
		where += " and d.id=" + loginUser.Businsess
	}

	join := " left join tbl_member_base as b on b.uid=a.uid"          //玩家表
	join += " left join tbl_operator_info as c on c.id=b.operator_id" //渠道表
	join += " left join tbl_business_info as d on d.id=b.business_id" //业务员表
	order := oderBy + " " + oderType

	offset := (param.Page - 1) * param.PageSize
	if param.Page == 1 {
		count := "select count(1) from tbl_report_proxy_daily as a " + join + " where " + where
		fmt.Println(count)
		err := meta.MerchantDB.Get(&data.T, count)
		if err != nil {
			fmt.Println(err)
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), count), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}
	field := "a.*," +
		"ifnull(b.username,'') username," +
		"ifnull(c.id,'') operator_id," +
		"ifnull(c.operator_name,'') operator_name," +
		"ifnull(d.id,'') business_id," +
		"ifnull(d.account_name,'') as business_name"
	query := "select " + field + " from tbl_report_proxy_daily  as a " + join + " where " + where + " order by " + order + " limit " + strconv.Itoa(param.PageSize) + " offset " + strconv.Itoa(offset)
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		fmt.Println(err)
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	return data, nil
}
