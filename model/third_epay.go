package model

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type thirdEpay struct {
}
type EpayCallBack struct {
	MchId      int    `json:"mch_id"`
	Code       int    `json:"code"`
	OrderNo    string `json:"order_no"`
	DisOrderNo string `json:"dis_order_no"`
	OrderPrice int    `json:"order_price"`
	RealPrice  int    `json:"real_price"`
	OrderCost  int    `json:"order_cost"`
	NtiTime    int    `json:"nti_time"`
	Sign       string `json:"sign"`
}

type EpayWithdrawCallBack struct {
	OrderNo    string `json:"order_no"`
	MchOrderNo string `json:"mch_order_no"`
	Amount     int    `json:"amount"`
	Status     int    `json:"status"`
	Fee        int    `json:"fee"`
	Sign       int64  `json:"sign"`
}

type withdrawResp struct {
	Code            string `json:"code"`
	Msg             string `json:"msg"`
	OrdStatus       string `json:"ordStatus"`
	MerchantOrderId string `json:"merchantOrderId"`
	Amount          string `json:"amount"`
	Time            string `json:"time"`
	Order           string `json:"order"`
	Sign            string `json:"sign"`
}

func (that *thirdEpay) DepositCallBack(fctx *fasthttp.RequestCtx) (map[string]string, error) {

	m := map[string]string{}
	params := EpayCallBack{}
	fmt.Println(string(fctx.PostBody()))
	if err := helper.JsonUnmarshal(fctx.PostBody(), &params); err != nil {
		fmt.Println("epay param format err:", err.Error())
		return m, errors.New(helper.ParamErr)
	}
	fmt.Println(params)
	m["order_id"] = params.OrderNo
	m["amount"] = fmt.Sprintf(`%d`, params.OrderPrice)
	m["success_time"] = fmt.Sprintf(`%d`, params.NtiTime)
	if params.Code != 1 {
		m["state"] = fmt.Sprintf(`%d`, DepositCancelled)
	} else {
		m["state"] = fmt.Sprintf(`%d`, DepositSuccess)
	}

	return m, nil
}

//func (that *thirdEpay) Withdraw(fctx *fasthttp.RequestCtx, ts time.Time, uid, amount, orderNo, accountNo, accountName, pixId, flag string, p TblPayFactory) (paymentWithdrawResp, error) {
//
//	var (
//		iamount     int64
//		accountType = int64(0)
//		pixType     string
//	)
//	if flag == "1" {
//		pixType = "CPF"
//		accountType = 1007
//	} else if flag == "2" {
//		pixType = "PHONE"
//		accountNo = "+55" + accountNo
//		accountType = 1003
//	} else if flag == "3" {
//		pixType = "EMAIL"
//		accountType = 1003
//	}
//	//accountType = 1012
//
//	da, err := decimal.NewFromString(amount)
//	if err != nil {
//		fmt.Println("err:", err)
//		return paymentWithdrawResp{}, errors.New(helper.AmountErr)
//	}
//	iamount = da.IntPart()
//
//	dp, err := withdrawEpayOrder(accountType, iamount*10000, p.Mchid, accountNo, accountName, pixId, pixType, p.Url, p.AppKey, orderNo, meta.Callback+"ew")
//	if err != nil {
//		fmt.Println("err:", err)
//		return dp, err
//	}
//	return dp, nil
//}

func (that *thirdEpay) WithdrawCallBack(fctx *fasthttp.RequestCtx) (paymentCallbackResp, error) {

	data := paymentCallbackResp{}
	fmt.Println("withdrawReq===", string(fctx.PostBody()))
	data.AppId = string(fctx.FormValue("appId"))
	data.CustId = string(fctx.FormValue("custId"))
	data.MerchantOrderId = string(fctx.FormValue("merchantOrderId"))
	data.Order = string(fctx.FormValue("order"))
	data.OrderStatus = string(fctx.FormValue("orderStatus"))
	data.Amount = string(fctx.FormValue("amount"))
	data.Sign = string(fctx.FormValue("sign"))
	fmt.Println("WithdrawCallBack:", data)

	content := fmt.Sprintf(`amount=%s&appId=%s&custId=%s&merchantOrderId=%s&order=%s&orderStatus=%s&key=%s`, data.Amount, data.AppId, data.CustId, data.MerchantOrderId, data.Order, data.OrderStatus, meta.TgPay.AppKey)
	fmt.Println(content)
	sign := strings.ToUpper(helper.MD5Hash(content))
	fmt.Println("wdcb_sign==", sign)
	if data.Sign != sign {
		fmt.Printf("old sign=%s, new state=%s", data.Sign, sign)
		return data, errors.New(helper.SignValidErr)
	}
	return data, nil
}

func (that *thirdEpay) Deposit(fctx *fasthttp.RequestCtx, ts time.Time, uid, amount, orderNo string, p TblPayFactory) (paymentDepositResp, error) {

	ip := helper.FromRequest(fctx)

	var (
		mid     int64
		appid   int64
		paycode int64
		iamount int64
	)
	if validator.CtypeDigit(p.Mchid) {
		mid, _ = strconv.ParseInt(p.Mchid, 10, 64)
	}
	if validator.CtypeDigit(p.AppId) {
		appid, _ = strconv.ParseInt(p.AppId, 10, 64)
	}
	if validator.CtypeDigit(p.PayCode) {
		paycode, _ = strconv.ParseInt(p.PayCode, 10, 64)
	}
	if validator.CtypeDigit(amount) {
		iamount, _ = strconv.ParseInt(amount, 10, 64)
	}

	return createEpayOrder(mid, appid, paycode, iamount*100, p.Url, p.AppKey, orderNo, ip, uid, meta.Callback+"epay", meta.WebUrl)
}

func createEpayOrder(mchid, appId, payCode, amount int64, url, appKey, orderId, ip, uid, payNoticeUrl, payJumpUrl string) (paymentDepositResp, error) {

	resp := paymentDepositResp{
		OrderID: orderId,
	}
	ts := time.Now().Unix()
	content := strings.ToUpper(fmt.Sprintf(`app_id=%d&mch_id=%d&order_no=%s&pay_code=%d&pay_jump_url=%s&pay_notice_url=%s&price=%d&time=%d&key=%s`, appId, mchid, orderId, payCode, payJumpUrl, payNoticeUrl, amount, ts, appKey))
	fmt.Println(content)
	sign := strings.ToLower(helper.MD5Hash(content))
	recs := map[string]interface{}{
		"mch_id":         mchid,
		"pay_code":       payCode,
		"order_no":       orderId,
		"price":          amount,
		"app_id":         appId,
		"user_ip":        ip,
		"user_id":        uid,
		"pay_notice_url": payNoticeUrl,
		"pay_jump_url":   payJumpUrl,
		"time":           fmt.Sprintf(`%d`, ts),
		"sign":           sign,
	}
	requestBody, err := helper.JsonMarshal(recs)
	if err != nil {
		return resp, errors.New(helper.FormatErr)
	}
	fmt.Println(string(requestBody))
	header := map[string]string{
		"Content-Type": "application/json",
	}
	requestURI := fmt.Sprintf("%s/CreateOrder", url)
	fmt.Println(requestURI)
	body, statusCode, err := httpDoTimeout(requestBody, "POST", requestURI, header, time.Second*8)
	if err != nil || statusCode != 200 {
		fmt.Println("statusCode:", statusCode)
		fmt.Println("response:", string(body))
		fmt.Println("err:", err)
		return resp, errors.New(helper.PayServerErr)
	}
	fmt.Println(string(body))

	var p fastjson.Parser
	v, err := p.ParseBytes(body)
	if err != nil {
		return resp, fmt.Errorf(helper.PayServerErr)
	}

	if v.GetInt("code") != 0 {
		return resp, fmt.Errorf(helper.PayServerErr)
	}
	d := v.Get("data")
	resp.Addr = string(d.GetStringBytes("pay_url"))
	resp.Oid = string(d.GetStringBytes("Dis_order_no"))

	return resp, nil
}

func withdrawEpayOrder(iamount float64, accountNo, uid, pixId, pixType, orderId string, pay TblPayFactory) (withdrawResp, error) {
	//resp := paymentWithdrawResp{
	//	OrderID: orderId,
	//}
	var data withdrawResp
	var amount int64
	iamount = iamount * 100
	amount = int64(iamount)
	content := fmt.Sprintf(`amount=%d&appId=%s&backUrl=%s&cardType=%s&countryCode=%s&cpf=%s&currencyCode=%s&custId=%s&email=%s&merchantOrderId=%s&phone=%s&remark=%s&type=%s&userName=%s&walletId=%s&key=%s`, amount, pay.AppId, pay.NotifyUrl, pixType, pay.CountryCode, accountNo, pay.CurrencyCode, pay.Mchid, "email", orderId, "phone", pay.PayCode, "PIX", uid, pixId, pay.AppKey)
	fmt.Println(content)
	sign := strings.ToUpper(helper.MD5Hash(content))
	fmt.Println(sign)
	requestURI := fmt.Sprintf("%s", pay.Url)
	fmt.Println(requestURI)

	recs := map[string]interface{}{
		"amount":          amount,
		"appId":           pay.AppId,
		"backUrl":         pay.NotifyUrl,
		"cardType":        pixType,
		"countryCode":     pay.CountryCode,
		"cpf":             accountNo,
		"currencyCode":    pay.CurrencyCode,
		"custId":          pay.Mchid,
		"email":           "email",
		"merchantOrderId": orderId,
		"phone":           "phone",
		"remark":          pay.PayCode,
		"type":            "PIX",
		"userName":        uid,
		"walletId":        pixId,
		"sign":            sign,
	}
	requestBody, err := helper.JsonMarshal(recs)
	if err != nil {
		return data, errors.New(helper.FormatErr)
	}
	fmt.Println(string(requestBody))
	//payload := strings.NewReader(fmt.Sprintf(`&amount=%s&appId=%s&backUrl=%s&cardType=%s&countryCode=%s&cpf=%s&currencyCode=%s&custId=%s&email=%s&merchantOrderId=%s&phone=%s&remark=%s&type=%s&userName=%s&walletId=%s&sign=%s`, amount, pay.AppId, pay.NotifyUrl, pixType, pay.CountryCode, accountNo, pay.CurrencyCode, pay.Mchid, "email", orderId, "phone", pay.PayCode, "PIX", username, pixId, sign))

	client := &http.Client{}
	req, err := http.NewRequest("POST", requestURI, bytes.NewReader(requestBody))

	if err != nil {
		fmt.Println("这里报错1")
		return data, errors.New(helper.PayServerErr)
	}
	req.Header.Add("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println("这里报错2")
		return data, errors.New(helper.PayServerErr)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("这里报错3")
		return data, errors.New(helper.PayServerErr)
	}
	fmt.Println(string(body))

	err = json.Unmarshal(body, &data)
	if err != nil {
		return data, fmt.Errorf(helper.PayServerErr)
	}
	fmt.Println("data===", data)

	return data, nil
}
