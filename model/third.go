package model

import (
	"time"

	"github.com/valyala/fasthttp"
)

type third interface {
	// 发起三方代收
	Deposit(fctx *fasthttp.RequestCtx, ts time.Time, uid, amount, orderNo string, p TblPayFactory) (paymentDepositResp, error)
	// 三方代收回调
	DepositCallBack(fctx *fasthttp.RequestCtx) (map[string]string, error)
	// 三方代付
	Withdraw(fctx *fasthttp.RequestCtx, ts time.Time, uid, amount, orderNo, accountNo, accountName, pixId, flag string, p TblPayFactory) (paymentWithdrawResp, error)
	// 三方代付回调
	WithdrawCallBack(fctx *fasthttp.RequestCtx) (map[string]string, error)
}

var (
	epay = new(thirdEpay)
)

//var thirdFuncCb = map[string]third{
//	"778889999": epay,
//}
