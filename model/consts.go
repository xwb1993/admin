package model

import "time"

// 常量定义
// 动态验证码的有效时间
const (
	otpTimeout = 60
)
const (
	apiTimeOut = time.Second * 8
)

// 存款状态
const (
	DepositConfirming    = 361 //确认中
	DepositSuccess       = 362 //存款成功
	DepositCancelled     = 363 //存款已取消
	DepositReviewing     = 364 //存款审核中
	DepositRepairSuccess = 365 //补单成功
)

// 取款状态
const (
	WithdrawReviewing     = 371 //审核中
	WithdrawReviewReject  = 372 //审核拒绝
	WithdrawDealing       = 373 //出款中
	WithdrawSuccess       = 374 //提款成功
	WithdrawFailed        = 375 //出款失败
	WithdrawAbnormal      = 376 //异常订单
	WithdrawAutoPayFailed = 377 // 代付失败
	WithdrawHangup        = 378 // 挂起
	WithdrawDispatched    = 379 // 已派单
	WithdrawAutoPaying    = 380 // 三方出款中

	// 通过redis锁定提款订单的key
	withdrawOrderLockKey = "w:order:%s"
	// 通过redis锁定提款订单的key
	depositOrderLockKey   = "d:order:%s"
	defaultRedisKeyPrefix = "rlock:"
	LockTimeout           = 20 * time.Second
)

// 后台上下分审核状态
const (
	AdjustReviewing    = "1" //后台调整审核中
	AdjustReviewPass   = "2" //后台调整审核通过
	AdjustReviewReject = "3" //后台调整审核不通过
)

// 后台调整类型
const (
	AdjustUpMode   = 1 // 上分
	AdjustDownMode = 2 // 下分
)

// 中心钱包上下分
const (
	DownPointApply       = 0 //下分申请
	DownPointApplyPass   = 1 //下分申请通过
	DownPointApplyReject = 2 //下分申请拒绝
	UpPointApplyPass     = 3 //上分申请成功
)

var (
	betTimeFlags = map[string]string{
		"1": "bet_time",
		"2": "settle_time",
		"3": "start_time",
	}
)
