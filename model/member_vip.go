package model

import (
	"admin/contrib/helper"
	"database/sql"
	"errors"
	"fmt"
	"github.com/shopspring/decimal"
	"time"

	g "github.com/doug-martin/goqu/v9"
)

func MemberVipInsert(data MemberVip) error {

	query, _, _ := dialect.Insert("tbl_member_vip").Rows(&data).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberVipValidate(vip int, depositAmount, flow int64) error {

	var (
		data []MemberVip
		vips = []int{vip + 1}
	)
	if vip > 0 {
		vips = append(vips, vip-1)
	}
	query, _, _ := dialect.From("tbl_member_vip").Select(colsMemberVip...).Where(g.Ex{"vip": vips}).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	for _, v := range data {
		if v.Vip == vip-1 {
			if v.DepositAmount >= depositAmount || v.Flow >= flow {
				return errors.New(helper.ParamErr)
			}
		}
		if v.Vip == vip+1 {
			if v.DepositAmount <= depositAmount || v.Flow <= flow {
				return errors.New(helper.ParamErr)
			}
		}
	}

	return nil
}

func MemberVipUpdate(vip int, data MemberVip) error {
	amount, err := decimal.NewFromString(data.Amount)
	if err != nil {
		return pushLog(fmt.Errorf("%s", err.Error()), helper.ParamErr)
	}
	rebateRate, err := decimal.NewFromString(data.RebateRate)
	if err != nil {
		return pushLog(fmt.Errorf("%s", err.Error()), helper.ParamErr)
	}
	record := g.Record{
		"name":                    data.Name,
		"deposit_amount":          data.DepositAmount,
		"flow":                    data.Flow,
		"amount":                  amount,
		"week_award":              data.WeekAward,
		"month_award":             data.MonthAward,
		"withdraw_rate":           data.WithdrawRate,
		"free_withdraw_num":       data.FreeWithdrawNum,
		"withdraw_single_limit":   data.WithdrawSingleLimit,
		"withdraw_limit":          data.WithdrawLimit,
		"charge_extra_award_rate": data.ChargeExtraAwardRate,
		"rebate_rate":             rebateRate,
		"props":                   data.Props,
		"updated_at":              time.Now().Unix(),
	}
	query, _, _ := dialect.Update("tbl_member_vip").Set(record).Where(g.Ex{"vip": vip}).ToSQL()
	fmt.Println(query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberVipList(page, pageSize uint, ex g.Ex) (MemberVipData, error) {

	data := MemberVipData{}
	t := dialect.From("tbl_member_vip")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsMemberVip...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("vip").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func getVip(vip int) (MemberVip, error) {

	var data MemberVip
	query, _, _ := dialect.From("tbl_member_vip").
		Select(colsMemberVip...).Where(g.Ex{"vip": vip}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}
