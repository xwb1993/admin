package model

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	ryrpc "admin/rpc"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/shopspring/decimal"
	"net/url"
	"strconv"
	"time"
)

type depositTotal struct {
	T sql.NullInt64   `json:"t"`
	S sql.NullFloat64 `json:"s"`
	D sql.NullFloat64 `json:"d"`
	U sql.NullFloat64 `json:"u"`
	A sql.NullFloat64 `json:"a"`
}
type tblDeposit struct {
	Id           string  `json:"id" db:"id" cbor:"id"`
	Oid          string  `json:"oid" db:"oid" cbor:"oid"`
	Uid          string  `json:"uid" db:"uid" cbor:"uid"`
	ParentId     string  `json:"parent_id" db:"parent_id" cbor:"parent_id"`
	ParentName   string  `json:"parent_name" db:"parent_name" cbor:"parent_name"`
	Username     string  `json:"username" db:"username" cbor:"username"`
	Fid          string  `json:"fid" db:"fid" cbor:"fid"`
	Fname        string  `json:"fname" db:"fname" cbor:"fname"`
	Amount       string  `json:"amount" db:"amount" cbor:"amount"`
	State        int     `json:"state" db:"state" cbor:"state"`
	CreatedAt    int64   `json:"created_at" db:"created_at" cbor:"created_at"`
	CreatedUid   string  `json:"created_uid" db:"created_uid" cbor:"created_uid"`
	CreatedName  string  `json:"created_name" db:"created_name" cbor:"created_name"`
	ConfirmAt    int64   `json:"confirm_at" db:"confirm_at" cbor:"confirm_at"`
	ConfirmUid   string  `json:"confirm_uid" db:"confirm_uid" cbor:"confirm_uid"`
	ConfirmName  string  `json:"confirm_name" db:"confirm_name" cbor:"confirm_name"`
	ReviewRemark string  `json:"review_remark" db:"review_remark" cbor:"review_remark"`
	TopId        string  `json:"top_id" db:"top_id" cbor:"top_id"`
	TopName      string  `json:"top_name" db:"top_name" cbor:"top_name"`
	Level        int     `json:"level" db:"level" cbor:"level"`
	Discount     float64 `json:"discount" db:"discount" cbor:"discount"`
	Tester       int     `json:"tester" db:"tester" cbor:"tester"`
	SuccessTime  int     `json:"success_time" db:"success_time" cbor:"success_time"`
	UsdtRate     float64 `json:"usdt_rate" db:"usdt_rate" cbor:"usdt_rate"`
	UsdtCount    float64 `json:"usdt_count" db:"usdt_count" cbor:"usdt_count"`
}

// 存款数据
type DepositData struct {
	T   int64             `json:"t"`
	D   []tblDeposit      `json:"d"`
	Agg map[string]string `json:"agg"`
}

// DepositList 存款订单列表
func DepositList(ex g.Ex, startTime, endTime string, page, pageSize int) (DepositData, error) {

	ex["tester"] = []int{1, 3}

	data := DepositData{}

	if startTime != "" && endTime != "" {

		startAt, err := helper.TimeToLoc(startTime, loc)
		if err != nil {
			return data, errors.New(helper.DateTimeErr)
		}

		endAt, err := helper.TimeToLoc(endTime, loc)
		if err != nil {
			return data, errors.New(helper.DateTimeErr)
		}

		ex["created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
	}

	if page == 1 {

		total := depositTotal{}
		countQuery, _, _ := dialect.From("tbl_deposit").Select(g.COUNT(1).As("t"), g.SUM("amount").As("s"), g.SUM("discount").As("d"), g.SUM("usdt_count").As("u")).Where(ex).ToSQL()
		err := meta.MerchantDB.Get(&total, countQuery)
		if err != nil {
			return data, pushLog(err, helper.DBErr)
		}

		if total.T.Int64 < 1 {
			return data, nil
		}

		data.Agg = map[string]string{
			"amount":    fmt.Sprintf("%.4f", total.S.Float64),
			"discount":  fmt.Sprintf("%.4f", total.D.Float64),
			"usdtCount": fmt.Sprintf("%.4f", total.U.Float64),
		}

		// 充值到账金额
		ex["state"] = 362
		countQuery, _, _ = dialect.From("tbl_deposit").Select(g.COUNT(1).As("t"), g.SUM("amount").As("a")).Where(ex).ToSQL()
		err = meta.MerchantDB.Get(&total, countQuery)
		if err != nil {
			return data, pushLog(err, helper.DBErr)
		}
		data.Agg["realAmount"] = fmt.Sprintf("%.4f", total.A.Float64)

		data.T = total.T.Int64
	}

	offset := uint((page - 1) * pageSize)
	query, _, _ := dialect.From("tbl_deposit").Select(colsDeposit...).
		Where(ex).Offset(offset).Limit(uint(pageSize)).Order(g.C("created_at").Desc()).ToSQL()

	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(err, helper.DBErr)
	}

	var ids []string
	for _, v := range data.D {
		ids = append(ids, v.Uid)
	}

	return data, nil
}

// DepositHistory 存款历史列表
func DepositHistory(uid, username, parentName, id, fid, oid, state,
	minAmount, maxAmount, startTime, endTime, sortField string, timeFlag uint8, page, pageSize, dty, isAsc int) (DepositData, error) {

	data := DepositData{}

	startAt, err := helper.TimeToLoc(startTime, loc)
	if err != nil {
		return data, errors.New(helper.DateTimeErr)
	}

	endAt, err := helper.TimeToLoc(endTime, loc)
	if err != nil {
		return data, errors.New(helper.DateTimeErr)
	}
	ex := g.Ex{
		"tester": []int{1, 3},
	}
	if uid != "" {
		ex["uid"] = uid
	}
	if username != "" {
		ex["username"] = username
	}
	if parentName != "" {
		ex["parent_name"] = parentName
	}

	if dty > 0 {
		ex["success_time"] = dty
	}

	if timeFlag == 1 {
		ex["created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
	} else {
		ex["confirm_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
	}

	if id != "" {
		ex["id"] = id
	}

	if fid != "" {
		ex["fid"] = fid
	}

	if oid != "" {
		ex["oid"] = oid
	}

	if state != "" && state != "0" {
		ex["state"] = state
	} else {
		ex["state"] = []int{DepositConfirming, DepositSuccess, DepositCancelled, DepositReviewing, DepositRepairSuccess}
	}

	ex["amount"] = g.Op{"gt": 0}

	if minAmount != "" && maxAmount != "" {
		minF, err := strconv.ParseFloat(minAmount, 64)
		if err != nil {
			return data, pushLog(err, helper.AmountErr)
		}

		maxF, err := strconv.ParseFloat(maxAmount, 64)
		if err != nil {
			return data, pushLog(err, helper.AmountErr)
		}

		ex["amount"] = g.Op{"between": exp.NewRangeVal(minF, maxF)}

	}

	if page == 1 {

		var total depositTotal
		query, _, _ := dialect.From("tbl_deposit").Select(g.COUNT(1).As("t"), g.SUM("amount").As("s")).Where(ex).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&total, query)
		if err != nil {
			return data, pushLog(err, helper.DBErr)
		}

		if total.T.Int64 < 1 {
			return data, nil
		}

		data.Agg = map[string]string{
			"amount": fmt.Sprintf("%.4f", total.S.Float64),
		}

		data.T = total.T.Int64
	}

	offset := uint((page - 1) * pageSize)
	orderField := g.L("username")
	if sortField != "" {
		orderField = g.L(sortField)
	} else {
		if timeFlag == 1 {
			orderField = g.L("created_at")
		} else {
			orderField = g.L("confirm_at")
		}
	}

	orderBy := orderField.Desc()
	if isAsc == 1 {
		orderBy = orderField.Asc()
	}
	query, _, _ := dialect.From("tbl_deposit").Select(colsDeposit...).
		Where(ex).Offset(offset).Limit(uint(pageSize)).Order(orderBy).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(err, helper.DBErr)
	}

	return data, nil
}

// MerchantDepositHistory 渠道存款历史列表
func MerchantDepositHistory(operatorId, uid, username, parentName, id, fid, oid, state,
	minAmount, maxAmount, startTime, endTime, sortField string, timeFlag uint8, page, pageSize, dty, isAsc int) (DepositData, error) {

	data := DepositData{}

	startAt, err := helper.TimeToLoc(startTime, loc)
	if err != nil {
		return data, errors.New(helper.DateTimeErr)
	}

	endAt, err := helper.TimeToLoc(endTime, loc)
	if err != nil {
		return data, errors.New(helper.DateTimeErr)
	}
	ex := g.Ex{
		"tester": []int{1, 3},
	}
	uids := make([]string, 0)
	if username != "" { // 用户名校验
		uName, _ := url.PathUnescape(username)
		if !validator.CheckUName(uName, 5, 50) {
			return data, errors.New(helper.UsernameErr)
		}
		mb, err := MemberFindByUnameAndOpeId(uName, operatorId)
		if err != nil {
			return data, errors.New(helper.UsernameErr)
		}
		ex["username"] = uName
		ex["uid"] = mb.Uid
	} else {
		members, err := MemberFindByOperatorId(operatorId)
		if err != nil {
			return data, errors.New(helper.OperatorIdErr)
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
			ex["uid"] = uids
		}
	}

	if parentName != "" {
		ex["parent_name"] = parentName
	}

	if dty > 0 {
		ex["success_time"] = dty
	}

	if timeFlag == 1 {
		ex["created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
	} else {
		ex["confirm_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
	}

	if id != "" {
		ex["id"] = id
	}

	if fid != "" {
		ex["fid"] = fid
	}

	if oid != "" {
		ex["oid"] = oid
	}

	if state != "" && state != "0" {
		ex["state"] = state
	} else {
		ex["state"] = []int{DepositConfirming, DepositSuccess, DepositCancelled, DepositReviewing, DepositRepairSuccess}
	}

	ex["amount"] = g.Op{"gt": 0}

	if minAmount != "" && maxAmount != "" {
		minF, err := strconv.ParseFloat(minAmount, 64)
		if err != nil {
			return data, pushLog(err, helper.AmountErr)
		}

		maxF, err := strconv.ParseFloat(maxAmount, 64)
		if err != nil {
			return data, pushLog(err, helper.AmountErr)
		}

		ex["amount"] = g.Op{"between": exp.NewRangeVal(minF, maxF)}

	}

	if page == 1 {

		var total depositTotal
		query, _, _ := dialect.From("tbl_deposit").Select(g.COUNT(1).As("t"), g.SUM("amount").As("s")).Where(ex).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&total, query)
		if err != nil {
			return data, pushLog(err, helper.DBErr)
		}

		if total.T.Int64 < 1 {
			return data, nil
		}

		data.Agg = map[string]string{
			"amount": fmt.Sprintf("%.4f", total.S.Float64),
		}

		data.T = total.T.Int64
	}

	offset := uint((page - 1) * pageSize)
	orderField := g.L("username")
	if sortField != "" {
		orderField = g.L(sortField)
	} else {
		if timeFlag == 1 {
			orderField = g.L("created_at")
		} else {
			orderField = g.L("confirm_at")
		}
	}

	orderBy := orderField.Desc()
	if isAsc == 1 {
		orderBy = orderField.Asc()
	}
	query, _, _ := dialect.From("tbl_deposit").Select(colsDeposit...).
		Where(ex).Offset(offset).Limit(uint(pageSize)).Order(orderBy).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(err, helper.DBErr)
	}

	return data, nil
}

// BusinessDepositHistory 业务员存款历史列表
func BusinessDepositHistory(businessId, uid, username, parentName, id, fid, oid, state,
	minAmount, maxAmount, startTime, endTime, sortField string, timeFlag uint8, page, pageSize, dty, isAsc int) (DepositData, error) {

	data := DepositData{}

	startAt, err := helper.TimeToLoc(startTime, loc)
	if err != nil {
		return data, errors.New(helper.DateTimeErr)
	}

	endAt, err := helper.TimeToLoc(endTime, loc)
	if err != nil {
		return data, errors.New(helper.DateTimeErr)
	}
	ex := g.Ex{
		"tester": []int{1, 3},
	}
	uids := make([]string, 0)
	if username != "" { // 用户名校验
		uName, _ := url.PathUnescape(username)
		if !validator.CheckUName(uName, 5, 50) {
			return data, errors.New(helper.UsernameErr)
		}
		mb, err := MemberFindByUnameAndProId(uName, businessId)
		if err != nil {
			return data, errors.New(helper.UsernameErr)
		}
		ex["username"] = uName
		ex["uid"] = mb.Uid
	} else {
		members, err := MemberFindByBusinessId(businessId)
		if err != nil {
			return data, errors.New(helper.BusinessIdErr)
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
			ex["uid"] = uids
		}
	}

	if parentName != "" {
		ex["parent_name"] = parentName
	}

	if dty > 0 {
		ex["success_time"] = dty
	}

	if timeFlag == 1 {
		ex["created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
	} else {
		ex["confirm_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
	}

	if id != "" {
		ex["id"] = id
	}

	if fid != "" {
		ex["fid"] = fid
	}

	if oid != "" {
		ex["oid"] = oid
	}

	if state != "" && state != "0" {
		ex["state"] = state
	} else {
		ex["state"] = []int{DepositConfirming, DepositSuccess, DepositCancelled, DepositReviewing, DepositRepairSuccess}
	}

	ex["amount"] = g.Op{"gt": 0}

	if minAmount != "" && maxAmount != "" {
		minF, err := strconv.ParseFloat(minAmount, 64)
		if err != nil {
			return data, pushLog(err, helper.AmountErr)
		}

		maxF, err := strconv.ParseFloat(maxAmount, 64)
		if err != nil {
			return data, pushLog(err, helper.AmountErr)
		}

		ex["amount"] = g.Op{"between": exp.NewRangeVal(minF, maxF)}

	}

	if page == 1 {

		var total depositTotal
		query, _, _ := dialect.From("tbl_deposit").Select(g.COUNT(1).As("t"), g.SUM("amount").As("s")).Where(ex).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&total, query)
		if err != nil {
			return data, pushLog(err, helper.DBErr)
		}

		if total.T.Int64 < 1 {
			return data, nil
		}

		data.Agg = map[string]string{
			"amount": fmt.Sprintf("%.4f", total.S.Float64),
		}

		data.T = total.T.Int64
	}

	offset := uint((page - 1) * pageSize)
	orderField := g.L("username")
	if sortField != "" {
		orderField = g.L(sortField)
	} else {
		if timeFlag == 1 {
			orderField = g.L("created_at")
		} else {
			orderField = g.L("confirm_at")
		}
	}

	orderBy := orderField.Desc()
	if isAsc == 1 {
		orderBy = orderField.Asc()
	}
	query, _, _ := dialect.From("tbl_deposit").Select(colsDeposit...).
		Where(ex).Offset(offset).Limit(uint(pageSize)).Order(orderBy).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(err, helper.DBErr)
	}

	return data, nil
}

func DepositUpdate(orderId string, state int) error {
	// 查询订单
	ex := g.Ex{"id": orderId}
	order, err := depositOrderFindOne(ex)
	if err != nil {
		err = fmt.Errorf("query order error: [%v]", err)
		fmt.Println(err)
		return err
	}

	if order.State == DepositSuccess {
		err = fmt.Errorf("duplicated deposite notify: [%d]", order.State)
		fmt.Println(err)
		return err
	}

	// 修改订单状态
	err = depositUpdate(fmt.Sprintf("%d", state), order)
	if err != nil {
		fmt.Printf("set order state error: [%v], old state=%d, new state=%d", err, order.State, state)
		return err
	}

	return nil
}

// depositOrderFindOne 查询存款订单
func depositOrderFindOne(ex g.Ex) (tblDeposit, error) {

	order := tblDeposit{}
	query, _, _ := dialect.From("tbl_deposit").Select(colsDeposit...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&order, query)
	if err == sql.ErrNoRows {
		return order, errors.New(helper.OrderNotExist)
	}

	if err != nil {
		return order, pushLog(err, helper.DBErr)
	}

	return order, nil
}

// 修改订单状态
func depositUpdate(state string, order tblDeposit) error {

	// 加锁
	err := depositLock(order.Id)
	if err != nil {
		return err
	}
	defer depositUnLock(order.Id)

	// 充值成功处理订单状态
	if state == fmt.Sprintf(`%d`, DepositRepairSuccess) {
		_ = cacheDepositProcessingRem(order.Uid)
		err = depositUpPointSuccess(order.Id, order.Uid, "", "补单", state)
		if err != nil {
			fmt.Println("err:", err)
			return err
		}
	} else {
		err = depositUpPointCancel(order.Id, order.Uid, "", "", state)
		if err != nil {
			fmt.Println("err:", err)

			return err
		}
	}

	return nil
}

// depositLock 锁定充值订单 防止并发多充钱
func depositLock(id string) error {

	key := fmt.Sprintf(depositOrderLockKey, id)
	return Lock(key)
}

// depositUnLock 解锁充值订单
func depositUnLock(id string) {
	key := fmt.Sprintf(depositOrderLockKey, id)
	Unlock(key)
}

// cacheDepositProcessingRem 清除未成功的订单计数
func cacheDepositProcessingRem(uid string) error {

	automatic_lock_key := fmt.Sprintf("finance:alock:%s", uid)
	return meta.MerchantRedis.Unlink(ctx, automatic_lock_key).Err()
}

// 存款上分
func depositUpPointSuccess(did, uid, name, remark, state string) error {

	// 判断状态是否合法
	allow := map[string]bool{
		fmt.Sprintf(`%d`, DepositRepairSuccess): true,
	}
	if _, ok := allow[state]; !ok {
		return errors.New(helper.OrderStateErr)
	}

	// 判断订单是否存在
	ex := g.Ex{"id": did, "state": []int{DepositConfirming, DepositReviewing}}
	order, err := depositOrderFindOne(ex)
	if err != nil {
		return err
	}

	// 如果已经有一笔订单补单成功,则其他订单不允许补单成功
	if fmt.Sprintf(`%d`, DepositRepairSuccess) == state {
		// 这里的ex不能覆盖上面的ex
		_, err = depositOrderFindOne(g.Ex{"id": order.Id, "state": DepositRepairSuccess})
		if err != nil && err.Error() != helper.OrderNotExist {
			return err
		}

		if err == nil {
			return errors.New(helper.OrderExist)
		}
	}

	now := time.Now()
	//if payAt != "" {
	//	confirmAt, err := strconv.ParseInt(payAt, 10, 64)
	//	if err == nil {
	//		if len(payAt) == 13 {
	//			confirmAt = confirmAt / 1000
	//		}
	//		now = time.Unix(confirmAt, 0)
	//	}
	//}
	record := g.Record{
		"state":         state,
		"confirm_at":    now.Unix(),
		"confirm_uid":   uid,
		"confirm_name":  name,
		"review_remark": remark,
	}
	//查询用户是否首存
	user, err := MemberFindByUid(uid)
	if user.DepositAmount == 0 {
		record["success_time"] = 1
	}
	query, _, _ := dialect.Update("tbl_deposit").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	money, _ := decimal.NewFromString(order.Amount)
	amount := money.String()
	cashType := helper.TransactionDeposit
	if money.Cmp(decimal.Zero) == -1 {
		cashType = helper.TransactionFinanceDownPoint
		amount = money.Abs().String()
	}

	// 后面都是存款成功的处理
	// 1、查询用户额度
	balance, err := MemberBalanceFindOne(order.Uid)
	if err != nil {
		return err
	}
	brl := decimal.NewFromFloat(balance.Brl)

	balanceAfter := brl.Add(money)
	//dsc := depositSuccessCount(order.Uid)

	var (
		parent ryrpc.TblMemberBase
	)
	if user.ParentID != "0" {
		parent, _ = MemberFindByUid(user.ParentID)
	}
	//if user.GrandID != "0" {
	//	grand, _ = MemberFindOneByUid(user.GrandID)
	//}
	//if user.GreatGrandID != "0" {
	//	grandGrand, _ = MemberFindOneByUid(user.GreatGrandID)
	//}

	//var discount, bonusAmount decimal.Decimal
	//if order.Discount > 0 && user.DepositAmount == 0 {
	//	var pdc []PromoDepositConfig
	//	pdc, err = PromoDepositList(1)
	//	if err != nil {
	//		return err
	//	}
	//	for _, v := range pdc {
	//		if money.GreaterThanOrEqual(decimal.NewFromFloat(v.MinAmount)) {
	//			discount = decimal.NewFromFloat(v.Bonus).Div(decimal.NewFromInt(100))
	//		}
	//		//计算多少赠送礼金
	//		bonusAmount = money.Mul(discount)
	//		if v.MaxAmount > 0 {
	//			if bonusAmount.GreaterThanOrEqual(decimal.NewFromFloat(v.MaxAmount)) {
	//				bonusAmount = decimal.NewFromFloat(v.MaxAmount)
	//			}
	//		}
	//		break
	//	}
	//} else if order.Discount > 0 {
	//	var pdc []PromoDepositConfig
	//	pdc, err = PromoDepositList(2)
	//	if err != nil {
	//		return err
	//	}
	//	for _, v := range pdc {
	//		if money.GreaterThanOrEqual(decimal.NewFromFloat(v.MinAmount)) {
	//			discount = decimal.NewFromFloat(v.Bonus).Div(decimal.NewFromInt(100))
	//		}
	//		//计算多少赠送礼金
	//		bonusAmount = money.Mul(discount)
	//		if v.MaxAmount > 0 {
	//			if bonusAmount.GreaterThanOrEqual(decimal.NewFromFloat(v.MaxAmount)) {
	//				bonusAmount = decimal.NewFromFloat(v.MaxAmount)
	//			}
	//		}
	//		break
	//	}
	//}

	//是首存要给上级邀请奖金
	var firstDepositBonus float64
	tbc, err := getBonusConfig("invite")
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	rechargeAmount := money.InexactFloat64()
	if tbc.RechargeAmountLv1 <= rechargeAmount && rechargeAmount < tbc.RechargeAmountLv2 {
		firstDepositBonus = tbc.RateAmountLv1
	} else if tbc.RechargeAmountLv2 <= rechargeAmount && rechargeAmount < tbc.RechargeAmountLv3 {
		firstDepositBonus = tbc.RateAmountLv2
	} else if rechargeAmount >= tbc.RechargeAmountLv3 {
		firstDepositBonus = tbc.RateAmountLv3
	}

	// 开启事务
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	// 2、更新订单状态
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	bonusAmount := decimal.NewFromFloat(order.Discount)
	// 3、更新余额
	ex = g.Ex{
		"uid": order.Uid,
	}
	mbs := g.Record{
		"deposit_amount": g.L(fmt.Sprintf("deposit_amount+%s", money.String())),
	}
	//首存彩金进彩金钱包
	if user.DepositAmount == 0 {
		//查询后台转盘开关是否开启
		if TurntableSwitch() {
			// 2个小时内充值成功,赠送一次转盘机会
			if time.Since(time.Unix(order.ConfirmAt, 0)).Hours() <= 2 {
				info := g.Record{
					"unuse_chance": g.L(fmt.Sprintf("unuse_chance+%d", 1)),
				}
				query, _, _ = dialect.Update("tbl_pdd_turntable_info").Set(info).Where(g.Ex{"uid": uid}).ToSQL()
				fmt.Println(query)
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return pushLog(err, helper.DBErr)
				}
			}
		}
		if meta.WalletMode == "1" {
			br := g.Record{
				"brl":                 g.L(fmt.Sprintf("brl+%s", money.Add(bonusAmount).String())),
				"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%s", money.Add(bonusAmount).String())),
			}
			query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(ex).ToSQL()
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}

			// 4、新增账变记录
			id := helper.GenId()
			mbTrans := MemberTransaction{
				AfterAmount:  balanceAfter.String(),
				Amount:       amount,
				BeforeAmount: fmt.Sprintf(`%f`, balance.Brl),
				BillNo:       order.Id,
				CreatedAt:    time.Now().UnixMilli(),
				ID:           id,
				CashType:     cashType,
				UID:          order.Uid,
				Username:     order.Username,
				Remark:       "deposit",
			}

			query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTrans).ToSQL()
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}

			if user.CanBonus == 1 {
				// 6、赠送金额及账变
				bonusTrans := MemberTransaction{
					AfterAmount:  balanceAfter.Add(bonusAmount).String(),
					Amount:       bonusAmount.String(),
					BeforeAmount: balanceAfter.String(),
					BillNo:       order.Id,
					CreatedAt:    time.Now().UnixMilli(),
					ID:           helper.GenId(),
					CashType:     helper.TransactionDepositBonus,
					UID:          order.Uid,
					Username:     order.Username,
					Remark:       "deposit bonus",
				}

				query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(bonusTrans).ToSQL()
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return pushLog(err, helper.DBErr)
				}
			}

			mbs["first_deposit_amount"] = amount
			promoRecord := g.Record{
				"bonus_amount": firstDepositBonus,
				"state":        2,
			}
			promoRecord["first_deposit_at"] = now.Unix()
			promoRecord["settled_at"] = now.Unix()
			promoRecord["deposit_amount"] = amount
			query, _, _ = dialect.Update("tbl_promo_invite_record").Set(promoRecord).Where(g.Ex{"child_uid": user.Uid}).ToSQL()
			fmt.Println(query)
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}

			// 给上面三级邀请奖金
			if user.ParentID != "0" && parent.CanBonus == 1 {
				exparent := g.Ex{
					"uid": user.ParentID,
				}
				brparent := g.Record{
					"brl":                 g.L(fmt.Sprintf("brl+%f", firstDepositBonus)),
					"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%f", firstDepositBonus)),
				}
				query, _, _ = dialect.Update("tbl_member_balance").Set(brparent).Where(exparent).ToSQL()
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return pushLog(err, helper.DBErr)
				}
				parentBalance, err := MemberBalanceFindOne(user.ParentID)
				if err != nil {
					return err
				}
				parentBrl := decimal.NewFromFloat(parentBalance.Brl)
				if err != nil {
					return err
				}
				balAfter := parentBrl.Add(decimal.NewFromFloat(firstDepositBonus))
				// 上级新增账变记录
				parTrans := MemberTransaction{
					AfterAmount:  balAfter.String(),
					Amount:       fmt.Sprintf("%f", firstDepositBonus),
					BeforeAmount: parentBrl.String(),
					BillNo:       helper.GenId(),
					CreatedAt:    time.Now().UnixMilli(),
					ID:           helper.GenId(),
					CashType:     helper.TransactionInvitationBonus,
					UID:          user.ParentID,
					Username:     user.ParentName,
					Remark:       "invitation bonus",
				}

				query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(parTrans).ToSQL()
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return pushLog(err, helper.DBErr)
				}
			}
		} else if meta.WalletMode == "2" {
			br := g.Record{
				"brl":                 g.L(fmt.Sprintf("brl+%s", money.Add(bonusAmount).String())),
				"unlock_amount":       g.L(fmt.Sprintf("unlock_amount+%s", money.String())),
				"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%s", bonusAmount.String())),
			}
			query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(ex).ToSQL()
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}

			// 4、新增账变记录
			id := helper.GenId()
			mbTrans := MemberTransaction{
				AfterAmount:  balanceAfter.String(),
				Amount:       amount,
				BeforeAmount: fmt.Sprintf(`%f`, balance.Brl),
				BillNo:       order.Id,
				CreatedAt:    time.Now().UnixMilli(),
				ID:           id,
				CashType:     cashType,
				UID:          order.Uid,
				Username:     order.Username,
				Remark:       "deposit",
			}

			query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTrans).ToSQL()
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}

			if user.CanBonus == 1 {
				// 6、赠送金额及账变
				bonusTrans := MemberTransaction{
					AfterAmount:  balanceAfter.Add(bonusAmount).String(),
					Amount:       bonusAmount.String(),
					BeforeAmount: balanceAfter.String(),
					BillNo:       order.Id,
					CreatedAt:    time.Now().UnixMilli(),
					ID:           helper.GenId(),
					CashType:     helper.TransactionDepositBonus,
					UID:          order.Uid,
					Username:     order.Username,
					Remark:       "deposit bonus",
				}

				query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(bonusTrans).ToSQL()
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return pushLog(err, helper.DBErr)
				}
			}

			mbs["first_deposit_amount"] = amount
			promoRecord := g.Record{
				"bonus_amount": firstDepositBonus,
				"state":        2,
			}
			promoRecord["first_deposit_at"] = now.Unix()
			promoRecord["settled_at"] = now.Unix()
			promoRecord["deposit_amount"] = amount
			query, _, _ = dialect.Update("tbl_promo_invite_record").Set(promoRecord).Where(g.Ex{"child_uid": user.Uid}).ToSQL()
			fmt.Println(query)
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}

			// 给上面三级邀请奖金
			if user.ParentID != "0" && parent.CanBonus == 1 {
				exparent := g.Ex{
					"uid": user.ParentID,
				}
				brparent := g.Record{
					"brl":           g.L(fmt.Sprintf("brl+%f", firstDepositBonus)),
					"unlock_amount": g.L(fmt.Sprintf("unlock_amount+%f", firstDepositBonus)),
				}
				query, _, _ = dialect.Update("tbl_member_balance").Set(brparent).Where(exparent).ToSQL()
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return pushLog(err, helper.DBErr)
				}
				parentBalance, err := MemberBalanceFindOne(user.ParentID)
				if err != nil {
					return err
				}
				parentBrl := decimal.NewFromFloat(parentBalance.Brl)
				if err != nil {
					return err
				}
				balAfter := parentBrl.Add(decimal.NewFromFloat(firstDepositBonus))
				// 上级新增账变记录
				parTrans := MemberTransaction{
					AfterAmount:  balAfter.String(),
					Amount:       fmt.Sprintf("%f", firstDepositBonus),
					BeforeAmount: parentBrl.String(),
					BillNo:       helper.GenId(),
					CreatedAt:    time.Now().UnixMilli(),
					ID:           helper.GenId(),
					CashType:     helper.TransactionInvitationBonus,
					UID:          user.ParentID,
					Username:     user.ParentName,
					Remark:       "invitation bonus",
				}

				query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(parTrans).ToSQL()
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return pushLog(err, helper.DBErr)
				}
			}
		}
	} else {
		if meta.WalletMode == "1" {
			br := g.Record{
				"brl":                 g.L(fmt.Sprintf("brl+%s", money.Add(bonusAmount).String())),
				"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%s", money.Add(bonusAmount).String())),
			}
			query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(ex).ToSQL()
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}

			// 4、新增账变记录
			id := helper.GenId()
			mbTrans := MemberTransaction{
				AfterAmount:  balanceAfter.String(),
				Amount:       amount,
				BeforeAmount: fmt.Sprintf(`%f`, balance.Brl),
				BillNo:       order.Id,
				CreatedAt:    time.Now().UnixMilli(),
				ID:           id,
				CashType:     cashType,
				UID:          order.Uid,
				Username:     order.Username,
				Remark:       "deposit",
			}

			query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTrans).ToSQL()
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}

			if user.CanBonus == 1 {
				// 6、赠送金额及账变
				bonusTrans := MemberTransaction{
					AfterAmount:  balanceAfter.Add(bonusAmount).String(),
					Amount:       bonusAmount.String(),
					BeforeAmount: balanceAfter.String(),
					BillNo:       order.Id,
					CreatedAt:    time.Now().UnixMilli(),
					ID:           helper.GenId(),
					CashType:     helper.TransactionDepositBonus,
					UID:          order.Uid,
					Username:     order.Username,
					Remark:       "deposit bonus",
				}

				query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(bonusTrans).ToSQL()
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return pushLog(err, helper.DBErr)
				}
			}
		} else if meta.WalletMode == "2" {
			br := g.Record{
				"brl":                 g.L(fmt.Sprintf("brl+%s", money.Add(bonusAmount).String())),
				"unlock_amount":       g.L(fmt.Sprintf("unlock_amount+%s", money.String())),
				"deposit_lock_amount": g.L(fmt.Sprintf("deposit_lock_amount+%s", bonusAmount.String())),
			}
			query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(ex).ToSQL()
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}

			// 4、新增账变记录
			id := helper.GenId()
			mbTrans := MemberTransaction{
				AfterAmount:  balanceAfter.String(),
				Amount:       amount,
				BeforeAmount: fmt.Sprintf(`%f`, balance.Brl),
				BillNo:       order.Id,
				CreatedAt:    time.Now().UnixMilli(),
				ID:           id,
				CashType:     cashType,
				UID:          order.Uid,
				Username:     order.Username,
				Remark:       "deposit",
			}

			query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTrans).ToSQL()
			_, err = tx.Exec(query)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}

			if user.CanBonus == 1 {
				// 6、赠送金额及账变
				bonusTrans := MemberTransaction{
					AfterAmount:  balanceAfter.Add(bonusAmount).String(),
					Amount:       bonusAmount.String(),
					BeforeAmount: balanceAfter.String(),
					BillNo:       order.Id,
					CreatedAt:    time.Now().UnixMilli(),
					ID:           helper.GenId(),
					CashType:     helper.TransactionDepositBonus,
					UID:          order.Uid,
					Username:     order.Username,
					Remark:       "deposit bonus",
				}

				query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(bonusTrans).ToSQL()
				_, err = tx.Exec(query)
				if err != nil {
					_ = tx.Rollback()
					return pushLog(err, helper.DBErr)
				}
			}
		}
	}

	// 5、会员充值金额和积分增加
	ex = g.Ex{
		"uid": order.Uid,
	}

	query, _, _ = dialect.Update("tbl_member_base").Set(mbs).Where(ex).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}
	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	if user.Invited != 1 && user.ParentID != "" && user.ParentID != "0" {
		dsc := depositSuccessAmount(order.Uid)
		if dsc >= tbc.RechargeAmountLv1 {
			pt := g.Record{
				"invite_num": g.L("invite_num+1"),
			}
			query, _, _ = dialect.Update("tbl_member_base").Set(pt).Where(g.Ex{"uid": user.ParentID}).ToSQL()
			fmt.Println(query)
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(err, helper.DBErr)
			}
		}
	}

	//fmt.Println("uid", uid)
	//ryrpc.MemberFlushByUid(uid)
	return nil
}

// 存款上分
func depositUpPointCancel(did, uid, name, remark, state string) error {

	// 判断状态是否合法
	allow := map[string]bool{
		fmt.Sprintf(`%d`, DepositReviewing): true,
	}
	if _, ok := allow[state]; !ok {
		return errors.New(helper.OrderStateErr)
	}

	// 判断订单是否存在
	ex := g.Ex{"id": did, "state": []int{DepositConfirming, DepositReviewing}}
	order, err := depositOrderFindOne(ex)
	if err != nil {
		return err
	}

	// 如果已经有一笔订单补单成功,则其他订单不允许补单成功
	if fmt.Sprintf(`%d`, DepositSuccess) == state || fmt.Sprintf(`%d`, DepositRepairSuccess) == state {
		// 这里的ex不能覆盖上面的ex
		//_, err = depositOrderFindOne(g.Ex{"oid": order.Oid, "state": DepositSuccess})
		_, err = depositOrderFindOne(g.Ex{"id": order.Id, "state": []int{DepositSuccess, DepositRepairSuccess}})
		if err != nil && err.Error() != helper.OrderNotExist {
			return err
		}

		if err == nil {
			return errors.New(helper.OrderExist)
		}
	}

	now := time.Now()
	//if payAt != "" {
	//	confirmAt, err := strconv.ParseInt(payAt, 10, 64)
	//	if err == nil {
	//		if len(payAt) == 13 {
	//			confirmAt = confirmAt / 1000
	//		}
	//		now = time.Unix(confirmAt, 0)
	//	}
	//}
	record := g.Record{
		"state":         state,
		"confirm_at":    now.Unix(),
		"confirm_uid":   uid,
		"confirm_name":  name,
		"review_remark": remark,
	}
	query, _, _ := dialect.Update("tbl_deposit").Set(record).Where(ex).ToSQL()
	fmt.Println(query)
	// 存款失败 直接修改订单状态
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}

func depositSuccessAmount(uid string) float64 {

	var data sql.NullFloat64
	query, _, _ := dialect.From("tbl_deposit").Select("amount").Where(g.Ex{"uid": uid, "state": DepositRepairSuccess}).Order(g.C("amount").Desc()).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&data, query)
	fmt.Println(query)
	if errors.Is(err, sql.ErrNoRows) {
		return 0
	}

	if err != nil {
		return 0
	}

	return data.Float64
}
