package model

import (
	"admin/contrib/helper"
	"database/sql"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
)

type tblQuoteMange struct {
	Id                  uint32  `db:"id" json:"id" cbor:"id"`
	OperatorId          uint32  `db:"operator_id" json:"operator_id" cbor:"operator_id"`
	OperatorName        uint32  `db:"operator_name" json:"operator_name" cbor:"operator_name"`
	BalanceQuota        float64 `db:"balance_quota" json:"balance_quota" cbor:"balance_quota"`
	BalanceUsed         float64 `db:"balance_used" json:"balance_used" cbor:"balance_used"`
	CommissionQuota     float64 `db:"commission_quota" json:"commission_quota" cbor:"commission_quota"`
	TestMemberQuota     float64 `db:"test_member_quota" json:"test_member_quota" cbor:"test_member_quota"`
	TestMemberUsed      float64 `db:"test_member_used" json:"test_member_used" cbor:"test_member_used"`
	TestMemberNum       int64   `db:"test_member_num" json:"test_member_num" cbor:"test_member_num"`
	TestMemberNumUsed   int64   `db:"test_member_num_used" json:"test_member_num_used" cbor:"test_member_num_used"`
	AutoWithdrawalLimit float64 `db:"auto_withdrawal_limit" json:"auto_withdrawal_limit" cbor:"auto_withdrawal_limit"`
}

// 游戏记录数据
type OperatorQuoteData struct {
	T int64           `json:"t" cbor:"t"`
	D []tblQuoteMange `json:"d" cbor:"d"`
	S uint            `json:"s"`
}

// 运营商额度列表
func GetOpeartorQuoteList(page, pageSize int, ex g.Ex) (OperatorQuoteData, error) {
	data := OperatorQuoteData{}

	t := dialect.From("tbl_operator_quota_manage")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT("id")).Where(ex).ToSQL()
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil && err != sql.ErrNoRows {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
		if data.T == 0 {
			return data, nil
		}
	}
	offset := (page - 1) * pageSize
	var d []tblQuoteMange
	//query, _, _ := dialect.Select(g.T("tbl_operator_quota_manage").As("tm")).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	query, _, _ := dialect.Select("a.*", "tbl_operator_info.operator_name").From(g.T("tbl_operator_quota_manage").As("a")).LeftJoin(g.T("tbl_operator_info"),
		g.On(g.Ex{"a.operator_id": g.I("tbl_operator_info.id")})).Where(ex).Order(g.I("operator_id").Desc()).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&d, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	if len(d) == 0 {
		return data, nil
	}
	data.D = d
	data.S = uint(pageSize)
	return data, nil
}

type AddOperatorQuoteParam struct {
	OperatorId          int     `json:"operator_id" db:"operator_id" cbor:"operator_id"`
	BalanceQuota        float64 `json:"balance_quota" db:"balance_quota" cbor:"balance_quota"`
	CommissionQuota     float64 `json:"commission_quota" db:"commission_quota" cbor:"commission_quota"`
	TestMemberQuota     float64 `json:"test_member_quota" db:"test_member_quota" cbor:"test_member_quota"`
	TestMemberNum       float64 `json:"test_member_num" db:"test_member_num" cbor:"test_member_num"`
	AutoWithdrawalLimit float64 `json:"auto_withdrawal_limit" db:"auto_withdrawal_limit" cbor:"auto_withdrawal_limit"`
}

// /添加额度
func AddOperatorQuote(param AddOperatorQuoteParam) error {

	var total int
	t := dialect.From("tbl_operator_quota_manage")
	query, _, _ := t.Select(g.COUNT("id")).Where(g.Ex{"operator_id": param.OperatorId}).ToSQL()
	err := meta.MerchantDB.Get(&total, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	if total > 0 {
		query, _, _ := dialect.Update("tbl_operator_quota_manage").Set(&param).Where(g.Ex{"operator_id": param.OperatorId}).ToSQL()
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

	} else {
		query, _, _ := dialect.Insert("tbl_operator_quota_manage").Rows(param).ToSQL()
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
	}
	return nil
}
