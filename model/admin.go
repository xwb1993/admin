package model

import (
	"admin/contrib/helper"
	"admin/contrib/session"
	"database/sql"
	"errors"
	"fmt"
	"strconv"
	"time"

	g "github.com/doug-martin/goqu/v9"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fastjson"
)

// Admin 用户数据库结构体
type Admin struct {
	ID            string `cbor:"id" db:"id" json:"id"`                                        // 主键ID
	GroupID       string `cbor:"group_id" db:"group_id" json:"group_id"`                      // 用户组ID
	LastLoginIP   string `cbor:"last_login_ip" db:"last_login_ip"`                            // 最后登录IP
	LastLoginTime uint32 `cbor:"last_login_time" db:"last_login_time" json:"last_login_time"` // 最后登录时间
	Pwd           string `cbor:"password" db:"password" json:"password"`                      // 密码
	State         int    `cbor:"state" db:"state" json:"state"`                               // 状态
	Seamo         string `cbor:"seamo" db:"seamo" json:"seamo"`
	Name          string `cbor:"name" db:"name" json:"name"`                         // 用户名
	CreateAt      uint32 `cbor:"create_at" db:"create_at" json:"create_at"`          // 创建时间
	CreatedUid    string `cbor:"created_uid" db:"created_uid" json:"created_uid"`    //创建人uid
	CreatedName   string `cbor:"created_name" db:"created_name" json:"created_name"` //创建人名
	UpdatedAt     uint32 `cbor:"updated_at" db:"updated_at" json:"updated_at"`       // 修改时间
	UpdatedUid    string `cbor:"updated_uid" db:"updated_uid" json:"updated_uid"`    //修改人uid
	UpdatedName   string `cbor:"updated_name" db:"updated_name" json:"updated_name"` //修改人名
}

type AdminData struct {
	D []Admin `json:"d"`
	T int64   `json:"t"`
	S uint    `json:"s"`
}

// AdminLoginResp 用户登录返回数据结构体
type AdminLoginResp struct {
	Token     string `json:"token" cbor:"token"`          // 用户token
	Allows    string `json:"allows" cbor:"allows"`        // 用户权限列表
	AdminName string `json:"user_name" cbor:"admin_name"` // 用户名
	Domain    string `json:"domain" cbor:"domain"`        // 用户名
	GroupId   string `json:"group_id" cbor:"group_id"`    //
}

func AdminInsert(data Admin) error {

	v := MurmurHash(data.Pwd, data.CreateAt)

	data.Pwd = fmt.Sprintf("%d", v)
	data.ID = helper.GenId()
	data.LastLoginIP = ""
	data.LastLoginTime = 0

	query, _, _ := dialect.Insert("tbl_admins").Rows(data).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		body := fmt.Errorf("%s,[%s]", err.Error(), query)
		return pushLog(body, helper.DBErr)
	}

	//log := fmt.Sprintf("系统管理/系统账号/新增 后台账号[%s],新增账号[%s],权限组id[%s],状态[%s] 新增",
	//	data.CreatedName, data.Name, data.GroupID, data.State)
	//fmt.Println(adminLogInsert("system", log, "insert", data.CreatedName))

	return nil
}

func AdminUpdateState(adminID, adminName, id, state string) error {

	ex := g.Ex{
		"id": id,
	}
	data := Admin{}
	query, _, _ := dialect.From("tbl_admins").Select(colsAdmin...).Where(ex).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return errors.New(helper.RecordNotExistErr)
	}

	if state == fmt.Sprintf("%d", data.State) {
		return errors.New(helper.NoDataUpdate)
	}

	record := g.Record{
		"state":        state,
		"updated_at":   time.Now().Unix(),
		"updated_uid":  adminID,
		"updated_name": adminName,
	}
	query, _, _ = dialect.Update("tbl_admins").Set(record).Where(g.Ex{"id": id}).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	//if state == "0" {
	//	log := fmt.Sprintf("系统管理/系统账号/更新状态 后台账号[%s],被操作账号[%s] 关闭", adminName, data.Name)
	//	fmt.Println(adminLogInsert("system", log, "close", adminName))
	//} else {
	//	log := fmt.Sprintf("系统管理/系统账号/更新状态 后台账号[%s],被操作账号[%s] 开启", adminName, data.Name)
	//	fmt.Println(adminLogInsert("system", log, "open", adminName))
	//}

	return nil
}

func AdminUpdate(adminID, adminName, id, pwd, gid string, record g.Record) error {

	var gname string
	ex := g.Ex{
		"gid": gid,
	}
	query, _, _ := dialect.From("tbl_admin_group").Select("gname").Where(ex).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&gname, query)
	if err != nil && err != sql.ErrNoRows {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return errors.New(helper.GroupIDErr)
	}

	ex = g.Ex{
		"id": id,
	}
	data := Admin{}
	query, _, _ = dialect.From("tbl_admins").Select(colsAdmin...).Where(ex).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return errors.New(helper.RecordNotExistErr)
	}

	record["updated_uid"] = adminID
	record["updated_name"] = adminName

	if pwd != "" {
		v := MurmurHash(pwd, data.CreateAt)
		record["password"] = fmt.Sprintf("%d", v)
	}

	query, _, _ = dialect.Update("tbl_admins").Set(record).Where(g.Ex{"id": id}).ToSQL()
	fmt.Println(query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	//log := fmt.Sprintf("系统管理/系统账号/更新 后台账号[%s],被更新账号[%s],sql[%s] 更新", adminName, data.Name, query)
	//fmt.Println(adminLogInsert("system", log, "update", adminName))

	return nil
}

func AdminList(adminGid string, page, pageSize uint, ex g.Ex) (AdminData, error) {

	data := AdminData{}
	gids, gidMap, err := groupSubList(adminGid)
	if err != nil {
		return data, err
	}

	if len(gids) == 0 {
		return data, nil
	}

	if gid, ok := ex["group_id"].(string); ok {
		if _, ok = gidMap[gid]; !ok {
			return data, errors.New(helper.MethodNoPermission)
		}
	} else {
		ex["group_id"] = gids
	}

	t := dialect.From("tbl_admins")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			body := fmt.Errorf("%s,[%s]", err.Error(), query)
			return data, pushLog(body, helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsAdmin...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("create_at").Desc()).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		body := fmt.Errorf("%s,[%s]", err.Error(), query)
		return data, pushLog(body, helper.DBErr)
	}

	data.S = pageSize
	return data, nil

}

func AdminToken(ctx *fasthttp.RequestCtx) (map[string]string, error) {
	b := ctx.UserValue("token").([]byte)

	var p fastjson.Parser

	data := map[string]string{}
	v, err := p.ParseBytes(b)
	if err != nil {
		return data, err
	}

	o, err := v.Object()
	if err != nil {
		return data, err
	}

	o.Visit(func(k []byte, v *fastjson.Value) {
		key := string(k)
		val, err := v.StringBytes()
		if err == nil {
			data[key] = string(val)
		}
	})

	return data, nil
}

func AdminLogin(deviceNo, username, password, seamo, ip string, lastLoginTime time.Time) (AdminLoginResp, error) {

	rsp := AdminLoginResp{}
	data := Admin{}
	t := dialect.From("tbl_admins")
	query, _, _ := t.Select(colsAdmin...).Where(g.Ex{"name": username}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return rsp, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	// 账号不存在提示
	if err == sql.ErrNoRows {
		return rsp, errors.New(helper.UserNotExist)
	}

	if data.State == 0 {
		return rsp, errors.New(helper.Blocked)
	}

	if !meta.IsDev {
		slat := helper.TOTP(data.Seamo, otpTimeout)
		if s, err := strconv.Atoi(seamo); err != nil || s != slat {
			fmt.Println(err, s, slat)
			return rsp, errors.New(helper.DynamicVerificationCodeErr)
		}
	}

	//暂时先注释
	//pwd := fmt.Sprintf("%d", MurmurHash(password, data.CreateAt))
	//if pwd != data.Pwd {
	//	return rsp, errors.New(helper.UsernameOrPasswordErr)
	//}
	if password != data.Pwd {
		return rsp, errors.New(helper.UsernameOrPasswordErr)
	}

	record := g.Record{
		"last_login_ip":   ip,
		"last_login_time": lastLoginTime.Unix(),
	}
	query, _, _ = dialect.Update("tbl_admins").Set(record).Where(g.Ex{"id": data.ID}).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		body := fmt.Errorf("%s,[%s]", err.Error(), query)
		return rsp, pushLog(body, helper.DBErr)
	}

	permission := "111"
	token := fmt.Sprintf("admin:token:%s", data.ID)
	b, _ := helper.JsonMarshal(data)
	sid, err := session.AdminSet(b, token, deviceNo)
	if err != nil {
		fmt.Println("AdminSet = ", err)
		return rsp, errors.New(helper.SessionErr)
	}

	rsp.Token = sid
	rsp.AdminName = username
	rsp.Allows = permission
	rsp.Domain = ""
	rsp.GroupId = data.GroupID

	//log := fmt.Sprintf("后台账号[%s]登陆,登陆ip[%s],登陆时间[%s],登陆设备号[%s] 登陆",
	//	username, ip, lastLoginTime.Format("2006-01-02 15:04:05"), deviceNo)
	//fmt.Println(adminLogInsert("login", log, "login", username))

	return rsp, nil
}

// 检测管理员账号是否已存在
func AdminExist(ex g.Ex) bool {

	var id string

	t := dialect.From("tbl_admins")
	query, _, _ := t.Select("id").Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&id, query)
	return err != sql.ErrNoRows
}

// 后台管理员退出登录
func AdminLogout(ctx *fasthttp.RequestCtx) {
	/*
		admin, err := AdminToken(ctx)
		if err != nil {
			fmt.Println("admin_logout_err: not fount admin:", err.Error())
			return
		}

			// 写入登出日志
			log := adminLoginLogBase{
				UID:       admin["id"],
				Name:      admin["name"],
				IP:        helper.FromRequest(ctx),
				Flag:      2,
				CreatedAt: uint32(time.Now().Unix()),
			}

			err = meta.Zlog.Post(esPrefixIndex("admin_login_log"), log)
			if err != nil {
				fmt.Printf("user %s login_log wirte err = %s", admin["name"], err.Error())
				return
			}
	*/
}
