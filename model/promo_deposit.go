package model

import (
	"admin/contrib/helper"
	"database/sql"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
)

func PromoDepositConfigInsert(data PromoDepositConfig) error {

	data.Id = helper.GenId()
	query, _, _ := dialect.Insert("tbl_promo_deposit").Rows(&data).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func PromoDepositConfigUpdate(id string, data PromoDepositConfig) error {

	query, _, _ := dialect.Update("tbl_promo_deposit").Set(&data).Where(g.Ex{"id": id}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func PromoDepositConfigList(page, pageSize uint, ex g.Ex) (PromoDepositConfigData, error) {

	data := PromoDepositConfigData{}
	t := dialect.From("tbl_promo_deposit")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPromoDepositConfig...).Where(ex).Offset(offset).Limit(pageSize).Order(g.C("sort").Asc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize
	return data, nil
}

func PromoDepositList(ty int) ([]PromoDepositConfig, error) {

	var data []PromoDepositConfig
	query, _, _ := dialect.From("tbl_promo_deposit").
		Select(colsPromoDepositConfig...).Where(g.Ex{"ty": ty}).Order(g.C("min_amount").Desc()).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}
