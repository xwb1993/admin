package model

import (
	"admin/contrib/helper"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
)

type WhiteListData struct {
	D []TblWhitelist `json:"d"`
	T int            `json:"t"`
	S int            `json:"s"`
}

// 白名单
// 白名单配置表
type TblWhitelist struct {
	Id          string `db:"id" json:"id"`
	Ip          string `db:"ip" json:"ip"`                     //ip
	Remark      string `db:"remark" json:"remark"`             //备注
	CreatedAt   uint32 `db:"created_at" json:"created_at"`     //添加时间
	CreatedUid  string `db:"created_uid" json:"created_uid"`   //添加人uid
	CreatedName string `db:"created_name" json:"created_name"` //添加人名
	Area        string `db:"area" json:"area"`                 //区域 国家 省 市
}

const whiteListkey = "ht:whitelist"

func WhiteListInsert(adminID, adminName, ip, remark string) error {

	country, region, city, err := IpLocation(ip)
	if err != nil {
		return err
	}

	data := TblWhitelist{
		Id:          helper.GenId(),
		Ip:          ip,
		Area:        country + " " + region + " " + city,
		Remark:      remark,
		CreatedAt:   uint32(time.Now().Unix()),
		CreatedUid:  adminID,
		CreatedName: adminName,
	}
	query, _, _ := dialect.Insert("tbl_whitelist").Rows(data).ToSQL()
	//fmt.Println(query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		if strings.HasPrefix(err.Error(), "Error 1062") {
			return errors.New(helper.RecordExistErr)
		}

		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	err = meta.MerchantRedis.SAdd(ctx, whiteListkey, ip).Err()
	return err
}

func WhiteListList(startTime, endTime string, ex g.Ex, page, pageSize int) (WhiteListData, error) {

	data := WhiteListData{}
	if startTime != "" && endTime != "" {
		startAt, err := time.ParseInLocation("2006-01-02 15:04:05", startTime, time.Local)
		if err != nil {
			return data, errors.New(helper.TimeTypeErr)
		}

		endAt, err := time.ParseInLocation("2006-01-02 15:04:05", endTime, time.Local)
		if err != nil {
			return data, errors.New(helper.TimeTypeErr)
		}

		if startAt.Unix() >= endAt.Unix() {
			return data, errors.New(helper.QueryTimeRangeErr)
		}
		ex["created_at"] = g.Op{"between": exp.NewRangeVal(startAt.Unix(), endAt.Unix())}
	}

	t := dialect.From("tbl_whitelist")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsWhiteList...).Where(ex).Order(g.C("created_at").Desc()).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	//	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	data.S = pageSize

	return data, nil
}

func WhiteListDelete(adminName, id string) error {

	data := TblWhitelist{}
	ex := g.Ex{
		"id": id,
	}
	query, _, _ := dialect.From("tbl_whitelist").Select(colsWhiteList...).Where(ex).ToSQL()
	//fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return errors.New(helper.RecordNotExistErr)
	}

	query, _, _ = dialect.Delete("tbl_whitelist").Where(ex).ToSQL()

	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	err = meta.MerchantRedis.SRem(ctx, whiteListkey, data.Ip).Err()

	return err
}

func WhiteListCheck(ip string) bool {

	exists := meta.MerchantRedis.SIsMember(ctx, whiteListkey, ip).Val()
	//fmt.Println("WhiteListCheck", cmd.String())
	return exists
}
