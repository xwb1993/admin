package model

import (
	"admin/contrib/helper"
	"fmt"

	g "github.com/doug-martin/goqu/v9"
)

type TblPayFactory struct {
	Fid          string `json:"fid" db:"fid" redis:"fid" cbor:"fid"`
	Name         string `json:"name" db:"name" redis:"name" cbor:"name"`
	Url          string `json:"url" db:"url" redis:"url"`
	NotifyUrl    string `json:"notify_url" db:"notify_url" redis:"notify_url"`
	Key          string `json:"key" db:"key" redis:"key"`
	Mchid        string `json:"mchid" db:"mchid" redis:"mchid"`
	AppId        string `json:"app_id" db:"app_id" redis:"app_id"`
	AppKey       string `json:"app_key" db:"app_key" redis:"app_key"`
	PayCode      string `json:"pay_code" db:"pay_code" redis:"pay_code"`
	Fmax         string `json:"fmax" db:"fmax" redis:"fmax" cbor:"fmax"`
	Fmin         string `json:"fmin" db:"fmin" redis:"fmin" cbor:"fmin"`
	AmountList   string `json:"amount_list" db:"amount_list" redis:"amount_list" cbor:"amount_list"`
	ShowName     string `json:"show_name" db:"show_name" cbor:"show_name"`
	State        string `json:"state" db:"state" cbor:"state"`
	PayRate      string `json:"pay_rate" db:"pay_rate" cbor:"pay_rate"`
	Ty           int    `json:"ty" db:"ty" cbor:"ty"`
	Automatic    string `json:"automatic" db:"automatic" cbor:"automatic"`
	Sort         int    `json:"sort" cbor:"sort" db:"sort"`
	CountryCode  string `json:"country_code" db:"country_code" cbor:"country_code"`
	CurrencyCode string `json:"currency_code" db:"currency_code" cbor:"currency_code"`
	Type         string `json:"type" db:"type" cbor:"type"`
}
type tblPayFactoryData struct {
	T int             `json:"t" cbor:"t"`
	D []TblPayFactory `json:"d" cbor:"d"`
}

// CachePayment 获取支付方式
func CachePayFactory(fid string) (TblPayFactory, error) {

	m := TblPayFactory{}
	query, _, _ := dialect.From("tbl_pay_factory").Select(colsPayFactory...).Where(g.Ex{"fid": fid}).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	return m, nil
}

func PayFactoryList() (tblPayFactoryData, error) {

	var data tblPayFactoryData
	query, _, _ := dialect.From("tbl_pay_factory").Select(colsPayFactory...).Order(g.C("sort").Asc()).ToSQL()
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	data.T = len(data.D)
	return data, nil
}

func UpdatePayFactory(data TblPayFactory) error {

	ex := g.Ex{
		"fid": data.Fid,
	}
	record := g.Record{
		"url":           data.Url,
		"key":           data.Key,
		"notify_url":    data.NotifyUrl,
		"mchid":         data.Mchid,
		"app_id":        data.AppId,
		"app_key":       data.AppKey,
		"pay_code":      data.PayCode,
		"pay_rate":      data.PayRate,
		"fmax":          data.Fmax,
		"fmin":          data.Fmin,
		"amount_list":   data.AmountList,
		"state":         data.State,
		"show_name":     data.ShowName,
		"sort":          data.Sort,
		"country_code":  data.CountryCode,
		"currency_code": data.CurrencyCode,
		"type":          data.Type,
	}

	query, _, _ := dialect.Update("tbl_pay_factory").Set(record).Where(ex).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}
