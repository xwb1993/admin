package model

import (
	"admin/contrib/helper"
	myredis "common/redis"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"strconv"
	"strings"
	"time"
)

type Loginuser struct {
	Admin     string
	Operator  string
	Businsess string
}

// 获取登录账号类型
func GetLoginUser(adminid string) *Loginuser {
	data := new(Loginuser)

	adminId := adminid
	//验证是否是渠道登录
	agentData := OperatorInfoData{}
	query, _, _ := dialect.From("tbl_operator_info").Select(g.COUNT("id")).Where(g.Ex{"id": adminId}).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&agentData.T, query)
	if err != nil && err != sql.ErrNoRows {
		return data
	}
	if agentData.T == 1 {
		data.Operator = adminId
	} else {
		//验证是否是业务员登录
		proxyData := BusinessInfoData{}
		query, _, _ = dialect.From("tbl_business_info").Select(g.COUNT("id")).Where(g.Ex{"id": adminId}).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&proxyData.T, query)
		if err != nil && err != sql.ErrNoRows {
			return data
		}
		if proxyData.T == 1 {
			data.Businsess = adminId
		}
	}
	fmt.Println(data)
	return data
}

// 统计数据修正
func FixDailyData(from string, to string) bool {
	t1, _ := time.Parse("2006-01-02", from)
	fromTimestamp := t1.Unix()

	t2, _ := time.Parse("2006-01-02", to)
	toTimestamp := t2.Unix()

	for {
		if fromTimestamp > toTimestamp {
			break
		}
		timeStr := time.Unix(fromTimestamp, 0).Format("2006-01-02")
		StatisticalData(timeStr)
		fromTimestamp = fromTimestamp + 86400
	}
	return true
}

// 定时任务
func CronDailyData() {
	//业务逻辑，每五分钟执行，凌晨00：05之前 执行昨天的统计，00:05 之后执行今天的统计
	now := time.Now()
	nowTimestamp := now.Unix()
	lcTimestamp := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location()).Unix()

	if (nowTimestamp - lcTimestamp) > 60*5 {
		//统计今天
		timeStr := time.Unix(nowTimestamp, 0).Format("2006-01-02")
		StatisticalData(timeStr)
	} else {
		//统计昨天
		yes := now.AddDate(0, 0, -1)
		yesTimestamp := time.Date(yes.Year(), yes.Month(), yes.Day(), 0, 0, 0, 0, yes.Location()).Unix()
		timeStr := time.Unix(yesTimestamp, 0).Format("2006-01-02")
		StatisticalData(timeStr)
	}
}

func StatisticalData(timeStr string) {
	//业务员数据
	var businessList []TblBusinessInfo
	query := "select * from tbl_business_info"

	err := meta.MerchantDB.Select(&businessList, query)
	if err != nil && err != sql.ErrNoRows {
		return
	}
	if len(businessList) > 0 {
		for _, value := range businessList {
			go goBusinessDailyData(value, timeStr)
		}
	}
	//渠道数据
	var operatorList []TblOperatorInfo
	query = "select * from tbl_operator_info"
	err = meta.MerchantDB.Select(&operatorList, query)
	if err != nil && err != sql.ErrNoRows {
		return
	}
	if len(operatorList) > 0 {
		for _, value := range operatorList {
			go goOperatorDailyData(&value, timeStr)
		}
	}
}

// 并发执行业务员数据统计
func goBusinessDailyData(value TblBusinessInfo, timeStr string) {
	businessData := ReportDailyData{}
	businessId := value.Id
	field := "IFNULL(sum(a.deposit),0) deposit," +
		"IFNULL(sum(a.withdraw),0) withdraw," +
		"IFNULL(sum(a.running),0) running," +
		"IFNULL(sum(a.game_round),0) game_round," +
		"IFNULL(sum(a.game_winlost),0) game_winlost," +
		"IFNULL(sum(a.turntable_bonus),0) turntable_bonus," +
		"IFNULL(sum(a.proxy_invite_bonus),0) proxy_invite_bonus," +
		"IFNULL(sum(a.first_deposit),0) first_deposit"
	query := "select " + field + " from tbl_report_user_daily as a join tbl_member_base as b on a.uid=b.uid where a.created_at='" + timeStr + "' and b.business_id=" + businessId
	_ = meta.MerchantDB.Get(&businessData, query)
	fmt.Println(query)
	//充值人数
	query = fmt.Sprintf("select count(1) from tbl_report_user_daily as a join tbl_member_base as b on a.uid=b.uid where a.deposit>0 and a.created_at='%s' and b.business_id=%s", timeStr, businessId)
	_ = meta.MerchantDB.Get(&businessData.DepositNum, query)
	//提现人数
	query = fmt.Sprintf("select count(1) from tbl_report_user_daily as a join tbl_member_base as b on a.uid=b.uid where a.withdraw>0 and a.created_at='%s' and and b.business_id=%s", timeStr, businessId)
	_ = meta.MerchantDB.Get(&businessData.WithdrawNum, query)
	//首充人数
	query = fmt.Sprintf("select count(1) from tbl_report_user_daily as a join tbl_member_base as b on a.uid=b.uid where a.first_deposit>0 and a.created_at='%s' and and b.business_id=%s", timeStr, businessId)
	_ = meta.MerchantDB.Get(&businessData.FirstDepositNum, query)

	businessData.BusinessId = businessId
	businessData.OperatorId = value.OperatorId
	businessData.CreateDate = timeStr
	todayBusinessData := ReportDailyData{}
	query = "select * from tbl_report_daily where business_id=" + businessId + " and create_date='" + timeStr + "'"
	_ = meta.MerchantDB.Get(&todayBusinessData, query)
	if todayBusinessData.Id > 0 || todayBusinessData.BusinessId == businessId {
		//更新
		businessData.Id = todayBusinessData.Id
		query, _, _ = dialect.Update("tbl_report_daily").Set(businessData).Where(g.Ex{"id": todayBusinessData.Id}).ToSQL()
		_, _ = meta.MerchantDB.Exec(query)
	} else {
		//插入
		query, _, _ = dialect.Insert("tbl_report_daily").Rows(businessData).ToSQL()
		_, _ = meta.MerchantDB.Exec(query)
	}
}

// 并发执行渠道数据统计
func goOperatorDailyData(value *TblOperatorInfo, timeStr string) {
	operatorData := ReportDailyData{}
	operatorId := value.Id

	field := "IFNULL(sum(a.deposit),0) deposit," +
		"IFNULL(sum(a.withdraw),0) withdraw," +
		"IFNULL(sum(a.running),0) running," +
		"IFNULL(sum(a.game_round),0) game_round," +
		"IFNULL(sum(a.game_winlost),0) game_winlost," +
		"IFNULL(sum(a.turntable_bonus),0) turntable_bonus," +
		"IFNULL(sum(a.proxy_invite_bonus),0) proxy_invite_bonus," +
		"IFNULL(sum(a.first_deposit),0) first_deposit"
	query := "select " + field + " from tbl_report_user_daily as a join tbl_member_base as b on a.uid=b.uid where a.created_at='" + timeStr + "' and b.operator_id=" + operatorId
	_ = meta.MerchantDB.Get(&operatorData, query)
	fmt.Println(query)
	//充值人数
	query = fmt.Sprintf("select count(1) from tbl_report_user_daily as a join tbl_member_base as b on a.uid=b.uid where a.deposit>0 and a.created_at='%s'  and b.operator_id=%s", timeStr, operatorId)
	_ = meta.MerchantDB.Get(&operatorData.DepositNum, query)
	//提现人数
	query = fmt.Sprintf("select count(1) from tbl_report_user_daily as a join tbl_member_base as b on a.uid=b.uid where a.withdraw>0 and a.created_at='%s'  and b.operator_id=%s", timeStr, operatorId)
	_ = meta.MerchantDB.Get(&operatorData.WithdrawNum, query)
	//首充人数
	query = fmt.Sprintf("select count(1) from tbl_report_user_daily as a join tbl_member_base as b on a.uid=b.uid where a.first_deposit>0 and a.created_at='%s'  and b.operator_id=%s", timeStr, operatorId)
	_ = meta.MerchantDB.Get(&operatorData.FirstDepositNum, query)

	operatorData.BusinessId = "0"
	operatorData.OperatorId = value.Id
	operatorData.CreateDate = timeStr
	todayOperatorData := ReportDailyData{}
	query = "select * from tbl_report_daily where business_id=0 and operator_id=" + operatorId + " and create_date='" + timeStr + "'"
	_ = meta.MerchantDB.Get(&todayOperatorData, query)
	if todayOperatorData.Id > 0 || todayOperatorData.OperatorId == operatorId {
		//更新
		operatorData.Id = todayOperatorData.Id
		query, _, _ = dialect.Update("tbl_report_daily").Set(operatorData).Where(g.Ex{"id": todayOperatorData.Id}).ToSQL()
		_, _ = meta.MerchantDB.Exec(query)
	} else {
		//插入
		query, _, _ = dialect.Insert("tbl_report_daily").Rows(operatorData).ToSQL()
		_, _ = meta.MerchantDB.Exec(query)
	}
}

// 获取伞下全部业务员 以及自身 列表
func GetBusinessTeam(businessId string) string {
	data := []string{}
	query := "select id from tbl_business_info where parent_id=" + businessId
	err := meta.MerchantDB.Select(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return ""
	}
	retData := businessId
	if len(data) > 0 {
		retData += "," + strings.Join(data, ",")
	}
	return retData
}

// 账变记录
func UpdateBalance(uid string, balance float64, changeType int, billNo string) (bool, error) {
	if balance == 0 {
		return false, errors.New(helper.AmountErr)
	}
	beforBalance := myredis.GetUserFieldFloat64(uid, "tbl_member_balance:brl")
	fmt.Println(beforBalance)
	userName := myredis.GetUserFieldString(uid, "tbl_member_base:username")
	//扣钱，先扣，后插记录
	if balance < 0 {
		if (beforBalance + balance) < 0 {
			return false, errors.New(helper.LackOfBalance)
		}
		ret := myredis.AddUserFieldValueByFloat64(uid, "tbl_member_balance:brl", balance)
		if ret == false {
			return false, errors.New(helper.ServerErr)
		}
		trans := MemberTransaction{
			AfterAmount:  strconv.FormatFloat(beforBalance+balance, 'f', 2, 64),
			Amount:       strconv.FormatFloat(balance, 'f', 2, 64),
			BeforeAmount: strconv.FormatFloat(beforBalance, 'f', 2, 64),
			BillNo:       billNo,
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     changeType,
			UID:          uid,
			Username:     userName,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		_, err := meta.MerchantDB.Exec(query)
		if err != nil {
			fmt.Println(err)
			return false, errors.New(helper.DBErr)
		}
	}
	//加钱，先插记录，后加钱
	if balance > 0 {
		trans := MemberTransaction{
			AfterAmount:  strconv.FormatFloat(beforBalance+balance, 'f', 2, 64),
			Amount:       strconv.FormatFloat(balance, 'f', 2, 64),
			BeforeAmount: strconv.FormatFloat(beforBalance, 'f', 2, 64),
			BillNo:       billNo,
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     changeType,
			UID:          uid,
			Username:     userName,
		}
		query, _, _ := dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		_, err := meta.MerchantDB.Exec(query)
		if err != nil {
			return false, errors.New(helper.DBErr)
		}
		ret := myredis.AddUserFieldValueByFloat64(uid, "tbl_member_balance:brl", balance)
		if ret == false {
			return false, errors.New(helper.ServerErr)
		}
	}
	return true, nil
}
