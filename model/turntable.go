package model

import (
	"admin/contrib/helper"
	ryrpc "admin/rpc"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"sort"
	"time"
)

type TurntableInfo struct {
	Reward float64 `json:"reward"`
	Count  int     `json:"count"`
	Remark string  `json:"remark"`
}

func TurnTableReviewList(startTime, endTime string, ex g.Ex, timeType, page, pageSize int) (ryrpc.TurntableReviewData, error) {

	data := ryrpc.TurntableReviewData{}

	startAt, err := helper.TimeToLoc(startTime, loc)
	if err != nil {
		return data, errors.New(helper.TimeTypeErr)
	}

	endAt, err := helper.TimeToLoc(endTime, loc)
	if err != nil {
		return data, errors.New(helper.TimeTypeErr)
	}

	if startAt >= endAt {
		return data, errors.New(helper.QueryTimeRangeErr)
	}
	if timeType == 1 {
		ex["apply_at"] = g.Op{
			"between": exp.NewRangeVal(startAt, endAt),
		}
	} else {
		ex["review_at"] = g.Op{
			"between": exp.NewRangeVal(startAt, endAt),
		}
	}

	t := dialect.From("tbl_pdd_turntable_review")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT("id")).Where(ex).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsTurntableReview...).Where(ex).Order(g.C("review_at").Desc(), g.C("apply_at").Desc()).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

func TurnTableReview(state string, record g.Record) error {

	data := ryrpc.TblPddTurntableReview{}

	query, _, _ := dialect.From("tbl_pdd_turntable_review").
		Select(colsTurntableReview...).Where(g.Ex{"id": record["id"]}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil {
		return errors.New(helper.DBErr)
	}

	// 只有审核中状态的才能操作
	if fmt.Sprintf(`%d`, data.State) != AdjustReviewing {
		return errors.New(helper.StateParamErr)
	}
	fmt.Println("state:", state)
	fmt.Println("data.State:", data.State)

	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	query, _, _ = dialect.Update("tbl_pdd_turntable_review").Set(record).Where(g.Ex{"id": record["id"]}).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}
	// 同意后奖励金额领取到游戏余额，清零奖励金额
	if state == AdjustReviewPass {
		// 更新调整记录状态
		info := g.Record{
			"amount":      0,
			"hand_amount": 0,
			"tot_amount":  g.L(fmt.Sprintf("tot_amount+%f", data.Amount)),
			"updated_at":  time.Now().Unix(),
		}
		query, _, _ = dialect.Update("tbl_pdd_turntable_info").Set(info).Where(g.Ex{"uid": data.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}
		// 获取钱包余额
		mb, err := MemberBalanceFindOne(data.Uid)
		if err != nil {
			return err
		}
		amount := fmt.Sprintf("%f", data.Amount)
		balanceAfter := fmt.Sprintf("%f", mb.Brl+data.Amount)
		// 新增账变记录
		trans := MemberTransaction{
			AfterAmount:  balanceAfter,
			Amount:       amount,
			BeforeAmount: fmt.Sprintf("%f", mb.Brl),
			BillNo:       helper.GenId(),
			CreatedAt:    time.Now().UnixMilli(),
			ID:           helper.GenId(),
			CashType:     helper.TransactionBonusWin,
			UID:          data.Uid,
			Username:     data.Username,
		}
		query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}

		// 新增余额
		br := g.Record{
			"brl":           g.L(fmt.Sprintf("brl+%f", data.Amount)),
			"unlock_amount": g.L(fmt.Sprintf("unlock_amount+%f", data.Amount)),
		}
		query, _, _ = dialect.Update("tbl_member_balance").Set(br).Where(g.Ex{"uid": data.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}
	}
	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}

func GetTurntableInfo(uid string) (ryrpc.TblPddTurntableInfo, error) {

	var data ryrpc.TblPddTurntableInfo
	query, _, _ := dialect.From("tbl_pdd_turntable_info").Select(colsTurntableInfo...).Where(g.Ex{"uid": uid}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return data, errors.New(helper.UIDErr)
	}
	data.RewardAmount = data.Amount + data.HandAmount

	return data, nil
}

func TurnTableHistoryList(startTime, endTime string, ex g.Ex, page, pageSize int) (ryrpc.TurntableHistoryData, error) {

	data := ryrpc.TurntableHistoryData{}

	startAt, err := helper.TimeToLoc(startTime, loc)
	if err != nil {
		return data, errors.New(helper.TimeTypeErr)
	}

	endAt, err := helper.TimeToLoc(endTime, loc)
	if err != nil {
		return data, errors.New(helper.TimeTypeErr)
	}

	if startAt >= endAt {
		return data, errors.New(helper.QueryTimeRangeErr)
	}

	t := dialect.From("tbl_pdd_turntable_history")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT("id")).Where(ex).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsTurntableHistory...).Where(ex).Order(g.C("created_at").Desc()).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

type TurnTableData struct {
	T int                   `json:"t"`
	D []TurnTableStatistics ` json:"d"`
	S int                   `json:"s"`
}

type TurnTableStatistics struct {
	Uid               string  `json:"uid"`
	InviteRegisterNum int     `json:"invite_register_num"` //邀请注册人数
	InviteRechargeNum int     `json:"invite_recharge_num"` //邀请充值人数(充值达到最低首充金额)
	DepositAmount     float64 `json:"deposit_amount"`      //充值金额
	WithdrawAmount    float64 `json:"withdraw_amount"`     //取款金额
	DwDiff            float64 `json:"dw_diff"`             //充提差
	Amount            float64 `json:"amount"`              //奖励金额
	HandAmount        float64 `json:"hand_amount"`         //手动奖励金额
}

func TurnTableReport(page, pageSize int, uid, userName string) (TurnTableData, error) {

	var data TurnTableData

	data.S = pageSize
	ex := g.Ex{}
	if userName != "" {
		ex["username"] = userName
	}
	if uid != "" {
		ex["uid"] = uid
	}

	t := dialect.From("tbl_pdd_turntable_info")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT("uid")).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil && err != sql.ErrNoRows {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if errors.Is(err, sql.ErrNoRows) || data.T == 0 {
			return data, nil
		}
	}
	offset := (uint(page) - 1) * uint(pageSize)
	var d []ryrpc.TblPddTurntableInfo
	query, _, _ := t.Select(colsTurntableInfo...).Where(ex).Order(g.I("created_at").Desc()).Offset(offset).Limit(uint(pageSize)).ToSQL()
	err := meta.MerchantDB.Select(&d, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if len(d) == 0 {
		return data, nil
	}
	for _, v := range d {
		m, err := MemberFindByUid(v.Uid)
		if err != nil {
			return data, err
		}
		statistics := TurnTableStatistics{
			Uid:               m.Uid,
			InviteRegisterNum: m.InviteRegisterNum,
			InviteRechargeNum: m.InviteNum,
			DepositAmount:     m.DepositAmount,
			WithdrawAmount:    v.TotAmount,
			DwDiff:            m.DepositAmount - v.TotAmount,
			Amount:            v.Amount,
			HandAmount:        v.HandAmount,
		}
		data.D = append(data.D, statistics)
	}
	sort.Sort(ByDepositAmount(data.D))
	return data, nil
}

// 按照充值金额降序排序
type ByDepositAmount []TurnTableStatistics

func (a ByDepositAmount) Len() int           { return len(a) }
func (a ByDepositAmount) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByDepositAmount) Less(i, j int) bool { return a[i].DepositAmount > a[j].DepositAmount }
