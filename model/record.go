package model

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"strconv"
	"strings"
)

// 游戏投注记录结构
type tblGameRecord struct {
	RowId          string  `db:"row_id" json:"row_id" cbor:"row_id"`
	BillNo         string  `db:"bill_no" json:"bill_no" cbor:"bill_no"`
	ApiType        string  `db:"api_type" json:"api_type" cbor:"api_type"`
	PlayerName     string  `db:"-" json:"player_name" cbor:"player_name"`
	Name           string  `db:"name" json:"name" cbor:"name"`
	Uid            string  `db:"uid" json:"-" cbor:"uid"`
	NetAmount      float64 `db:"net_amount" json:"net_amount" cbor:"net_amount"`
	BetTime        int64   `db:"bet_time" json:"bet_time" cbor:"bet_time"`
	StartTime      int64   `db:"start_time" json:"start_time" cbor:"start_time"`
	Resettle       uint8   `db:"resettle" json:"resettle" cbor:"resettle"`
	Presettle      uint8   `db:"presettle" json:"presettle" cbor:"presettle"`
	GameType       string  `db:"game_type" json:"game_type" cbor:"game_type"`
	GameCode       string  `db:"game_code" json:"game_code" cbor:"game_code"`
	BetAmount      float64 `db:"bet_amount" json:"bet_amount" cbor:"bet_amount"`
	ValidBetAmount float64 `db:"valid_bet_amount" json:"valid_bet_amount" cbor:"valid_bet_amount"`
	RebateAmount   float64 `db:"-" json:"rebate_amount" cbor:"rebate_amount"`
	Flag           int     `db:"flag" json:"flag" cbor:"flag"`
	PlayType       string  `db:"play_type" json:"play_type" cbor:"play_type"`
	Prefix         string  `db:"prefix" json:"prefix" cbor:"prefix"`
	Result         string  `db:"result" json:"result" cbor:"result"`
	CreatedAt      uint64  `db:"-" json:"-" cbor:"created_at"`
	UpdatedAt      uint64  `db:"-" json:"-" cbor:"updated_at"`
	ApiName        string  `db:"api_name" json:"api_name" cbor:"api_name"`
	ApiBillNo      string  `db:"api_bill_no" json:"api_bill_no" cbor:"api_bill_no"`
	MainBillNo     string  `db:"main_bill_no" json:"main_bill_no" cbor:"main_bill_no"`
	GameName       string  `db:"game_name" json:"game_name" cbor:"game_name"`
	HandicapType   string  `db:"handicap_type" json:"handicap_type" cbor:"handicap_type"`
	Handicap       string  `db:"handicap" json:"handicap" cbor:"handicap"`
	Odds           float64 `db:"odds" json:"odds" cbor:"odds"`
	SettleTime     int64   `db:"settle_time" json:"settle_time" cbor:"settle_time"`
	TopUid         string  `db:"top_uid" json:"-"`                                  //总代uid
	TopName        string  `db:"top_name" json:"top_name"`                          //总代代理
	ParentUid      string  `db:"parent_uid" json:"-"`                               //上级uid
	ParentName     string  `db:"parent_name" json:"parent_name" cbor:"parent_name"` //上级代理
}

// 游戏记录数据
type GameRecordData struct {
	T   int64           `json:"t" cbor:"t"`
	D   []tblGameRecord `json:"d" cbor:"d"`
	Agg tblGameRecord   `json:"agg" cbor:"agg"`
}

func GameRecord(pageSize, page uint, params map[string]string) (GameRecordData, error) {

	data := GameRecordData{}
	if len(params["username"]) > 0 {
		username := strings.ToLower(params["username"])
		mb, err := MemberFindByUsername(username)
		if err != nil {
			return data, errors.New(helper.UsernameErr)
		}
		params["uid"] = mb.Uid
		params["name"] = ""
	}
	//查询条件
	ex := g.Ex{}

	if params["pid"] != "" {
		if strings.Contains(params["pid"], ",") {
			pids := strings.Split(params["pid"], ",")

			var ids []interface{}
			for _, v := range pids {
				if validator.CtypeDigit(v) {
					ids = append(ids, v)
				}
			}

			ex["api_type"] = ids
		}

		if !strings.Contains(params["pid"], ",") {
			if validator.CtypeDigit(params["pid"]) {
				ex["api_type"] = params["pid"]
			}
		}
	}

	if params["flag"] != "" {
		ex["flag"] = params["flag"]
	}

	rangeField := ""
	if params["time_flag"] != "" {
		rangeField = betTimeFlags[params["time_flag"]]
	}

	if rangeField == "" {
		return data, errors.New(helper.QueryTermsErr)
	}

	if !validator.CtypeDigit(params["time_flag"]) {
		return data, errors.New(helper.QueryTermsErr)
	}

	if params["bet_min"] == "" && params["bet_max"] != "" {
		max, _ := strconv.ParseFloat(params["bet_max"], 64)
		ex["bet_amount"] = g.Op{"lt": max}
	}

	if params["bet_min"] != "" && params["bet_max"] == "" {
		min, _ := strconv.ParseFloat(params["bet_min"], 64)
		ex["bet_amount"] = g.Op{"gt": min}
	}

	if params["bet_min"] != "" && params["bet_max"] != "" {
		min, _ := strconv.ParseFloat(params["bet_min"], 64)
		max, _ := strconv.ParseFloat(params["bet_max"], 64)
		if max < min {
			return data, errors.New(helper.BetAmountRangeErr)
		}

		ex["bet_amount"] = g.Op{"between": exp.NewRangeVal(min, max)}
	}

	if params["uid"] != "" {
		ex["uid"] = params["uid"]
	}

	if params["plat_type"] != "" {
		ex["game_type"] = params["plat_type"]
	}

	if params["game_name"] != "" {
		ex["game_name"] = params["game_name"]
	}

	if params["win_lose"] != "" {
		if params["win_lose"] == "1" {
			ex["net_amount"] = g.Op{"gt": 0}
		}
		if params["win_lose"] == "2" {
			ex["net_amount"] = g.Op{"lt": 0}
		}
	}

	if params["bill_no"] != "" {
		ex["bill_no"] = params["bill_no"]
	}

	if params["api_bill_no"] != "" {
		ex["api_bill_no"] = params["api_bill_no"]
	}

	if params["pre_settle"] != "" {
		early, _ := strconv.Atoi(params["pre_settle"])
		ex["presettle"] = early
	}

	if params["resettle"] != "" {
		second, _ := strconv.Atoi(params["resettle"])
		ex["resettle"] = second
	}

	if params["parent_name"] != "" {
		ex["parent_name"] = params["parent_name"]
	}

	if params["top_name"] != "" {
		ex["top_name"] = params["top_name"]
	}

	if params["main_bill_no"] != "" {
		ex["main_bill_no"] = params["main_bill_no"]
	}

	return recordAdminGame(params["time_flag"], params["start_time"], params["end_time"], page, pageSize, ex, params)
}

func recordAdminGame(flag, startTime, endTime string, page, pageSize uint, ex g.Ex, mp map[string]string) (GameRecordData, error) {

	data := GameRecordData{}

	startAt, err := helper.TimeToLocMs(startTime, loc)
	if err != nil {
		return data, errors.New(helper.DateTimeErr)
	}

	endAt, err := helper.TimeToLocMs(endTime, loc)
	if err != nil {
		return data, errors.New(helper.DateTimeErr)
	}

	if startAt >= endAt {
		return data, errors.New(helper.QueryTimeRangeErr)
	}

	//var (
	//	gameType string
	//	apiType  string
	//)
	//// 如果是会员精确查询，不限制31天内
	//if value, ok := ex["game_type"].(string); ok {
	//	gameType = value
	//}
	//if value, ok := ex["api_type"].(string); ok {
	//	apiType = value
	//}
	// 查询范围有用户的可以查92天的数据
	//if uid, ok := mp["uid"]; ok && len(uid) > 0 {
	//	if (endAt - startAt) > 92*24*60*60*1000 {
	//		return data, errors.New(helper.QueryTimeRangeErr)
	//	}
	//} else if gameType == "" && apiType == "" && (endAt-startAt) > 7*24*60*60*1000 {
	//	return data, errors.New(helper.QueryTimeRangeErr)
	//}
	tableName := "tbl_game_record"

	ex["tester"] = 1
	ex[betTimeFlags[flag]] = g.Op{"between": exp.NewRangeVal(startAt, endAt+999)}

	if page == 1 {
		query, _, _ := dialect.From(tableName).Select(g.COUNT(1)).Where(ex).Limit(1).ToSQL()
		err = meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(err, helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
		query, _, _ = dialect.From(tableName).Select(g.SUM("net_amount").As("net_amount"), g.SUM("valid_bet_amount").As("valid_bet_amount"),
			g.SUM("bet_amount").As("bet_amount"),
		).Where(ex).ToSQL()
		err = meta.MerchantDB.Get(&data.Agg, query)
		if err != nil {
			return data, pushLog(err, helper.DBErr)
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := dialect.From(tableName).Select(colsGameRecord...).Where(ex).Order(g.C(betTimeFlags[flag]).Desc(), g.C("row_id").Desc()).Offset(offset).Limit(pageSize).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(err, helper.DBErr)
	}

	return data, nil
}

func MerchantGameRecord(pageSize, page uint, params map[string]string) (GameRecordData, error) {
	ex := g.Ex{}
	data := GameRecordData{}
	uids := make([]string, 0)
	if len(params["username"]) > 0 {
		username := strings.ToLower(params["username"])
		mb, err := MemberFindByUnameAndOpeId(username, params["operator_id"])
		if err != nil {
			return data, errors.New(helper.UsernameErr)
		}
		params["name"] = ""
		if mb.Uid != "" {
			ex["uid"] = mb.Uid
		}
	} else {
		members, err := MemberFindByOperatorId(params["operator_id"])
		if err != nil {
			return data, errors.New(helper.OperatorIdErr)
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
		}
	}
	//查询条件

	if params["pid"] != "" {
		if strings.Contains(params["pid"], ",") {
			pids := strings.Split(params["pid"], ",")

			var ids []interface{}
			for _, v := range pids {
				if validator.CtypeDigit(v) {
					ids = append(ids, v)
				}
			}

			ex["api_type"] = ids
		}

		if !strings.Contains(params["pid"], ",") {
			if validator.CtypeDigit(params["pid"]) {
				ex["api_type"] = params["pid"]
			}
		}
	}

	if params["flag"] != "" {
		ex["flag"] = params["flag"]
	}

	rangeField := ""
	if params["time_flag"] != "" {
		rangeField = betTimeFlags[params["time_flag"]]
	}

	if rangeField == "" {
		return data, errors.New(helper.QueryTermsErr)
	}

	if !validator.CtypeDigit(params["time_flag"]) {
		return data, errors.New(helper.QueryTermsErr)
	}

	if params["bet_min"] == "" && params["bet_max"] != "" {
		max, _ := strconv.ParseFloat(params["bet_max"], 64)
		ex["bet_amount"] = g.Op{"lt": max}
	}

	if params["bet_min"] != "" && params["bet_max"] == "" {
		min, _ := strconv.ParseFloat(params["bet_min"], 64)
		ex["bet_amount"] = g.Op{"gt": min}
	}

	if params["bet_min"] != "" && params["bet_max"] != "" {
		min, _ := strconv.ParseFloat(params["bet_min"], 64)
		max, _ := strconv.ParseFloat(params["bet_max"], 64)
		if max < min {
			return data, errors.New(helper.BetAmountRangeErr)
		}

		ex["bet_amount"] = g.Op{"between": exp.NewRangeVal(min, max)}
	}

	if len(uids) > 0 {
		ex["uid"] = uids
	}

	if params["plat_type"] != "" {
		ex["game_type"] = params["plat_type"]
	}

	if params["game_name"] != "" {
		ex["game_name"] = params["game_name"]
	}

	if params["win_lose"] != "" {
		if params["win_lose"] == "1" {
			ex["net_amount"] = g.Op{"gt": 0}
		}
		if params["win_lose"] == "2" {
			ex["net_amount"] = g.Op{"lt": 0}
		}
	}

	if params["bill_no"] != "" {
		ex["bill_no"] = params["bill_no"]
	}

	if params["api_bill_no"] != "" {
		ex["api_bill_no"] = params["api_bill_no"]
	}

	if params["pre_settle"] != "" {
		early, _ := strconv.Atoi(params["pre_settle"])
		ex["presettle"] = early
	}

	if params["resettle"] != "" {
		second, _ := strconv.Atoi(params["resettle"])
		ex["resettle"] = second
	}

	if params["parent_name"] != "" {
		ex["parent_name"] = params["parent_name"]
	}

	if params["top_name"] != "" {
		ex["top_name"] = params["top_name"]
	}

	if params["main_bill_no"] != "" {
		ex["main_bill_no"] = params["main_bill_no"]
	}

	return recordAdminGame(params["time_flag"], params["start_time"], params["end_time"], page, pageSize, ex, params)
}

func BusinessGameRecord(pageSize, page uint, params map[string]string) (GameRecordData, error) {
	ex := g.Ex{}
	data := GameRecordData{}
	uids := make([]string, 0)
	if len(params["username"]) > 0 {
		username := strings.ToLower(params["username"])
		mb, err := MemberFindByUnameAndProId(username, params["business_id"])
		if err != nil {
			return data, errors.New(helper.UsernameErr)
		}
		params["name"] = ""
		if mb.Uid != "" {
			ex["uid"] = mb.Uid
		}
	} else {
		members, err := MemberFindByBusinessId(params["business_id"])
		if err != nil {
			return data, errors.New(helper.OperatorIdErr)
		}
		if len(members) > 0 {
			for _, val := range members {
				uids = append(uids, val.Uid)
			}
		}
	}
	//查询条件

	if params["pid"] != "" {
		if strings.Contains(params["pid"], ",") {
			pids := strings.Split(params["pid"], ",")

			var ids []interface{}
			for _, v := range pids {
				if validator.CtypeDigit(v) {
					ids = append(ids, v)
				}
			}

			ex["api_type"] = ids
		}

		if !strings.Contains(params["pid"], ",") {
			if validator.CtypeDigit(params["pid"]) {
				ex["api_type"] = params["pid"]
			}
		}
	}

	if params["flag"] != "" {
		ex["flag"] = params["flag"]
	}

	rangeField := ""
	if params["time_flag"] != "" {
		rangeField = betTimeFlags[params["time_flag"]]
	}

	if rangeField == "" {
		return data, errors.New(helper.QueryTermsErr)
	}

	if !validator.CtypeDigit(params["time_flag"]) {
		return data, errors.New(helper.QueryTermsErr)
	}

	if params["bet_min"] == "" && params["bet_max"] != "" {
		max, _ := strconv.ParseFloat(params["bet_max"], 64)
		ex["bet_amount"] = g.Op{"lt": max}
	}

	if params["bet_min"] != "" && params["bet_max"] == "" {
		min, _ := strconv.ParseFloat(params["bet_min"], 64)
		ex["bet_amount"] = g.Op{"gt": min}
	}

	if params["bet_min"] != "" && params["bet_max"] != "" {
		min, _ := strconv.ParseFloat(params["bet_min"], 64)
		max, _ := strconv.ParseFloat(params["bet_max"], 64)
		if max < min {
			return data, errors.New(helper.BetAmountRangeErr)
		}

		ex["bet_amount"] = g.Op{"between": exp.NewRangeVal(min, max)}
	}

	if len(uids) > 0 {
		ex["uid"] = uids
	}

	if params["plat_type"] != "" {
		ex["game_type"] = params["plat_type"]
	}

	if params["game_name"] != "" {
		ex["game_name"] = params["game_name"]
	}

	if params["win_lose"] != "" {
		if params["win_lose"] == "1" {
			ex["net_amount"] = g.Op{"gt": 0}
		}
		if params["win_lose"] == "2" {
			ex["net_amount"] = g.Op{"lt": 0}
		}
	}

	if params["bill_no"] != "" {
		ex["bill_no"] = params["bill_no"]
	}

	if params["api_bill_no"] != "" {
		ex["api_bill_no"] = params["api_bill_no"]
	}

	if params["pre_settle"] != "" {
		early, _ := strconv.Atoi(params["pre_settle"])
		ex["presettle"] = early
	}

	if params["resettle"] != "" {
		second, _ := strconv.Atoi(params["resettle"])
		ex["resettle"] = second
	}

	if params["parent_name"] != "" {
		ex["parent_name"] = params["parent_name"]
	}

	if params["top_name"] != "" {
		ex["top_name"] = params["top_name"]
	}

	if params["main_bill_no"] != "" {
		ex["main_bill_no"] = params["main_bill_no"]
	}

	return recordAdminGame(params["time_flag"], params["start_time"], params["end_time"], page, pageSize, ex, params)
}
