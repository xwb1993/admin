package model

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

func ReportApiFeeList(param ReportApiFeeParam, adminid string) (ReportApiFeeRetData, error) {
	data := ReportApiFeeRetData{}
	monthDate := param.MonthDate
	if param.MonthDate == "" {
		monthDate = time.Now().Format("2006-01")
	}
	fromDate := monthDate + "-01"
	toDate := monthDate + "-31"

	loginUser := GetLoginUser(adminid)

	if loginUser.Operator != "" {
		if param.IsBusiness == 1 {
			//渠道查询业务员对账表
			data = reportBusinessApiFeeList(param, fromDate, toDate, " and operator_id="+loginUser.Operator)
		} else {
			data = reportOperatorApiFeeList(param, fromDate, toDate, " and id="+loginUser.Operator)
		}
	} else if loginUser.Businsess != "" {
		data = reportBusinessApiFeeList(param, fromDate, toDate, "and id in("+GetBusinessTeam(loginUser.Businsess)+")")
	} else {
		if param.IsOperator == 1 {
			//总后台查询渠道对账表
			data = reportOperatorApiFeeList(param, fromDate, toDate, "")
		}
		if param.IsBusiness == 1 {
			//总后台查询业务员对账表
			data = reportBusinessApiFeeList(param, fromDate, toDate, "")
		}
	}

	return data, nil
}

// 渠道对账表
func reportOperatorApiFeeList(param ReportApiFeeParam, fromDate string, toDate string, queryRange string) ReportApiFeeRetData {

	data := ReportApiFeeRetData{}
	where1, where2, where3 := "1=1", "1=1", "1=1"

	if param.OperatorId != "" {
		where1 += " and id=" + param.OperatorId
		where2 += " and operator_id=" + param.OperatorId
		where3 += " and operator_id=" + param.OperatorId

	}
	// 1.获取待查询的渠道列表
	where1 += queryRange
	offset := (param.Page - 1) * param.PageSize
	sql1 := "select  id as operator_id,operator_name,recharge_fee as deposit_fee,withdraw_fee,api_fee from tbl_operator_info where " + where1 + " order by id asc limit " + strconv.Itoa(param.PageSize) + " offset " + strconv.Itoa(offset)
	fmt.Println(sql1)
	// 2.获取各渠道当月的 各平台盈亏，计算api费用
	where2 += " and create_date>='" + fromDate + "' and create_date<='" + toDate + "'"
	sql2 := "select operator_id,game_id,ifnull(sum(game_winlost),0) game_winlost,ifnull(sum(game_tax),0) game_tax from tbl_report_game_daily where " + where2 + " and  business_id=0 and operator_id>0 group by operator_id,game_id"
	fmt.Println(sql2)
	// 3.获取各渠道当月的 充值，提现等数据
	where3 += " and create_date>='" + fromDate + "' and create_date<='" + toDate + "'"
	sql3 := "select operator_id,ifnull(sum(deposit),0) deposit,ifnull(sum(withdraw),0) withdraw from tbl_report_daily where " + where3 + " and business_id=0 and operator_id>0 group by operator_id"
	fmt.Println(sql3)
	err := meta.MerchantDB.Get(&data.T, "select count(1) from tbl_operator_info where "+where1)
	if err != nil && err != sql.ErrNoRows {
		return data
	}

	err = meta.MerchantDB.Select(&data.D, sql1)
	if err != nil && err != sql.ErrNoRows {
		fmt.Println(err)
		return data
	}

	//游戏数据
	gameReport := []ReportGameDailyData{}
	err = meta.MerchantDB.Select(&gameReport, sql2)
	if err != nil && err != sql.ErrNoRows {
		fmt.Println(err)
		return data
	}
	gameReportMap := map[string]float64{}
	for _, value := range gameReport {
		gameReportMap[value.OperatorId+"-"+strconv.FormatInt(value.GameId, 10)] = value.GameWinlost + value.GameTax
	}
	//充值提现数据
	monthReport := []ReportDailyData{}
	err = meta.MerchantDB.Select(&monthReport, sql3)
	if err != nil && err != sql.ErrNoRows {
		fmt.Println(err)
		return data
	}
	monthReportMap := map[string]ReportDailyData{}
	for _, value := range monthReport {
		monthReportMap[value.OperatorId] = value
	}

	//构造返回数据
	for key, value := range data.D {
		apiFeeRate := map[string]float64{}
		json.Unmarshal([]byte(value.ApiFee), &apiFeeRate)
		OperatorId := value.OperatorId
		totalApiFee := 0.0
		for gameId, rate := range apiFeeRate {
			sigleApi := gameReportMap[OperatorId+"-"+gameId] * rate
			if sigleApi > 0 {
				totalApiFee += sigleApi
			}
		}

		data.D[key].ApiFee = strconv.FormatFloat(totalApiFee, 'f', 2, 64)
		data.D[key].Deposit = monthReportMap[OperatorId].Deposit
		data.D[key].DepositFee = monthReportMap[OperatorId].Deposit * value.DepositFee
		data.D[key].Withdraw = monthReportMap[OperatorId].Withdraw
		data.D[key].WithdrawFee = monthReportMap[OperatorId].Withdraw * value.WithdrawFee
		data.D[key].TotalProfit = data.D[key].Deposit - data.D[key].DepositFee - data.D[key].Withdraw - data.D[key].WithdrawFee - totalApiFee
	}
	return data
}

// 业务员对账表
func reportBusinessApiFeeList(param ReportApiFeeParam, fromDate string, toDate string, queryRange string) ReportApiFeeRetData {
	data := ReportApiFeeRetData{}
	where1, where2, where3 := "1=1", "1=1", "1=1"

	if param.OperatorId != "" {
		where1 += " and a.operator_id=" + param.OperatorId
		where2 += " and operator_id=" + param.OperatorId
		where3 += " and operator_id=" + param.OperatorId

	}
	if param.BusinessId != "" {
		where1 += " and a.id=" + param.BusinessId
		where2 += " and business_id=" + param.BusinessId
		where3 += " and business_id=" + param.BusinessId

	}
	// 1.获取待查询的业务员列表
	where1 += queryRange
	offset := (param.Page - 1) * param.PageSize
	sql1 := "select a.id as business_id,a.account_name business_name,a.operator_id,b.operator_name,b.recharge_fee as deposit_fee,b.withdraw_fee,api_fee from tbl_business_info a inner join  tbl_operator_info as b on b.id=a.operator_id where " + where1 + " order by a.id asc limit " + strconv.Itoa(param.PageSize) + " offset " + strconv.Itoa(offset)
	fmt.Println(sql1)
	// 2.获取各渠道当月的 各平台盈亏，计算api费用
	where2 += " and create_date>='" + fromDate + "' and create_date<='" + toDate + "'"
	sql2 := "select business_id,game_id,ifnull(sum(game_winlost),0) game_winlost,ifnull(sum(game_tax),0) game_tax from tbl_report_game_daily where " + where2 + " and  business_id>0 group by business_id,game_id"
	fmt.Println(sql2)
	// 3.获取各渠道当月的 充值，提现等数据
	where3 += " and create_date>='" + fromDate + "' and create_date<='" + toDate + "'"
	sql3 := "select business_id,ifnull(sum(deposit),0) deposit,ifnull(sum(withdraw),0) withdraw from tbl_report_daily where " + where3 + " and business_id>0  group by business_id"
	fmt.Println(sql3)
	err := meta.MerchantDB.Get(&data.T, "select count(1) from tbl_business_info where "+where1)
	if err != nil && err != sql.ErrNoRows {
		return data
	}

	err = meta.MerchantDB.Select(&data.D, sql1)
	if err != nil && err != sql.ErrNoRows {
		fmt.Println(err)
		return data
	}
	fmt.Println(data.D)
	//游戏数据
	gameReport := []ReportGameDailyData{}
	err = meta.MerchantDB.Select(&gameReport, sql2)
	if err != nil && err != sql.ErrNoRows {
		fmt.Println(err)
		return data
	}
	gameReportMap := map[string]float64{}
	for _, value := range gameReport {
		gameReportMap[value.BusinessId+"-"+strconv.FormatInt(value.GameId, 10)] = value.GameWinlost + value.GameTax
	}
	//充值提现数据
	monthReport := []ReportDailyData{}
	err = meta.MerchantDB.Select(&monthReport, sql3)
	if err != nil && err != sql.ErrNoRows {
		fmt.Println(err)
		return data
	}
	monthReportMap := map[string]ReportDailyData{}
	for _, value := range monthReport {
		monthReportMap[value.BusinessId] = value
	}

	//构造返回数据
	for key, value := range data.D {
		apiFeeRate := map[string]float64{}
		json.Unmarshal([]byte(value.ApiFee), &apiFeeRate)
		BusinessId := value.BusinessId
		totalApiFee := 0.0
		for gameId, rate := range apiFeeRate {
			sigleApi := gameReportMap[BusinessId+"-"+gameId] * rate
			if sigleApi > 0 {
				totalApiFee += sigleApi
			}
		}

		data.D[key].ApiFee = strconv.FormatFloat(totalApiFee, 'f', 2, 64)
		data.D[key].Deposit = monthReportMap[BusinessId].Deposit
		data.D[key].DepositFee = monthReportMap[BusinessId].Deposit * value.DepositFee
		data.D[key].Withdraw = monthReportMap[BusinessId].Withdraw
		data.D[key].WithdrawFee = monthReportMap[BusinessId].Withdraw * value.WithdrawFee
		data.D[key].TotalProfit = data.D[key].Deposit - data.D[key].DepositFee - data.D[key].Withdraw - data.D[key].WithdrawFee - totalApiFee
	}
	return data
}
