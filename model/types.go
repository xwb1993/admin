package model

type MemberVipData struct {
	D []MemberVip `json:"d" cbor:"d"`
	T int64       `json:"t" cbor:"t"`
	S uint        `json:"s" cbor:"s"`
}

// 结构体定义
type MemberVip struct {
	Vip                  int     `json:"vip" db:"vip" cbor:"vip"`                                           //会员等级
	Name                 string  `json:"name" db:"name" cbor:"name"`                                        //会员等级名称
	DepositAmount        int64   `json:"deposit_amount" db:"deposit_amount" cbor:"deposit_amount"`          //对应的积分
	Flow                 int64   `json:"flow" db:"flow" cbor:"flow"`                                        //升级对应的流水
	KeepFlow             int64   `json:"keep_flow" db:"keep_flow" cbor:"keep_flow"`                         //保级对应的流水
	Amount               string  `json:"amount" db:"amount" cbor:"amount"`                                  //升级奖励金额
	FreeWithdrawNum      int     `json:"free_withdraw_num" db:"free_withdraw_num" cbor:"free_withdraw_num"` //免费提款次数
	WithdrawLimit        int     `json:"withdraw_limit" db:"withdraw_limit" cbor:"withdraw_limit"`          //提款限额
	RebateRate           string  `json:"rebate_rate" db:"rebate_rate" cbor:"rebate_rate"`                   //返水比例
	Props                int     `json:"props" db:"props" cbor:"props"`                                     //道具
	UpdatedAt            int64   `json:"updated_at" db:"updated_at" cbor:"updated_at"`                      //更新时间
	CreatedAt            int64   `json:"created_at" db:"created_at" cbor:"created_at"`                      //创建时间
	WeekAward            float64 `json:"week_award" db:"week_award"`                                        //周奖励金额
	MonthAward           float64 `json:"month_award" db:"month_award"`                                      //月奖励金额
	WithdrawRate         float64 `json:"withdraw_rate" db:"withdraw_rate"`                                  //提现手续费
	WithdrawSingleLimit  int     `json:"withdraw_single_limit" db:"withdraw_single_limit"`                  //提现单笔限额
	ChargeExtraAwardRate float64 `json:"charge_extra_award_rate" db:"charge_extra_award_rate"`              //每笔充值赠送
}

type PromoSignConfigData struct {
	D []PromoSignConfig `json:"d" cbor:"d"`
	T int64             `json:"t" cbor:"t"`
	S uint              `json:"s" cbor:"s"`
}

type PromoSignConfig struct {
	Vip             int    `json:"vip" cbor:"vip" db:"vip"`                                           //会员等级
	Sign1Amount     string `json:"sign1_amount" cbor:"sign1_amount" db:"sign1_amount"`                //第一天签到彩金金额
	Sign2Amount     string `json:"sign2_amount" cbor:"sign2_amount" db:"sign2_amount"`                //第二天签到彩金金额
	Sign3Amount     string `json:"sign3_amount" cbor:"sign3_amount" db:"sign3_amount"`                //第三天签到彩金金额
	Sign4Amount     string `json:"sign4_amount" cbor:"sign4_amount" db:"sign4_amount"`                //第四天签到彩金金额
	Sign5Amount     string `json:"sign5_amount" cbor:"sign5_amount" db:"sign5_amount"`                //第五天签到彩金金额
	Sign6Amount     string `json:"sign6_amount" cbor:"sign6_amount" db:"sign6_amount"`                //第六天签到彩金金额
	Sign7Amount     string `json:"sign7_amount" cbor:"sign7_amount" db:"sign7_amount"`                //第七天签到彩金金额
	SignWeekAmount  string `json:"sign_week_amount" cbor:"sign_week_amount" db:"sign_week_amount"`    //周签到彩金金额
	SignMonthAmount string `json:"sign_month_amount" cbor:"sign_month_amount" db:"sign_month_amount"` //月签到彩金金额
}

type PromoSignRecordData struct {
	D []PromoSignRecord `json:"d" cbor:"d"`
	T int64             `json:"t" cbor:"t"`
	S uint              `json:"s" cbor:"s"`
}

type PromoSignRecord struct {
	UID       string `json:"uid" cbor:"uid" db:"uid"`
	Username  string `json:"username" cbor:"username" db:"username"`          //会员名
	Vip       int    `json:"vip" cbor:"vip" db:"vip"`                         //签到时的会员等级
	SignWeek1 string `json:"sign_week_1" cbor:"sign_week_1" db:"sign_week_1"` //周第一天签到
	SignWeek2 string `json:"sign_week_2" cbor:"sign_week_2" db:"sign_week_2"` //周第二天签到
	SignWeek3 string `json:"sign_week_3" cbor:"sign_week_3" db:"sign_week_3"` //周第三天签到
	SignWeek4 string `json:"sign_week_4" cbor:"sign_week_4" db:"sign_week_4"` //周第四天签到
	SignWeek5 string `json:"sign_week_5" cbor:"sign_week_5" db:"sign_week_5"` //周第五天签到
	SignWeek6 string `json:"sign_week_6" cbor:"sign_week_6" db:"sign_week_6"` //周第六天签到
	SignWeek7 string `json:"sign_week_7" cbor:"sign_week_7" db:"sign_week_7"` //周第七天签到
	Sign1     string `json:"sign1" cbor:"sign1" db:"sign1"`                   //第一天签到
	Sign2     string `json:"sign2" cbor:"sign2" db:"sign2"`                   //第二天签到
	Sign3     string `json:"sign3" cbor:"sign3" db:"sign3"`                   //第三天签到
	Sign4     string `json:"sign4" cbor:"sign4" db:"sign4"`                   //第四天签到
	Sign5     string `json:"sign5" cbor:"sign5" db:"sign5"`                   //第五天签到
	Sign6     string `json:"sign6" cbor:"sign6" db:"sign6"`                   //第六天签到
	Sign7     string `json:"sign7" cbor:"sign7" db:"sign7"`                   //第七天签到
	Sign8     string `json:"sign8" cbor:"sign8" db:"sign8"`                   //第八天签到
	Sign9     string `json:"sign9" cbor:"sign9" db:"sign9"`                   //第九天签到
	Sign10    string `json:"sign10" cbor:"sign10" db:"sign10"`                //第十天签到
	Sign11    string `json:"sign11" cbor:"sign11" db:"sign11"`                //第十一天签到
	Sign12    string `json:"sign12" cbor:"sign12" db:"sign12"`                //第十二天签到
	Sign13    string `json:"sign13" cbor:"sign13" db:"sign13"`                //第十三天签到
	Sign14    string `json:"sign14" cbor:"sign14" db:"sign14"`                //第十四天签到
	Sign15    string `json:"sign15" cbor:"sign15" db:"sign15"`                //第十五天签到
	Sign16    string `json:"sign16" cbor:"sign16" db:"sign16"`                //第十六天签到
	Sign17    string `json:"sign17" cbor:"sign17" db:"sign17"`                //第十七天签到
	Sign18    string `json:"sign18" cbor:"sign18" db:"sign18"`                //第十八天签到
	Sign19    string `json:"sign19" cbor:"sign19" db:"sign19"`                //第十九天签到
	Sign20    string `json:"sign20" cbor:"sign20" db:"sign20"`                //第二十天签到
	Sign21    string `json:"sign21" cbor:"sign21" db:"sign21"`                //第二十一天签到
	Sign22    string `json:"sign22" cbor:"sign22" db:"sign22"`                //第二十二天签到
	Sign23    string `json:"sign23" cbor:"sign23" db:"sign23"`                //第二十三天签到
	Sign24    string `json:"sign24" cbor:"sign24" db:"sign24"`                //第二十四天签到
	Sign25    string `json:"sign25" cbor:"sign25" db:"sign25"`                //第二十五天签到
	Sign26    string `json:"sign26" cbor:"sign26" db:"sign26"`                //第二十六天签到
	Sign27    string `json:"sign27" cbor:"sign27" db:"sign27"`                //第二十七天签到
	Sign28    string `json:"sign28" cbor:"sign28" db:"sign28"`                //第二十八天签到
	Sign29    string `json:"sign29" cbor:"sign29" db:"sign29"`                //第二十九天签到
	Sign30    string `json:"sign30" cbor:"sign30" db:"sign30"`                //第三十天签到
	LastSign  string `json:"last_sign" cbor:"last_sign" db:"last_sign"`       //最后一次签到时间yyyy-mm-dd
}

type PromoSignRewardRecordData struct {
	D []PromoSignRewardRecord `json:"d" cbor:"d"`
	T int64                   `json:"t" cbor:"t"`
	S uint                    `json:"s" cbor:"s"`
}

type PromoSignRewardRecord struct {
	ID        string `json:"id" db:"id" cbor:"id"`                         //id
	UID       string `json:"uid" db:"uid" cbor:"uid"`                      //uid
	Username  string `json:"username" db:"username" cbor:"username"`       //会员名
	Vip       int    `json:"vip" db:"vip" cbor:"vip"`                      //vip
	Day       int    `json:"day" db:"day" cbor:"day"`                      //第几天
	MonthDay  int    `json:"month_day" db:"month_day" cbor:"month_day"`    //月签到第几天
	Amount    string `json:"amount" db:"amount" cbor:"amount"`             //奖金金额
	CreatedAt int64  `json:"created_at" db:"created_at" cbor:"created_at"` //记录时间
}

type PromoTreasureConfigData struct {
	D []PromoTreasureConfig `json:"d" cbor:"d"`
	T int64                 `json:"t" cbor:"t"`
	S uint                  `json:"s" cbor:"s"`
}

type PromoTreasureConfig struct {
	ID          string `json:"id" cbor:"id" db:"id"`
	InviteNum   uint32 `json:"invite_num" cbor:"invite_num" db:"invite_num"`       //邀请人数
	Amount      string `json:"amount" cbor:"amount" db:"amount"`                   //宝箱金额
	TotalAmount string `json:"total_amount" cbor:"total_amount" db:"total_amount"` //累计宝箱金额
}

type PromoTreasureRecordData struct {
	D []PromoTreasureRecord `json:"d" cbor:"d"`
	T int64                 `json:"t" cbor:"t"`
	S uint                  `json:"s" cbor:"s"`
}

type PromoTreasureRecord struct {
	ID        string `json:"id" cbor:"id" db:"id"`
	UID       string `json:"uid" cbor:"uid" db:"uid"`
	Username  string `json:"username" cbor:"username" db:"username"`       //会员名
	InviteNum uint32 `json:"invite_num" cbor:"invite_num" db:"invite_num"` //邀请人数
	Amount    string `json:"amount" cbor:"amount" db:"amount"`             //宝箱金额
}

type MemberBalance struct {
	UID        string `json:"-" cbor:"-" db:"uid"`
	Brl        string `json:"brl" cbor:"brl" db:"brl"`                         //巴西余额
	LockAmount string `json:"lock_amount" cbor:"lock_amount" db:"lock_amount"` //锁定余额
}

// 账变表
type MemberTransaction struct {
	AfterAmount  string `db:"after_amount" json:"after_amount" cbor:"after_amount"`    //账变后的金额
	Amount       string `db:"amount" json:"amount" cbor:"amount"`                      //用户填写的转换金额
	BeforeAmount string `db:"before_amount" json:"before_amount" cbor:"before_amount"` //账变前的金额
	BillNo       string `db:"bill_no" json:"bill_no" cbor:"bill_no"`                   //转账|充值|提现ID
	CashType     int    `db:"cash_type" json:"cash_type" cbor:"cash_type"`             //0:转入1:转出2:转入失败补回3:转出失败扣除4:存款5:提现
	CreatedAt    int64  `db:"created_at" json:"created_at" cbor:"created_at"`          //
	ID           string `db:"id" json:"id" cbor:"id"`                                  //
	UID          string `db:"uid" json:"uid" cbor:"uid"`                               //用户ID
	Username     string `db:"username" json:"username" cbor:"username"`                //用户名
	Remark       string `db:"remark" json:"remark" cbor:"remark"`                      //备注
}

// 帐变类型
type TransType struct {
	ID     string `json:"id" cbor:"id" db:"id"`
	Name   string `json:"name" cbor:"name" db:"name"`          //帐变中文名
	EnName string `json:"en_name" cbor:"en_name" db:"en_name"` //帐变英文名
}

// 游戏列表返回数据结构
type GameData struct {
	D []Game_t `json:"d" cbor:"d"`
	T int64    `json:"t" cbor:"t"`
	S uint     `json:"s" cbor:"s"`
}

// 数据库 游戏字段
type Game_t struct {
	ID         string   `json:"id" db:"id" cbor:"id"`
	PlatformID string   `json:"platform_id" db:"platform_id" cbor:"platform_id"` //场馆ID
	Name       string   `json:"name" db:"name" cbor:"name"`                      //游戏名称
	EnName     string   `json:"en_name" db:"en_name" cbor:"en_name"`             //英文名称
	BrAlias    string   `json:"br_alias" db:"br_alias" cbor:"br_alias"`          //巴西别名
	ClientType string   `json:"client_type" db:"client_type" cbor:"client_type"` //0:all 1:web 2:h5 4:app 此处值为支持端的数值之和
	GameType   int      `json:"game_type" db:"game_type" cbor:"game_type"`       //游戏类型:1=真人,2=捕鱼,3=电子,4=体育
	GameID     string   `json:"game_id" db:"game_id" cbor:"game_id"`             //游戏ID
	Img        string   `json:"img" db:"img" cbor:"img"`                         //手机图片
	Online     int      `json:"online" db:"online" cbor:"online"`                //0 下线 1上线
	IsHot      int      `json:"is_hot" db:"is_hot" cbor:"is_hot"`                //0 正常 1热门
	IsFav      int      `json:"is_fav" db:"is_fav" cbor:"is_fav"`                //0 正常 1热门
	IsNew      int      `json:"is_new" db:"is_new" cbor:"is_new"`                //是否最新:0=否,1=是
	Sorting    int      `json:"sorting" db:"sorting" cbor:"sorting"`             //排序
	CreatedAt  int64    `json:"created_at" db:"created_at" cbor:"created_at"`    //添加时间
	TagId      string   `db:"tag_id" json:"tag_id" cbor:"tag_id"`                //标签集合
	TagIds     []string `json:"tag_ids" cbor:"tag_ids"`
}

type PlatformData struct {
	T int64      `json:"t" cbor:"t"`
	D []Platform `json:"d" cbor:"d"`
	S int        `json:"s" cbor:"s"`
}

type Platform struct {
	ID          string `db:"id" json:"id" cbor:"id"`
	Name        string `db:"name" json:"name" cbor:"name"`
	GameType    int    `db:"game_type" json:"game_type" cbor:"game_type"`
	State       int    `db:"state" json:"state" cbor:"state"`
	Maintained  int    `db:"maintained" json:"maintained" cbor:"maintained"`
	Seq         int    `db:"seq" json:"seq" cbor:"seq"`
	Logo        string `db:"logo" json:"logo" cbor:"logo"`
	CreatedAt   int32  `db:"created_at" json:"created_at" cbor:"created_at"`
	UpdatedAt   int32  `db:"updated_at" json:"updated_at" cbor:"updated_at"`
	UpdatedUID  string `db:"updated_uid" json:"updated_uid" cbor:"updated_uid"`
	UpdatedName string `db:"updated_name" json:"updated_name" cbor:"updated_name"`
}

type platJson struct {
	ID         string `db:"id" json:"id" cbor:"id"`
	Name       string `db:"name" json:"name" cbor:"name"`
	GameType   int    `db:"game_type" json:"game_type" cbor:"game_type"`
	State      int    `db:"state" json:"state" cbor:"state"`
	Maintained int    `db:"maintained" json:"maintained" cbor:"maintained"`
	Seq        int    `db:"seq" json:"seq" cbor:"seq"`
	Logo       string `db:"logo" json:"logo" cbor:"logo"`
}

type PromoDepositConfigData struct {
	D []PromoDepositConfig `json:"d" cbor:"d"`
	T int64                `json:"t" cbor:"t"`
	S uint                 `json:"s" cbor:"s"`
}

type PromoDepositConfig struct {
	Id        string  `json:"id" cbor:"id" db:"id"`
	Name      string  `json:"name" cbor:"name" db:"name"`                   //配置名称
	Bonus     float64 `json:"bonus" cbor:"bonus" db:"bonus"`                //奖金比例
	Flow      float64 `json:"flow" cbor:"flow" db:"flow"`                   //流水倍数
	MaxAmount float64 `json:"max_amount" cbor:"max_amount" db:"max_amount"` //最大金额
	MinAmount float64 `json:"min_amount" cbor:"min_amount" db:"min_amount"` //最小金额
	Ty        int     `json:"ty" cbor:"ty" db:"ty"`                         //1首存2次存
	Sort      int     `json:"sort" cbor:"sort" db:"sort"`
}

// paymentDepositResp 存款
type paymentDepositResp struct {
	Addr    string `json:"addr" cbor:"addr"`         // 三方返回的充值地址
	QrCode  string `json:"qr_code" cbor:"qr_code"`   // 充值二维码地址
	OrderID string `json:"order_id" cbor:"order_id"` //我方订单号
	Oid     string `json:"oid" cbor:"oid"`           //三方的订单号
}

// paymentWithdrawResp 提款
type paymentWithdrawResp struct {
	OrderID string `json:"order_id" cbor:"order_id"` //我方订单号
	Oid     string `json:"oid" cbor:"oid"`           //三方的订单号
}

type MemBalance struct {
	Brl               float64 `json:"brl" db:"brl" cbor:"brl"`                                                 //全部余额
	BrlAmount         float64 `json:"brl_amount" db:"-" cbor:"brl_amount"`                                     //可提现余额
	UnlockAmount      float64 `json:"unlock_amount" db:"unlock_amount" cbor:"unlock_amount"`                   //解锁余额
	LockAmount        float64 `json:"lock_amount" db:"lock_amount" cbor:"lock_amount"`                         //提现中余额
	AgencyAmount      float64 `json:"agency_amount" db:"agency_amount" cbor:"agency_amount"`                   //推广账户余额
	DepositAmount     float64 `json:"deposit_amount" db:"-" cbor:"deposit_amount"`                             //存款账户余额
	DepositBalance    float64 `json:"deposit_balance" db:"-" cbor:"deposit_balance"`                           //存款可提现余额
	DepositLockAmount float64 `json:"deposit_lock_amount" db:"deposit_lock_amount" cbor:"deposit_lock_amount"` //存款不可提现余额
	AgencyBalance     float64 `json:"agency_balance" db:"-" cbor:"agency_balance"`                             //推广账户可提现余额
	AgencyLockAmount  float64 `json:"agency_lock_amount" db:"agency_lock_amount" cbor:"agency_lock_amount"`    //推广账户不可提现余额
}

type TblMemberBalance struct {
	Prefix            string  `json:"prefix" db:"prefix" cbor:"prefix"`
	Uid               string  `json:"uid" db:"uid" cbor:"uid"`
	Brl               float64 `json:"brl" db:"brl" cbor:"brl"`                                                 //全部余额
	BrlAmount         float64 `json:"brl_amount" db:"-" cbor:"brl_amount"`                                     //可提现余额
	UnlockAmount      float64 `json:"unlock_amount" db:"unlock_amount" cbor:"unlock_amount"`                   //解锁余额
	LockAmount        float64 `json:"lock_amount" db:"lock_amount" cbor:"lock_amount"`                         //提现中余额
	AgencyAmount      float64 `json:"agency_amount" db:"agency_amount" cbor:"agency_amount"`                   //推广账户余额
	DepositAmount     float64 `json:"deposit_amount" db:"-" cbor:"deposit_amount"`                             //存款账户余额
	DepositBalance    float64 `json:"deposit_balance" db:"-" cbor:"deposit_balance"`                           //存款可提现余额
	DepositLockAmount float64 `json:"deposit_lock_amount" db:"deposit_lock_amount" cbor:"deposit_lock_amount"` //存款不可提现余额
	AgencyBalance     float64 `json:"agency_balance" db:"-" cbor:"agency_balance"`                             //推广账户可提现余额
	AgencyLockAmount  float64 `json:"agency_lock_amount" db:"agency_lock_amount" cbor:"agency_lock_amount"`    //推广账户不可提现余额
}

type tblPromoInviteRecord struct {
	Id             string  `db:"id" cbor:"id" json:"id"`
	Uid            string  `db:"uid" cbor:"uid" json:"uid"`
	Username       string  `db:"username" cbor:"username" json:"username"`
	Lvl            int     `db:"lvl" cbor:"lvl" json:"lvl"`                                        //123 一级二级三级
	ChildUid       string  `db:"child_uid" cbor:"child_uid" json:"child_uid"`                      //下级的uid
	ChildUsername  string  `db:"child_username" cbor:"child_username" json:"child_username"`       //下级的账号
	FirstDepositAt int     `db:"first_deposit_at" cbor:"first_deposit_at" json:"first_deposit_at"` //首存时间
	DepositAmount  float64 `db:"deposit_amount" cbor:"deposit_amount" json:"deposit_amount"`       //存款金额
	BonusAmount    float64 `db:"bonus_amount" cbor:"bonus_amount" json:"bonus_amount"`             //奖金
	CreatedAt      uint32  `db:"created_at" cbor:"created_at" json:"created_at"`                   //注册时间
	State          int     `db:"state" cbor:"state" json:"state"`                                  //1注册未充值2充值未结算3已结算4过期
}

type TblBonusConfig struct {
	Id                string  `db:"id" cbor:"id" json:"id"`
	Name              string  `db:"name" cbor:"name" json:"name"`
	Code              string  `db:"code" cbor:"code" json:"code"`
	RechargeAmountLv1 float64 `db:"recharge_amount_lv1" cbor:"recharge_amount_lv1" json:"recharge_amount_lv1"`
	RateAmountLv1     float64 `db:"rate_amount_lv1" cbor:"rate_amount_lv1" json:"rate_amount_lv1"`
	RechargeAmountLv2 float64 `db:"recharge_amount_lv2" cbor:"recharge_amount_lv2" json:"recharge_amount_lv2"`
	RateAmountLv2     float64 `db:"rate_amount_lv2" cbor:"rate_amount_lv2" json:"rate_amount_lv2"`
	RechargeAmountLv3 float64 `db:"recharge_amount_lv3" cbor:"recharge_amount_lv3" json:"recharge_amount_lv3"`
	RateAmountLv3     float64 `db:"rate_amount_lv3" cbor:"rate_amount_lv3" json:"rate_amount_lv3"`
	LvlOneRebate      float64 `db:"lvl_one_rebate" cbor:"lvl_one_rebate" json:"lvl_one_rebate"`
	LvlTwoRebate      float64 `db:"lvl_two_rebate" cbor:"lvl_two_rebate" json:"lvl_two_rebate"`
	LvlThreeRebate    float64 `db:"lvl_three_rebate" cbor:"lvl_three_rebate" json:"lvl_three_rebate"`
}

type BonusConfigData struct {
	D []TblBonusConfig `cbor:"d" json:"d"`
	T int              `cbor:"t" json:"t"`
	S uint             `cbor:"s" json:"s"`
}

type TblOperatorInfo struct {
	Id                 string  `json:"id" db:"id"`
	OperatorName       string  `json:"operator_name" db:"operator_name"`               // 渠道名称
	Password           string  `json:"password" db:"password"`                         // 密码
	ProxyExtendLink    string  `json:"proxy_extend_link" db:"proxy_extend_link"`       // 全民代理推广链接
	WithdrawRemain     float64 `json:"withdraw_remain" db:"withdraw_remain"`           // 渠道额度
	RechargeFee        float64 `json:"recharge_fee" db:"recharge_fee"`                 // 充值手续费比例
	WithdrawFee        float64 `json:"withdraw_fee" db:"withdraw_fee"`                 // 提现手续费比例
	ApiFee             string  `json:"api_fee" db:"api_fee"`                           // api费用
	SingleUrl          string  `json:"single_url" db:"single_url"`                     // 独立域名
	YoutubeShareLink   string  `json:"youtube_share_link" db:"youtube_share_link"`     // YouTube分享链接
	TwitterShareLink   string  `json:"twitter_share_link" db:"twitter_share_link"`     // Twitter分享链接
	TelegramShareLink  string  `json:"telegram_share_link" db:"telegram_share_link"`   // Telegram分享链接
	InstagramShareLink string  `json:"instagram_share_link" db:"instagram_share_link"` // Instagram分享链接
	CreatedAt          int64   `json:"created_at" db:"created_at"`                     // 创建时间
	CreatedUid         string  `json:"created_uid" db:"created_uid"`                   // 创建人uid
	CreatedName        string  `json:"created_name" db:"created_name"`                 // 创建人名
	UpdatedAt          int64   `json:"updated_at" db:"updated_at"`                     // 修改时间
	UpdatedUid         string  `json:"updated_uid" db:"updated_uid"`                   // 修改人uid
	UpdatedName        string  `json:"updated_name" db:"updated_name"`                 // 修改人名
	State              string  `json:"state" db:"state"`                               // 0 未启用 1 启用
}

type OperatorInfoData struct {
	D []TblOperatorInfo `cbor:"d" json:"d"`
	T int               `cbor:"t" json:"t"`
	S uint              `cbor:"s" json:"s"`
}

type TblGameConfig struct {
	Id          string `db:"id" json:"id"`
	CfgType     string `db:"cfg_type"  json:"cfg_type"`
	CfgValue    string `db:"cfg_value"  json:"cfg_value"`
	Description string `db:"description" json:"description"`
	TypeId      string `db:"type_id"  json:"type_id"`
}

type GameConfigData struct {
	D []TblGameConfig `json:"d"`
	T int             `json:"t"`
	S uint            `json:"s"`
}

type TblBusinessInfo struct {
	Id           string `json:"id" db:"id"`
	AccountName  string `json:"account_name" db:"account_name"`   // 业务员名称
	LoginAccount string `json:"login_account" db:"login_account"` // 登录账号
	Password     string `json:"password" db:"password"`           // 密码
	InviteUrl    string `json:"invite_url" db:"invite_url"`       // 邀请链接
	OperatorId   string `json:"operator_id" db:"operator_id"`     // 渠道id
	CreatedAt    int64  `json:"created_at" db:"created_at"`       // 创建时间
	UpdatedAt    int64  `json:"updated_at" db:"updated_at"`       // 修改时间
	Type         string `json:"type" db:"type"`                   // 业务员级别
	ParentId     string `json:"parent_id" db:"parent_id"`         // 上级业务员id
	ParentName   string `json:"parent_name" db:"parent_name"`     // 上级业务员名称
}

type BusinessInfoData struct {
	D []TblBusinessInfo `json:"d"`
	T int               `json:"t"`
	S uint              `json:"s"`
}

// 日报表参数，通用
type ReportDailyParam struct {
	Uid        int    `json:"uid"`
	OperatorId string `json:"operator_id"` // 渠道id
	BusinessId string `json:"business_id"` // 业务员id
	StartTime  string `json:"start_time"`  // 查询开始时间
	EndTime    string `json:"end_time"`    // 查询结束时间
	Page       int    `json:"page"`
	PageSize   int    `json:"page_size"`
	OderBY     string `json:"order_by"`
	OderType   string `json:"order_type"`
	IsOperator int    `json:"is_operator"` //是否渠道列表
	IsBusiness int    `json:"is_business"` //是否业务员列表
}

// 玩家日报表 响应数据
type ReportUserDailyRetData struct {
	T int                           `cbor:"t" json:"t"`
	D []ReportUserDailyData         `cbor:"d" json:"d"`
	S ReportUserDailyStatisticsData `cbor:"s" json:"s"`
}

// 玩家日报表
type ReportUserDailyData struct {
	UID              int     `json:"uid" db:"uid"`
	Deposit          float64 `json:"deposit" db:"deposit"`
	FirstDeposit     float64 `json:"first_deposit" db:"first_deposit"`
	Withdraw         float64 `json:"withdraw" db:"withdraw"`
	Running          float64 `json:"running" db:"running"`
	GameRound        int32   `json:"game_round" db:"game_round"`
	GameWinlost      float64 `json:"game_winlost" db:"game_winlost"`
	GameTax          float64 `json:"game_tax" db:"game_tax"` // 游戏税收
	TurntableBonus   float64 `json:"turntable_bonus" db:"turntable_bonus"`
	ProxyInviteBonus float64 `json:"proxy_invite_bonus" db:"proxy_invite_bonus"`
	CreatedAt        string  `json:"created_at" db:"created_at"`
	Username         string  `json:"username" db:"username"`
	OperatorId       string  `json:"operator_id" db:"operator_id"`
	OperatorName     string  `json:"operator_name" db:"operator_name"`
	BusinessId       string  `json:"business_id" db:"business_id"`
	BusinessName     string  `json:"business_name" db:"business_name"`
}

// 玩家日况统计数据
type ReportUserDailyStatisticsData struct {
	Deposit          float64 `json:"deposit" db:"deposit"`
	Withdraw         float64 `json:"withdraw" db:"withdraw"`
	Running          float64 `json:"running" db:"running"`
	GameRound        int32   `json:"game_round" db:"game_round"`
	GameWinlost      float64 `json:"game_winlost" db:"game_winlost"`
	GameTax          float64 `json:"game_tax" db:"game_tax"` // 游戏税收
	TurntableBonus   float64 `json:"turntable_bonus" db:"turntable_bonus"`
	ProxyInviteBonus float64 `json:"proxy_invite_bonus" db:"proxy_invite_bonus"`
}

// 日况统计表 响应数据
type ReportDailyRetData struct {
	T int                  `cbor:"t" json:"t"`
	D []ReportDailyData    `cbor:"d" json:"d"`
	S ReportStatisticsData `cbor:"s" json:"s"`
}

// 日况统计表
type ReportDailyData struct {
	Id               int64   `json:"id" db:"id"`
	BusinessId       string  `json:"business_id" db:"business_id"`               // 业务员id
	OperatorId       string  `json:"operator_id" db:"operator_id"`               // 渠道id
	DepositNum       float64 `json:"deposit_num" db:"deposit_num"`               // 充值人数
	Deposit          float64 `json:"deposit" db:"deposit"`                       // 充值数量
	FirstDepositNum  float64 `json:"first_deposit_num" db:"first_deposit_num"`   // 首充人数
	FirstDeposit     float64 `json:"first_deposit" db:"first_deposit"`           // 首充数量
	WithdrawNum      float64 `json:"withdraw_num" db:"withdraw_num"`             // 提现人数
	Withdraw         float64 `json:"withdraw" db:"withdraw"`                     // 提现数量
	Running          float64 `json:"running" db:"running"`                       // 流水
	GameRound        int32   `json:"game_round" db:"game_round"`                 // 游戏局数
	GameTax          float64 `json:"game_tax" db:"game_tax"`                     // 游戏税收
	GameWinlost      float64 `json:"game_winlost" db:"game_winlost"`             // 游戏赢亏
	TurntableBonus   float64 `json:"turntable_bonus" db:"turntable_bonus"`       // 转盘奖励
	ProxyInviteBonus float64 `json:"proxy_invite_bonus" db:"proxy_invite_bonus"` // 代理邀请奖励
	CreateDate       string  `json:"create_date" db:"create_date"`               // 时间
	OperatorName     string  `json:"operator_name" db:"operator_name"`           // 渠道账号
	BusinessName     string  `json:"business_name" db:"business_name"`           // 业务员账号
}

// 日况统计数据
type ReportStatisticsData struct {
	DepositNum       float64 `json:"deposit_num" db:"deposit_num"`               // 充值人数
	Deposit          float64 `json:"deposit" db:"deposit"`                       // 充值数量
	FirstDepositNum  float64 `json:"first_deposit_num" db:"first_deposit_num"`   // 首充人数
	FirstDeposit     float64 `json:"first_deposit" db:"first_deposit"`           // 首充数量
	WithdrawNum      float64 `json:"withdraw_num" db:"withdraw_num"`             // 提现人数
	Withdraw         float64 `json:"withdraw" db:"withdraw"`                     // 提现数量
	Running          float64 `json:"running" db:"running"`                       // 流水
	GameRound        int32   `json:"game_round" db:"game_round"`                 // 游戏局数
	GameTax          float64 `json:"game_tax" db:"game_tax"`                     // 游戏税收
	GameWinlost      float64 `json:"game_winlost" db:"game_winlost"`             // 游戏赢亏
	TurntableBonus   float64 `json:"turntable_bonus" db:"turntable_bonus"`       // 转盘奖励
	ProxyInviteBonus float64 `json:"proxy_invite_bonus" db:"proxy_invite_bonus"` // 代理邀请奖励
}

// 测试号接收参数
type MemberTestParam struct {
	Uid        int    `json:"uid"`
	StartTime  string `json:"start_time"`  // 查询开始时间
	EndTime    string `json:"end_time"`    // 查询结束时间
	OperatorId string `json:"operator_id"` // 渠道id
	Page       int    `json:"page"`
	PageSize   int    `json:"page_size"`
}

// 游戏日况统计表 响应数据
type ReportGameDailyRetData struct {
	T int                      `cbor:"t" json:"t"`
	D []ReportGameDailyData    `cbor:"d" json:"d"`
	S ReportGameStatisticsData `cbor:"s" json:"s"`
}

// 游戏日报表参数，通用
type GameDailyParam struct {
	GameId     int    `json:"game_id"`
	OperatorId string `json:"operator_id"` // 渠道id
	BusinessId string `json:"business_id"` // 业务员id
	StartTime  string `json:"start_time"`  // 查询开始时间
	EndTime    string `json:"end_time"`    // 查询结束时间
	Page       int    `json:"page"`
	PageSize   int    `json:"page_size"`
	OderBY     string `json:"order_by"`
	OderType   string `json:"order_type"`
	IsOperator int    `json:"is_operator"` //是否渠道列表
	IsBusiness int    `json:"is_business"` //是否业务员列表
}

// 游戏日况统计表
type ReportGameDailyData struct {
	Id           int64   `json:"id" db:"id"`
	GameId       int64   `json:"game_id" db:"game_id"`             //游戏ID
	GameName     string  `json:"game_name" db:"game_name"`         //游戏名称
	BusinessId   string  `json:"business_id" db:"business_id"`     // 业务员id
	OperatorId   string  `json:"operator_id" db:"operator_id"`     // 渠道id
	Running      float64 `json:"running" db:"running"`             // 流水
	GameRound    int32   `json:"game_round" db:"game_round"`       // 游戏局数
	GameTax      float64 `json:"game_tax" db:"game_tax"`           // 游戏税收
	GameWinlost  float64 `json:"game_winlost" db:"game_winlost"`   // 游戏赢亏
	CreateDate   string  `json:"create_date" db:"create_date"`     // 时间
	OperatorName string  `json:"operator_name" db:"operator_name"` // 渠道账号
	BusinessName string  `json:"business_name" db:"business_name"` // 业务员账号
	Rate         float64 `json:"rate" db:"rate"`                   // 游戏回报率
}

// 游戏总统计数据
type ReportGameStatisticsData struct {
	Running     float64 `json:"running" db:"running"`           // 流水
	GameRound   int32   `json:"game_round" db:"game_round"`     // 游戏局数
	GameTax     float64 `json:"game_tax" db:"game_tax"`         // 游戏税收
	GameWinlost float64 `json:"game_winlost" db:"game_winlost"` // 游戏赢亏
}

// API费用 响应数据
type ReportApiFeeRetData struct {
	T int                `cbor:"t" json:"t"`
	D []ReportApiFeeData `cbor:"d" json:"d"`
}

// API费用参数，通用
type ReportApiFeeParam struct {
	MonthDate  string `json:"month_date"`  //月份 Y-m 格式
	OperatorId string `json:"operator_id"` // 渠道id
	BusinessId string `json:"business_id"` // 业务员id
	Page       int    `json:"page"`
	PageSize   int    `json:"page_size"`
	OderBY     string `json:"order_by"`
	OderType   string `json:"order_type"`
	IsOperator int    `json:"is_operator"` //是否渠道列表
	IsBusiness int    `json:"is_business"` //是否业务员列表
}

// Api费用统计表
type ReportApiFeeData struct {
	MonthDate    string  `json:"month_date" db:"month_date"`
	BusinessId   string  `json:"business_id" db:"business_id"`     // 业务员id
	OperatorId   string  `json:"operator_id" db:"operator_id"`     // 渠道id
	Deposit      float64 `json:"deposit" db:"deposit"`             // 充值数量
	DepositFee   float64 `json:"deposit_fee" db:"deposit_fee"`     // 充值手续费
	Withdraw     float64 `json:"withdraw" db:"withdraw"`           // 提现数量
	WithdrawFee  float64 `json:"withdraw_fee" db:"withdraw_fee"`   // 提现手续费
	ApiFee       string  `json:"api_fee" db:"api_fee"`             // Api费用
	TotalProfit  float64 `json:"total_profit" db:"total_profit"`   // 总利润
	OperatorName string  `json:"operator_name" db:"operator_name"` // 渠道账号
	BusinessName string  `json:"business_name" db:"business_name"` // 业务员账号
}

// 代理日况参数，通用
type ReportProxyDailyParam struct {
	ProxyId    string `json:"proxy_id"`
	OperatorId string `json:"operator_id"` // 渠道id
	BusinessId string `json:"business_id"` // 业务员id
	ParentId   int    `json:"parent_id"`   // 父级id
	StartTime  string `json:"start_time"`  // 查询开始时间
	EndTime    string `json:"end_time"`    // 查询结束时间
	Page       int    `json:"page"`
	PageSize   int    `json:"page_size"`
	OderBY     string `json:"order_by"`
	OderType   string `json:"order_type"`
}

// 代理日况报表 响应数据
type ReportProxyDailyRetData struct {
	T int                    `cbor:"t" json:"t"`
	D []ReportProxyDailyData `cbor:"d" json:"d"`
}

// 代理日况报表
type ReportProxyDailyData struct {
	Id                 int     `json:"id" db:"id"`
	ProxyId            int     `json:"proxy_id" db:"proxy_id"`
	Level              int     `json:"level" db:"level"`
	Deposit            float64 `json:"deposit" db:"deposit"`
	DepositNum         int32   `json:"deposit_num" db:"deposit_num"` // 充值人数
	Withdraw           float64 `json:"withdraw" db:"withdraw"`
	WithdrawNum        int32   `json:"withdraw_num" db:"withdraw_num"`           // 提现人数
	FirstDepositNum    float64 `json:"first_deposit_num" db:"first_deposit_num"` // 首充人数
	FirstDeposit       float64 `json:"first_deposit" db:"first_deposit"`         // 首充数量
	Running            float64 `json:"running" db:"running"`
	GameRound          int32   `json:"game_round" db:"game_round"`
	GameWinlost        float64 `json:"game_winlost" db:"game_winlost"`
	GameTax            float64 `json:"game_tax" db:"game_tax"` // 游戏税收
	TurntableBonus     float64 `json:"turntable_bonus" db:"turntable_bonus"`
	ProxyInviteBonus   float64 `json:"proxy_invite_bonus" db:"proxy_invite_bonus"`
	CreateDate         string  `json:"create_date" db:"create_date"`                   // 时间
	DepositCommiReturn float64 `json:"deposit_commi_return" db:"deposit_commi_return"` //直属下级充值金额超过50的订单,用于代理周佣金返还
	TeamNum            int32   `json:"team_num" db:"team_num"`                         //团队人数
	ValidNum           int32   `json:"valid_num" db:"valid_num"`                       //有效人数
	Username           string  `json:"username" db:"username"`
	OperatorId         string  `json:"operator_id" db:"operator_id"`
	OperatorName       string  `json:"operator_name" db:"operator_name"`
	BusinessId         string  `json:"business_id" db:"business_id"`
	BusinessName       string  `json:"business_name" db:"business_name"`
}

// 玩家控制表
type MemberControllerButton struct {
	Uid                int  `json:"uid" db:"uid"`
	GameButton         int8 `json:"game_button" db:"game_button"`                   //游戏开关
	WithdrawButton     int8 `json:"withdraw_button" db:"withdraw_button"`           //提现开关
	TaxButton          int8 `json:"tax_button" db:"tax_button"`                     //抽税开关
	AutoWithdrawButton int8 `json:"auto_withdraw_button" db:"auto_withdraw_button"` //自动出款开关
	ControlCodeButton  int8 `json:"control_code_button" db:"control_code_button"`   //控码开关
	UnbindButton       int8 `json:"unbind_button" db:"unbind_button"`               //掉绑开关
}

// 登录日志参数
type MemberLoginLogParam struct {
	Uid      int    `json:"uid"`
	LoginIp  string `json:"login_ip"`
	Page     int    `json:"page"`
	PageSize int    `json:"page_size"`
	OderBY   string `json:"order_by"`
	OderType string `json:"order_type"`
}

// 登录日志响应参数
type MemberLoginLogRetData struct {
	T int                  `cbor:"t" json:"t"`
	D []MemberLoginLogData `cbor:"d" json:"d"`
}
type MemberLoginLogData struct {
	Uid          int64  `json:"uid" db:"uid"`
	Device       string `json:"device" db:"device"`
	Username     string `json:"username" db:"username"`
	LoginIp      string `json:"login_ip" db:"login_ip"`
	LoginTime    string `json:"login_time" db:"login_time"`
	BusinessId   string `json:"business_id" db:"business_id"`     // 业务员id
	OperatorId   string `json:"operator_id" db:"operator_id"`     // 渠道id
	OperatorName string `json:"operator_name" db:"operator_name"` // 渠道账号
	BusinessName string `json:"business_name" db:"business_name"` // 业务员账号
}
