package model

import (
	"admin/contrib/helper"
	ryrpc "admin/rpc"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/mitchellh/mapstructure"
)

func MemberRpcQuery(uid string) error {

	memberBalance := ryrpc.SelectParam{
		TableName:      "tbl_member_balance",
		Record:         "*",
		QueryCondition: g.Ex{"uid": uid},
		OperateRedis:   true,
		RedisKey:       fmt.Sprintf("%s:%s", "tbl_member_balance", uid),
	}

	memBalance, err := ryrpc.RpcQuery(memberBalance)
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	fmt.Println(memBalance)
	// 打印确认数据类型
	fmt.Printf("Type: %T\n", memBalance)

	// 构件并初始化一个使用默认设置的 mapstructure.Decoder
	var transaction TblMemberBalance
	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Result:           &transaction,
		WeaklyTypedInput: true,
	})

	if err != nil {
		return err
	}
	// 使用 decoder 将 map 数据解码到 transaction 结构体
	err = decoder.Decode(memBalance)
	if err != nil {
		return err
	}
	fmt.Println(transaction)

	return nil
}

func MemberRpcUpdate(uid string) error {
	record := TblMemberBalance{
		Prefix:            "",
		Uid:               uid,
		Brl:               60,
		BrlAmount:         20,
		UnlockAmount:      30,
		LockAmount:        0,
		AgencyAmount:      50,
		DepositAmount:     0,
		DepositBalance:    0,
		DepositLockAmount: 0,
		AgencyBalance:     0,
		AgencyLockAmount:  0,
	}
	recordBalance := ryrpc.SelectParam{
		TableName:      "tbl_member_balance",
		Record:         record,
		QueryCondition: g.Ex{"uid": uid},
		OperateRedis:   true,
		RedisKey:       fmt.Sprintf("%s:%s", "tbl_member_balance", uid),
	}
	fmt.Println(recordBalance)
	_, err := ryrpc.RpcUpdate(recordBalance)
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}

func MemberRpcInset(uid string) error {
	record := TblMemberBal{
		Prefix:            "",
		Uid:               uid,
		Brl:               100,
		UnlockAmount:      30,
		LockAmount:        20,
		AgencyAmount:      50,
		DepositLockAmount: 50,
		AgencyLockAmount:  70,
	}

	recordBalance := ryrpc.SelectParam{
		TableName:      "tbl_member_balance",
		Record:         record,
		QueryCondition: g.Ex{"uid": uid},
		OperateRedis:   true,
		RedisKey:       fmt.Sprintf("%s:%s", "tbl_member_balance", uid),
	}
	fmt.Println(recordBalance)
	_, err := ryrpc.RpcInsert(recordBalance)
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}

func MemberRpcDel(uid string) error {

	recordBalance := ryrpc.SelectParam{
		TableName:      "tbl_member_balance",
		QueryCondition: g.Ex{"uid": uid},
		OperateRedis:   true,
		RedisKey:       fmt.Sprintf("%s:%s", "tbl_member_balance", uid),
	}
	fmt.Println(recordBalance)
	_, err := ryrpc.RpcDelete(recordBalance)
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}

type TblMemberBal struct {
	Prefix            string  `json:"prefix" db:"prefix" cbor:"prefix"`
	Uid               string  `json:"uid" db:"uid" cbor:"uid"`
	Brl               float64 `json:"brl" db:"brl" cbor:"brl"`                                                 //全部余额
	UnlockAmount      float64 `json:"unlock_amount" db:"unlock_amount" cbor:"unlock_amount"`                   //解锁余额
	LockAmount        float64 `json:"lock_amount" db:"lock_amount" cbor:"lock_amount"`                         //提现中余额
	AgencyAmount      float64 `json:"agency_amount" db:"agency_amount" cbor:"agency_amount"`                   //推广账户余额
	DepositLockAmount float64 `json:"deposit_lock_amount" db:"deposit_lock_amount" cbor:"deposit_lock_amount"` //存款不可提现余额
	AgencyLockAmount  float64 `json:"agency_lock_amount" db:"agency_lock_amount" cbor:"agency_lock_amount"`    //推广账户不可提现余额
}
