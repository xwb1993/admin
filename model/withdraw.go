package model

import (
	"admin/contrib/helper"
	"admin/contrib/validator"
	"database/sql"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"time"
)

type withdrawTotal struct {
	T   sql.NullInt64   `json:"t"`
	Agg sql.NullFloat64 `json:"agg"`
}

// tblWithdraw 出款
type tblWithdraw struct {
	ID             string  `db:"id" json:"id" cbor:"id"`                                        //
	OID            string  `db:"oid" json:"oid" cbor:"oid"`                                     //转账前的金额
	UID            string  `db:"uid" json:"uid" cbor:"uid"`                                     //用户ID
	Username       string  `db:"username" json:"username" cbor:"username"`                      //用户名
	TopId          string  `db:"top_id" json:"top_id" cbor:"top_id"`                            //总代uid
	TopName        string  `db:"top_name" json:"top_name" cbor:"top_name"`                      //总代代理
	ParentId       string  `db:"parent_id" json:"parent_id" cbor:"parent_id"`                   //上级uid
	ParentName     string  `db:"parent_name" json:"parent_name" cbor:"parent_name"`             //上级代理
	FID            string  `db:"fid" json:"fid" cbor:"fid"`                                     //通道id
	Fname          string  `db:"fname" json:"fname" cbor:"fname"`                               //通道名称
	Amount         float64 `db:"amount" json:"amount" cbor:"amount"`                            //金额
	Fee            float64 `db:"fee" json:"fee" cbor:"fee"`                                     //手续费
	State          int     `db:"state" json:"state" cbor:"state"`                               //0:待确认:1存款成功2:已取消
	PixAccount     string  `db:"pix_account" json:"pix_account" cbor:"pix_account"`             //银行名
	RealName       string  `db:"real_name" json:"real_name" cbor:"real_name"`                   //持卡人姓名
	PixId          string  `db:"pix_id" json:"pix_id" cbor:"pix_id"`                            //银行卡号
	CreatedAt      int64   `db:"created_at" json:"created_at" cbor:"created_at"`                //
	ConfirmAt      int64   `db:"confirm_at" json:"confirm_at" cbor:"confirm_at"`                //确认时间
	ConfirmUID     string  `db:"confirm_uid" json:"confirm_uid" cbor:"confirm_uid"`             //确认人uid
	ConfirmName    string  `db:"confirm_name" json:"confirm_name" cbor:"confirm_name"`          //确认人名
	ReviewRemark   string  `db:"review_remark" json:"review_remark" cbor:"review_remark"`       //确认人名
	WithdrawAt     int64   `db:"withdraw_at" json:"withdraw_at" cbor:"withdraw_at"`             //三方场馆ID
	WithdrawRemark string  `db:"withdraw_remark" json:"withdraw_remark" cbor:"withdraw_remark"` //确认人名
	WithdrawUID    string  `db:"withdraw_uid" json:"withdraw_uid" cbor:"withdraw_uid"`          //确认人uid
	WithdrawName   string  `db:"withdraw_name" json:"withdraw_name" cbor:"withdraw_name"`       //确认人名
	HangUpUID      string  `db:"hang_up_uid" json:"hang_up_uid" cbor:"hang_up_uid"`             // 挂起人uid
	HangUpRemark   string  `db:"hang_up_remark" json:"hang_up_remark" cbor:"hang_up_remark"`    // 挂起备注
	HangUpName     string  `db:"hang_up_name" json:"hang_up_name" cbor:"hang_up_name"`          //  挂起人名字
	HangUpAt       int     `db:"hang_up_at" json:"hang_up_at" cbor:"hang_up_at"`                //  挂起时间
	ReceiveAt      int64   `db:"receive_at" json:"receive_at" cbor:"receive_at"`                //领取时间
	Balance        string  `db:"balance" json:"balance" cbor:"balance"`                         //提款前余额
	Flag           string  `db:"flag" json:"flag" cbor:"flag"`                                  //1 cpf 2 phone 3 email
	Level          int     `db:"level" json:"level" cbor:"level"`                               //会员等级
}

// FWithdrawData 取款数据
type FWithdrawData struct {
	T   int64         `json:"t"`
	D   []tblWithdraw `json:"d"`
	Agg tblWithdraw   `json:"agg"`
}

// 订单回调response
type paymentCallbackResp struct {
	AppId           string `json:"appId"`           //机构号
	CustId          string `json:"custId"`          //商户编号
	MerchantOrderId string `json:"merchantOrderId"` // 商户订单号
	Order           string `json:"order"`           // 平台订单号
	OrderStatus     string `json:"orderStatus"`     // 业务状态
	Amount          string `json:"amount"`          //金额
	Sign            string `json:"sign"`            // 签名
}

// WithdrawList 提款记录
func WithdrawList(ex g.Ex, ty uint8, startTime, endTime string, page, pageSize uint) (FWithdrawData, error) {

	if startTime != "" && endTime != "" {

		startAt, err := helper.TimeToLoc(startTime, loc)
		if err != nil {
			return FWithdrawData{}, errors.New(helper.DateTimeErr)
		}

		endAt, err := helper.TimeToLoc(endTime, loc)
		if err != nil {
			return FWithdrawData{}, errors.New(helper.DateTimeErr)
		}

		if ty == 1 {
			ex["created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
		} else {
			ex["withdraw_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
		}
	}
	// 待派单特殊操作 只显示一天的数据
	if ty == 3 {
		now := time.Now().Unix()
		ex["created_at"] = g.Op{"between": exp.NewRangeVal(now-172800, now)}
	}

	var data FWithdrawData
	if page == 1 {
		var total withdrawTotal
		query, _, _ := dialect.From("tbl_withdraw").Select(g.COUNT(1).As("t"), g.SUM("amount").As("agg")).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&total, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		data.T = total.T.Int64
		data.Agg = tblWithdraw{
			Amount: total.Agg.Float64,
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := dialect.From("tbl_withdraw").
		Select(colsWithdraw...).Where(ex).Order(g.C("created_at").Desc()).Offset(offset).Limit(pageSize).ToSQL()
	//fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// WithdrawFind 查找单条提款记录, 订单不存在返回错误: OrderNotExist
func WithdrawFind(id string) (tblWithdraw, error) {

	w := tblWithdraw{}
	query, _, _ := dialect.From("tbl_withdraw").Select(colsWithdraw...).Where(g.Ex{"id": id}).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&w, query)
	if err == sql.ErrNoRows {
		return w, errors.New(helper.OrderNotExist)
	}

	if err != nil {
		return w, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return w, nil
}

func WithdrawUpdateInfo(id string, record g.Record) error {

	query, _, _ := dialect.Update("tbl_withdraw").Where(g.Ex{"id": id}).Set(record).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func WithdrawRiskReview(id string, state int, record g.Record, withdraw tblWithdraw) error {

	//判断状态是否合法
	allow := map[int]bool{
		WithdrawReviewReject:  true,
		WithdrawDealing:       true,
		WithdrawSuccess:       true,
		WithdrawFailed:        true,
		WithdrawAutoPayFailed: true,
	}
	if _, ok := allow[state]; !ok {
		return errors.New(helper.StateParamErr)
	}

	//1、判断订单是否存在
	var order tblWithdraw
	ex := g.Ex{"id": id}
	query, _, _ := dialect.From("tbl_withdraw").Select(colsWithdraw...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&order, query)
	if err != nil || len(order.Username) < 1 {
		return errors.New(helper.IDErr)
	}

	query, _, _ = dialect.Update("tbl_withdraw").Set(record).Where(ex).ToSQL()
	switch order.State {
	case WithdrawReviewing:
		// 审核中(风控待领取)的订单只能流向分配, 上层业务处理
		return errors.New(helper.OrderStateErr)
	case WithdrawDealing:
		// 出款处理中可以是自动代付失败(WithdrawAutoPayFailed) 提款成功(WithdrawSuccess) 提款失败(WithdrawFailed)
		// 自动代付失败和提款成功是调用三方代付才会有的状态
		// 提款失败通过提款管理的[拒绝]操作进行流转(同时出款类型必须是手动出款)
		if state == WithdrawAutoPayFailed {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}

			return nil
		}

		if state != WithdrawSuccess && (state != WithdrawFailed) {
			return errors.New(helper.OrderStateErr)
		}

	case WithdrawAutoPayFailed:
		// 代付失败可以通过手动代付将状态流转至出款中
		if state == WithdrawDealing {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}

			return nil
		}

		// 代付失败的订单也可以通过手动出款直接将状态流转至出款成功
		// 代付失败的订单还可以通过拒绝直接将状态流转至出款失败
		if state != WithdrawFailed && state != WithdrawSuccess {
			return errors.New(helper.OrderStateErr)
		}

	case WithdrawHangup:
		// 挂起的订单只能领取(该状态流转上传业务已经处理), 该状态只能流转至审核中(WithdrawReviewing)
		return errors.New(helper.OrderStateErr)

	case WithdrawDispatched:
		// 派单状态可流转状态为 挂起(WithdrawHangup) 通过(WithdrawDealing) 拒绝(WithdrawReviewReject)
		// 其中流转至挂起状态由上层业务处理
		if state == WithdrawDealing {
			_, err = meta.MerchantDB.Exec(query)
			if err != nil {
				return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
			}

			return nil
		}

		if state != WithdrawReviewReject {
			return errors.New(helper.OrderStateErr)
		}

	default:
		// 审核拒绝, 提款成功, 出款失败三个状态为终态 不能进行其他处理
		return errors.New(helper.OrderStateErr)
	}

	// 3 如果是出款成功,修改订单状态为提款成功,扣除锁定钱包中的钱,发送通知
	if WithdrawSuccess == state {
		return withdrawOrderSuccess(query, order)
	}

	order.ReviewRemark = record["review_remark"].(string)
	order.WithdrawRemark = record["withdraw_remark"].(string)
	// 出款失败
	return withdrawOrderFailed(query, order)
}

// 检查锁定钱包余额是否充足
func lockBalanceIsEnough(uid string, amount decimal.Decimal) (decimal.Decimal, error) {

	balance, err := MemberBalanceFindOne(uid)
	if err != nil {
		return decimal.NewFromFloat(balance.LockAmount), err
	}
	brl := decimal.NewFromFloat(balance.LockAmount)
	if brl.Sub(amount).IsNegative() {
		return brl, errors.New(helper.LackOfBalance)
	}

	return brl, nil
}

// 提款成功
func withdrawOrderSuccess(query string, order tblWithdraw) error {

	money := decimal.NewFromFloat(order.Amount)

	// 判断锁定余额是否充足
	_, err := lockBalanceIsEnough(order.UID, money)
	if err != nil {
		return err
	}

	//开启事务
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	// 更新提款订单状态
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	// 锁定钱包下分
	ex := g.Ex{
		"uid": order.UID,
	}
	gr := g.Record{
		"last_withdraw_at": order.CreatedAt,
		"lock_amount":      g.L(fmt.Sprintf("lock_amount-%s", money.String())),
	}
	if meta.WalletMode == "2" {
		query, _, _ = dialect.Delete("tbl_promo_inspection").Where(g.Ex{"uid": order.UID, "ty": 3}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
		gr["deposit_lock_amount"] = "0"
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(gr).Where(ex).ToSQL()
	_, err = tx.Exec(query)
	fmt.Println(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	//ryrpc.MemberFlushByUid(order.UID)

	// TODO 修改会员提款限制
	//date := time.Unix(order.CreatedAt, 0).Format("20060102")
	//_ = withDrawDailyLimitUpdate(money, date, order.Username)

	return nil
}

// 提款审核中
func withdrawOrderReviewing(query string, order tblWithdraw) error {

	//开启事务
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	// 更新提款订单状态
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	return nil
}
func withdrawOrderFailed(query string, order tblWithdraw) error {
	//开启事务
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	//5、更新订单状态
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	ret, err := UpdateBalance(order.UID, order.Amount+order.Fee, helper.TransactionWithDrawFail, order.ID)
	if ret == false {
		tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	fmt.Println("修改提现状态完成")

	return nil
}

// 弃用
func withdrawOrderFailed2(query string, order tblWithdraw) error {

	money := decimal.NewFromFloat(order.Amount)
	moneyFee := decimal.NewFromFloat(order.Fee)

	//4、查询用户额度
	balance, err := MemberBalanceFindOne(order.UID)
	if err != nil {
		return err
	}
	blr := decimal.NewFromFloat(balance.Brl)
	balanceAfter := blr.Add(money)
	fmt.Println("修改提现状态开始", order)

	//开启事务
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	//5、更新订单状态
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	//6、更新余额

	ex := g.Ex{
		"uid": order.UID,
	}
	var balanceRecord g.Record
	if meta.WalletMode == "1" {
		balanceRecord = g.Record{
			"brl":         g.L(fmt.Sprintf("brl+%s", money.Add(moneyFee).String())),
			"lock_amount": g.L(fmt.Sprintf("lock_amount-%s", money.Add(moneyFee).String())),
		}
	} else if meta.WalletMode == "2" {
		balanceRecord = g.Record{
			"brl":           g.L(fmt.Sprintf("brl+%s", money.Add(moneyFee).String())),
			"lock_amount":   g.L(fmt.Sprintf("lock_amount-%s", money.Add(moneyFee).String())),
			"unlock_amount": g.L(fmt.Sprintf("unlock_amount+%s", money.Add(moneyFee).String())),
		}
	}
	query, _, _ = dialect.Update("tbl_member_balance").Set(balanceRecord).Where(ex).ToSQL()
	fmt.Println("更新余额SQL:", query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	//7、新增账变记录
	mbTrans := MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       money.String(),
		BeforeAmount: fmt.Sprintf(`%f`, balance.Brl),
		BillNo:       order.ID,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           helper.GenId(),
		CashType:     helper.TransactionWithDrawFail,
		UID:          order.UID,
		Username:     order.Username,
	}

	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTrans).ToSQL()
	fmt.Println("新增账变SQL:", query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	//8、新增账变记录 手续费退回
	mbTransFee := MemberTransaction{
		AfterAmount:  balanceAfter.Add(moneyFee).String(),
		Amount:       moneyFee.String(),
		BeforeAmount: fmt.Sprintf(`%f`, balance.Brl),
		BillNo:       order.ID,
		CreatedAt:    time.Now().UnixMilli(),
		ID:           helper.GenId(),
		CashType:     helper.TransactionWithdrawalFeeRem,
		UID:          order.UID,
		Username:     order.Username,
	}

	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(mbTransFee).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	//ryrpc.MemberFlushByUid(order.UID)
	fmt.Println("修改提现状态完成")

	return nil
}

// WithdrawLock 锁定提款订单
// 订单因为外部因素(接口)导致的状态流转应该加锁
func WithdrawLock(id string) error {

	key := fmt.Sprintf(withdrawOrderLockKey, id)
	err := Lock(key)
	if err != nil {
		return err
	}

	return nil
}

// WithdrawUnLock 解锁提款订单
func WithdrawUnLock(id string) {
	Unlock(fmt.Sprintf(withdrawOrderLockKey, id))
}

func Lock(id string) error {

	val := fmt.Sprintf("%s%s", defaultRedisKeyPrefix, id)
	ok, err := meta.MerchantRedis.SetNX(ctx, val, "1", LockTimeout).Result()
	if err != nil {
		return pushLog(err, helper.RedisErr)
	}
	if !ok {
		return errors.New(helper.RequestBusy)
	}

	return nil
}

func Unlock(id string) {

	val := fmt.Sprintf("%s%s", defaultRedisKeyPrefix, id)
	res, err := meta.MerchantRedis.Unlink(ctx, val).Result()
	if err != nil || res != 1 {
		fmt.Println("Unlock res = ", res)
		fmt.Println("Unlock err = ", err)
	}
}

// 提款历史记录
func WithdrawHistoryList(ex g.Ex, ty, startTime, endTime, isAsc, sortField string, page, pageSize uint) (FWithdrawData, error) {

	ex["tester"] = []int{1, 3}
	orderField := g.L("created_at")
	if sortField != "" {
		orderField = g.L(sortField)
	}

	orderBy := orderField.Desc()
	if isAsc == "1" {
		orderBy = orderField.Asc()
	}
	if startTime != "" && endTime != "" {

		startAt, err := helper.TimeToLoc(startTime, loc)
		if err != nil {
			return FWithdrawData{}, errors.New(helper.DateTimeErr)
		}

		endAt, err := helper.TimeToLoc(endTime, loc)
		if err != nil {
			return FWithdrawData{}, errors.New(helper.DateTimeErr)
		}

		if startAt >= endAt {
			return FWithdrawData{}, errors.New(helper.QueryTimeRangeErr)
		}

		if ty == "1" {
			ex["created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
		} else {
			ex["withdraw_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
		}
	}

	data := FWithdrawData{}
	if page == 1 {
		query, _, _ := dialect.From("tbl_withdraw").Select(g.COUNT("id")).Where(ex).ToSQL()
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}

		query, _, _ = dialect.From("tbl_withdraw").Select(g.SUM("amount").As("amount")).Where(ex).ToSQL()
		err = meta.MerchantDB.Get(&data.Agg, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
	}
	offset := (page - 1) * pageSize
	query, _, _ := dialect.From("tbl_withdraw").Select(colsWithdraw...).Where(ex).
		Offset(offset).Limit(pageSize).Order(orderBy, g.C("id").Desc()).ToSQL()
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// 财务拒绝提款订单
func WithdrawReject(id string, state int, d g.Record) error {
	return withdrawDownPoint(id, "", state, d)
}

// 取款下分
func withdrawDownPoint(did, bankcard string, state int, record g.Record) error {

	//判断状态是否合法
	allow := map[int]bool{
		WithdrawReviewReject:  true,
		WithdrawDealing:       true,
		WithdrawSuccess:       true,
		WithdrawFailed:        true,
		WithdrawAutoPayFailed: true,
		WithdrawAutoPaying:    true,
	}
	if _, ok := allow[state]; !ok {
		return errors.New(helper.StateParamErr)
	}

	//1、判断订单是否存在
	var order tblWithdraw
	ex := g.Ex{"id": did}
	query, _, _ := dialect.From("tbl_withdraw").Select(colsWithdraw...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&order, query)
	fmt.Println("判断订单是否存在", state, query, order)
	if err != nil || len(order.Username) < 1 {
		return errors.New(helper.IDErr)
	}

	query, _, _ = dialect.Update("tbl_withdraw").Set(record).Where(ex).ToSQL()
	//switch order.State {
	//case WithdrawReviewing:
	//	// 审核中(风控待领取)的订单只能流向分配, 上层业务处理
	//	return errors.New(helper.OrderStateErr)
	//case WithdrawDealing:
	//	// 出款处理中可以是自动代付失败(WithdrawAutoPayFailed) 提款成功(WithdrawSuccess) 提款失败(WithdrawFailed)
	//	// 自动代付失败和提款成功是调用三方代付才会有的状态
	//	// 提款失败通过提款管理的[拒绝]操作进行流转(同时出款类型必须是手动出款)
	//	if state == WithdrawAutoPayFailed {
	//		_, err = meta.MerchantDB.Exec(query)
	//		if err != nil {
	//			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	//		}
	//
	//		return nil
	//	}
	//
	//	if state != WithdrawSuccess && (state != WithdrawFailed) {
	//		return errors.New(helper.OrderStateErr)
	//	}
	//
	//case WithdrawAutoPayFailed:
	//	// 代付失败可以通过手动代付将状态流转至出款中
	//	if state == WithdrawDealing {
	//		_, err = meta.MerchantDB.Exec(query)
	//		if err != nil {
	//			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	//		}
	//
	//		return nil
	//	}
	//
	//	// 代付失败的订单也可以通过手动出款直接将状态流转至出款成功
	//	// 代付失败的订单还可以通过拒绝直接将状态流转至出款失败
	//	if state != WithdrawFailed && state != WithdrawSuccess {
	//		return errors.New(helper.OrderStateErr)
	//	}
	//
	//case WithdrawHangup:
	//	// 挂起的订单只能领取(该状态流转上传业务已经处理), 该状态只能流转至审核中(WithdrawReviewing)
	//	return errors.New(helper.OrderStateErr)
	//
	//case WithdrawDispatched:
	//	// 派单状态可流转状态为 挂起(WithdrawHangup) 通过(WithdrawDealing) 拒绝(WithdrawReviewReject)
	//	// 其中流转至挂起状态由上层业务处理
	//	if state == WithdrawDealing {
	//		_, err = meta.MerchantDB.Exec(query)
	//		if err != nil {
	//			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	//		}
	//
	//		return nil
	//	}
	//
	//	if state != WithdrawReviewReject {
	//		return errors.New(helper.OrderStateErr)
	//	}
	//case WithdrawAutoPaying:
	//	// 三方出款失败，修改出款失败状态
	//	fmt.Println("三方出款失败，修改出款失败状态")
	//default:
	//	// 审核拒绝, 提款成功, 出款失败三个状态为终态 不能进行其他处理
	//	return errors.New(helper.OrderStateErr)
	//}

	// 3 如果是出款成功,修改订单状态为提款成功,扣除锁定钱包中的钱,发送通知
	if WithdrawSuccess == state {
		return withdrawOrderSuccess(query, order)
	} else if WithdrawAutoPaying == state {
		return withdrawOrderReviewing(query, order)
	}

	//order.ReviewRemark = record["review_remark"].(string)
	//order.WithdrawRemark = record["withdraw_remark"].(string)
	// 出款失败
	fmt.Println("设置出款失败", query, order)
	return withdrawOrderFailed(query, order)
}

// WithdrawHandToAuto 手动代付
func WithdrawHandToAuto(fctx *fasthttp.RequestCtx, withdraw tblWithdraw, t time.Time) error {

	p, err := CachePayFactory(withdraw.FID)
	if err != nil {
		return err
	}

	if len(p.Fid) == 0 || p.State == "0" {
		return errors.New(helper.CateNotExist)
	}

	as := fmt.Sprintf(`%f`, withdraw.Amount)
	// check amount range, continue the for loop if amount out of range
	_, ok := validator.CheckFloatScope(as, p.Fmin, p.Fmax)
	if !ok {
		return errors.New(helper.AmountOutRange)
	}

	data, err := withdrawEpayOrder(withdraw.Amount, withdraw.PixAccount, withdraw.UID, withdraw.PixId, "CPF", withdraw.ID, p)
	fmt.Println("Pay  payment.Pay err = ", err)
	if err != nil {
		return err
	}
	if data.Code != "000000" {
		return fmt.Errorf(helper.PayServerErr)
	}
	if data.OrdStatus == "08" {
		record := g.Record{
			"review_remark":   data.Msg,
			"withdraw_remark": data.Msg,
			"state":           WithdrawFailed,
		}
		err = WithdrawReject(withdraw.ID, WithdrawFailed, record)
		if err != nil {
			fmt.Println("withdrawHandToAuto failed 2:", withdraw.ID, err)
		}
		//err = withdrawAutoUpdate(withdraw.ID, WithdrawFailed)
		return fmt.Errorf("status=%s,err=%s", data.OrdStatus, data.Msg)
	} else {
		err = withdrawAutoUpdate(withdraw.ID, WithdrawAutoPaying)
		if err != nil {
			fmt.Println("withdrawHandToAuto failed 2:", withdraw.ID, err)
		}
		return nil
	}
}

func withdrawAutoUpdate(id string, state int) error {

	r := g.Record{"state": state}

	query, _, _ := dialect.Update("tbl_withdraw").Set(r).Where(g.Ex{"id": id}).ToSQL()
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return err
}

// WithdrawAutoPaySetFailed 将订单状态从出款中修改为代付失败
func WithdrawAutoPaySetFailed(id string, confirmAt int64, confirmUid, confirmName string) error {

	order, err := WithdrawFind(id)
	if err != nil {
		return err
	}

	// 只能将出款中(三方处理中, 即自动代付)的订单状态流转为代付失败
	if order.State != WithdrawDealing && order.State != WithdrawAutoPaying {
		return errors.New(helper.OrderStateErr)
	}

	// 将automatic设为1是为了确保状态为代付失败的订单一定为自动出款(automatic=1)
	record := g.Record{
		"state":     WithdrawAutoPayFailed,
		"automatic": "1",
	}

	return withdrawDownPoint(id, "", WithdrawAutoPayFailed, record)
}

// WithdrawalCallBack 提款回调
func WithdrawalCallBack(fctx *fasthttp.RequestCtx) {
	var (
		err  error
		data paymentCallbackResp
	)
	payment := &thirdEpay{}
	// 获取并校验回调参数
	data, err = payment.WithdrawCallBack(fctx)
	if err != nil {
		fctx.SetBody([]byte(`failed`))
		pushLog(err, helper.WithdrawFailure)
		return
	}
	fmt.Println("获取并校验回调参数:", data)

	// 查询订单
	order, err := WithdrawFind(data.MerchantOrderId)
	if err != nil {
		err = fmt.Errorf("query order error: [%v]", err)
		fctx.SetBody([]byte(`failed`))
		pushLog(err, helper.WithdrawFailure)
		return
	}

	// 提款成功只考虑出款中和代付失败的情况
	// 审核中的状态不用考虑，因为不会走到三方去，出款成功和出款失败是终态也不用考虑
	if order.State != WithdrawReviewing && order.State != WithdrawAutoPaying {
		err = fmt.Errorf("duplicated Withdrawal notify: [%v]", err)
		fctx.SetBody([]byte(`failed`))
		pushLog(err, helper.WithdrawFailure)
		return
	}

	now := fctx.Time()
	// 修改订单状态
	var state int
	if data.OrderStatus == "06" {
		state = WithdrawAutoPaying
	} else if data.OrderStatus == "07" {
		state = WithdrawSuccess
	} else {
		state = WithdrawFailed
	}
	err = withdrawUpdate(data.MerchantOrderId, state, now)
	if err != nil {
		err = fmt.Errorf("set order state [%d] to [%d] error: [%v]", order.State, state, err)
		pushLog(err, helper.WithdrawFailure)
		fctx.SetBody([]byte(`failed`))
		return
	}
	fctx.SetBody([]byte(`success`))
}

// 接收到三方回调后调用这个方法（三方调用缺少confirm uid和confirm name这些信息）
func withdrawUpdate(id string, state int, t time.Time) error {

	// 加锁
	err := withdrawLock(id)
	if err != nil {
		return err
	}
	defer withdrawUnLock(id)

	record := g.Record{
		"state": state,
	}

	switch state {
	case WithdrawSuccess:
		record["withdraw_at"] = fmt.Sprintf("%d", t.Unix())
	case WithdrawAutoPaying:
		record["confirm_at"] = fmt.Sprintf("%d", t.Unix())
	case WithdrawFailed:
		record["confirm_at"] = fmt.Sprintf("%d", t.Unix())
	default:
		return errors.New(helper.StateParamErr)
	}

	return withdrawDownPoint(id, "", state, record)
}

// WithdrawLock 锁定提款订单
// 订单因为外部因素(接口)导致的状态流转应该加锁
func withdrawLock(id string) error {

	key := fmt.Sprintf(withdrawOrderLockKey, id)
	err := Lock(key)
	if err != nil {
		return err
	}

	return nil
}

// WithdrawUnLock 解锁提款订单
func withdrawUnLock(id string) {
	Unlock(fmt.Sprintf(withdrawOrderLockKey, id))
}
