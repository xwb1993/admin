package model

import (
	"admin/contrib/helper"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
)

type TblMemberAdjust struct {
	Id           string  `json:"id" cbor:"id" db:"id"`
	Uid          string  `json:"uid" cbor:"uid" db:"uid"`
	Username     string  `json:"username" cbor:"username" db:"username"`
	Amount       float64 `json:"amount" cbor:"amount" db:"amount"`
	AdjustMode   int     `json:"adjust_mode" cbor:"adjust_mode" db:"adjust_mode"`
	ApplyRemark  string  `json:"apply_remark" cbor:"apply_remark" db:"apply_remark"`
	ReviewRemark string  `json:"review_remark" cbor:"review_remark" db:"review_remark"`
	State        int     `json:"state" cbor:"state" db:"state"`
	ApplyAt      int64   `json:"apply_at" cbor:"apply_at" db:"apply_at"`
	ApplyUid     string  `json:"apply_uid" cbor:"apply_uid" db:"apply_uid"`
	ApplyName    string  `json:"apply_name" cbor:"apply_name" db:"apply_name"`
	ReviewAt     int64   `json:"review_at" cbor:"review_at" db:"review_at"`
	ReviewUid    string  `json:"review_uid" cbor:"review_uid" db:"review_uid"`
	ReviewName   string  `json:"review_name" cbor:"review_name" db:"review_name"`
	Tester       int     `json:"tester" cbor:"tester" db:"tester"`
}

type AdjustData struct {
	T int               `cbor:"t" json:"t"`
	D []TblMemberAdjust `cbor:"d" json:"d"`
}

func AdjustInsert(data TblMemberAdjust) error {

	tx, err := meta.MerchantDB.Begin()
	if data.AdjustMode == 1 {
		query, _, _ := dialect.Insert("tbl_member_adjust").Rows(&data).ToSQL()
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			tx.Rollback()
			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
	} else if data.AdjustMode == 2 { //下分扣钱
		ret, err := UpdateBalance(data.Uid, -data.Amount, helper.TransactionDownPoint, data.Id)
		if ret == false {
			tx.Rollback()
			return err
		}
		query, _, _ := dialect.Insert("tbl_member_adjust").Rows(&data).ToSQL()
		_, err = meta.MerchantDB.Exec(query)
		if err != nil {
			tx.Rollback()
			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
	}
	tx.Commit()
	return nil
}

// AdjustList 账户调整列表
func AdjustList(startTime, endTime string, ex g.Ex, page, pageSize int) (AdjustData, error) {

	data := AdjustData{}

	startAt, err := helper.TimeToLoc(startTime, loc)
	if err != nil {
		return data, errors.New(helper.TimeTypeErr)
	}

	endAt, err := helper.TimeToLoc(endTime, loc)
	if err != nil {
		return data, errors.New(helper.TimeTypeErr)
	}

	if startAt >= endAt {
		return data, errors.New(helper.QueryTimeRangeErr)
	}

	ex["apply_at"] = g.Op{
		"between": exp.NewRangeVal(startAt, endAt),
	}

	t := dialect.From("tbl_member_adjust")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT("id")).Where(ex).ToSQL()
		fmt.Println(query)
		err = meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsMemberAdjust...).Where(ex).Order(g.C("review_at").Desc(), g.C("apply_at").Desc()).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

// AdjustReview 账户调整-审核
func AdjustReview(state string, record g.Record) error {

	data := TblMemberAdjust{}

	query, _, _ := dialect.From("tbl_member_adjust").
		Select(colsMemberAdjust...).Where(g.Ex{"id": record["id"]}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&data, query)
	if err != nil {
		return errors.New(helper.DBErr)
	}

	// 只有审核中状态的才能操作
	if fmt.Sprintf(`%d`, data.State) != AdjustReviewing {
		return errors.New(helper.StateParamErr)
	}
	record["apply_remark"] = data.ApplyRemark

	fmt.Println("state:", state)
	fmt.Println("data.State:", data.State)
	tx, err := meta.MerchantDB.Begin()
	r := g.Record{
		"state":         state, //审核状态:1=审核中,2=审核通过,3=审核未通过
		"review_remark": record["review_remark"],
		"review_at":     record["review_at"],
		"review_uid":    record["review_uid"],
		"review_name":   record["review_name"],
	}
	// 更新调整记录状态
	query, _, _ = dialect.Update("tbl_member_adjust").Set(r).Where(g.Ex{"id": record["id"]}).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}
	if data.AdjustMode == 1 { //上分
		if state == AdjustReviewPass { //加钱
			ret, err := UpdateBalance(data.Uid, data.Amount, helper.TransactionUpPoint, data.Id)
			if ret == false {
				tx.Rollback()
				return pushLog(err, helper.DBErr)
			}
		}

	}

	if data.AdjustMode == 2 { //下分
		if state == AdjustReviewReject { //加钱
			ret, err := UpdateBalance(data.Uid, data.Amount, helper.TransactionDownPointBack, data.Id)
			if ret == false {
				tx.Rollback()
				return pushLog(err, helper.DBErr)
			}
		}
	}
	tx.Commit()
	return nil
}
