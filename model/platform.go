package model

import (
	"admin/contrib/helper"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	g "github.com/doug-martin/goqu/v9"
)

func PlatformUpdate(adminID, adminName, id string, state, maintained, seq int) error {

	data := Platform{}
	query, _, _ := dialect.From("tbl_platforms").Select(colsPlatform...).Where(g.Ex{"id": id}).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&data, query)
	if err != nil && err != sql.ErrNoRows {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return errors.New(helper.RecordNotExistErr)
	}

	if 0 == state && 0 == maintained && 0 == seq {
		return errors.New(helper.NoDataUpdate)
	}

	if data.State == state && data.Maintained == maintained && data.Seq == seq {
		return errors.New(helper.NoDataUpdate)
	}

	record := g.Record{
		"updated_uid":  adminID,
		"updated_name": adminName,
	}
	if state != 0 {
		record["state"] = state
	}
	if maintained != 0 {
		record["maintained"] = maintained
	}
	if seq != 0 {
		record["seq"] = seq
	}
	//tx, err := meta.MerchantDB.Begin() // 开启事务
	//if err != nil {
	//	return pushLog(err, helper.DBErr)
	//}
	//
	//query, _, _ = dialect.Update("tbl_platforms").Set(record).Where(g.Ex{"id": id}).ToSQL()
	//_, err = tx.Exec(query)
	//if err != nil {
	//	_ = tx.Rollback()
	//	return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	//}
	//
	//query, _, _ = dialect.Update("tbl_game_lists").Set(g.Record{"online": state}).Where(g.Ex{"platform_id": id}).ToSQL()
	//_, err = tx.Exec(query)
	//if err != nil {
	//	_ = tx.Rollback()
	//	return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	//}
	//
	//_ = tx.Commit()

	query, _, _ = dialect.Update("tbl_platforms").Set(record).Where(g.Ex{"id": id}).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	GameFlushAll()
	_ = LoadPlatforms()

	return nil
}

func PlatformList(ex g.Ex, page, pageSize int) (PlatformData, error) {

	data := PlatformData{
		S: pageSize,
	}
	t := dialect.From("tbl_platforms")
	if page == 1 {
		query, _, _ := t.Select(g.COUNT(1)).Where(ex).ToSQL()
		fmt.Println(query)
		err := meta.MerchantDB.Get(&data.T, query)
		if err != nil {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}

	}

	offset := (page - 1) * pageSize
	query, _, _ := t.Select(colsPlatform...).Where(ex).
		Order(g.C("created_at").Desc()).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}

type Nav struct {
	Status bool               `json:"status"`
	Data   map[int][]platJson `json:"data"`
}

func LoadPlatforms() error {

	var (
		data []platJson
		nav  = Nav{
			Status: true,
		}
	)
	nav.Data = make(map[int][]platJson)
	ex := g.Ex{
		"state": 1,
	}
	query, _, _ := dialect.From("tbl_platforms").
		Select(colsPlatJson...).Where(ex).Order(g.C("game_type").Asc(), g.C("seq").Asc()).ToSQL()
	query = "/* master */ " + query
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data, query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if len(data) == 0 {
		return errors.New(helper.RecordNotExistErr)
	}

	pipe := meta.MerchantRedis.Pipeline()
	defer pipe.Close()

	for _, v := range data {
		if _, ok := nav.Data[v.GameType]; !ok {
			nav.Data[v.GameType] = []platJson{v}
		} else {
			nav.Data[v.GameType] = append(nav.Data[v.GameType], v)
		}

		k := fmt.Sprintf("plat:%s", v.ID)
		b, _ := json.Marshal(v)
		pipe.Unlink(ctx, k)
		pipe.Set(ctx, k, string(b), 0)
		pipe.Persist(ctx, k)
	}

	navBytes, _ := json.Marshal(nav)
	navKey := "nav"
	pipe.Unlink(ctx, navKey)
	pipe.Set(ctx, navKey, string(navBytes), 0)
	pipe.Persist(ctx, navKey)
	_, err = pipe.Exec(ctx)
	if err != nil {
		return pushLog(err, helper.RedisErr)
	}

	return nil
}
