package model

import (
	"admin/contrib/helper"
	ryrpc "admin/rpc"
	myConfig "common/config"
	"crypto/md5"
	"database/sql"
	"errors"
	"fmt"
	"github.com/shopspring/decimal"
	"github.com/valyala/fasthttp"
	"math/rand"
	"strconv"
	"strings"
	"time"

	g "github.com/doug-martin/goqu/v9"
	"github.com/doug-martin/goqu/v9/exp"
	"github.com/speps/go-hashids"
)

// 查询用户单条数据
func MemberFindByUsername(username string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{}
	if username != "" {
		ex["username"] = username
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

// 查询渠道用户单条数据
func MemberFindByUnameAndOpeId(username, operatorId string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{
		"username":    username,
		"operator_id": operatorId,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

// 查询业务员用户单条数据
func MemberFindByUnameAndProId(username, businessId string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{
		"username":    username,
		"business_id": businessId,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

func MemberFindByOperatorId(operatorId string) ([]ryrpc.TblMemberBase, error) {

	var m []ryrpc.TblMemberBase

	ex := g.Ex{}
	if operatorId != "" {
		ex["operator_id"] = operatorId
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).ToSQL()
	err := meta.MerchantDB.Select(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.OperatorIdErr)
	}

	return m, nil
}

func MemberFindByBusinessId(businessId string) ([]ryrpc.TblMemberBase, error) {

	var m []ryrpc.TblMemberBase

	ex := g.Ex{}
	if businessId != "" {
		ex["business_id"] = businessId
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).ToSQL()
	err := meta.MerchantDB.Select(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.BusinessIdErr)
	}

	return m, nil
}

func MemberFindByUid(uid string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{}
	if uid != "" {
		ex["uid"] = uid
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select("*").Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	return m, nil
}

// 查询用户单条数据
func MemberFindByUidAndOpeId(uid, operatorId string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{
		"tm.uid":         uid,
		"tm.operator_id": operatorId,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select("tm.*", "mb.brl", "mpa.deposit", "mpa.deposit_num", "mpa.first_deposit", "mpa.withdraw", "mpa.withdraw_num", "mpa.running", "mpa.game_round", "mpa.game_tax", "mpa.game_winlost", "mpa.proxy_invite_bonus").
		LeftJoin(g.T("tbl_member_balance").As("mb"), g.On(g.Ex{"tm.uid": g.I("mb.uid")})).
		LeftJoin(g.T("tbl_member_proxy_accu").As("mpa"), g.On(g.Ex{"tm.uid": g.I("mpa.uid"), "mpa.level": 0})).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	if err == sql.ErrNoRows {
		return m, errors.New(helper.OperatorIdErr)
	}

	return m, nil
}

// 查询用户单条数据
func MemberFindByUidAndProId(uid, businessId string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}

	ex := g.Ex{
		"uid":         uid,
		"business_id": businessId,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.OperatorIdErr)
	}

	return m, nil
}

func MemberBalanceFindOne(uid string) (TblMemberBalance, error) {

	balance := TblMemberBalance{}
	query, _, _ := dialect.From("tbl_member_balance").Select(colsMemberBalance...).Where(g.Ex{"uid": uid}).Limit(1).ToSQL()
	fmt.Println(query)
	err := meta.MerchantDB.Get(&balance, query)
	if err != nil {
		return balance, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return balance, err
}

func MemBalanceFindOne(uid string) (MemBalance, error) {
	colsMemBalance := helper.EnumFields(MemBalance{})
	balance := MemBalance{}
	query, _, _ := dialect.From("tbl_member_balance").Select(colsMemBalance...).Where(g.Ex{"uid": uid}).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&balance, query)
	if err != nil {
		return balance, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return balance, err
}

type MemberData struct {
	ryrpc.TblMemberBase
	MemBalance
}

type MemberPageData struct {
	T  int64                      `json:"t"`
	D  []ryrpc.TblMemberBaseTotal `json:"d"`
	S  uint                       `json:"s"`
	Ip map[string]string          `json:"ip"`
}

func MemberList(page, pageSize int, startTime, endTime, agent, adminId string, ex g.Ex) (MemberPageData, error) {

	data := MemberPageData{}
	var err error
	if startTime != "" && endTime != "" {

		startAt, err := helper.TimeToLoc(startTime, loc)
		if err != nil {
			return data, errors.New(helper.TimeTypeErr)
		}

		endAt, err := helper.TimeToLoc(endTime, loc)
		if err != nil {
			return data, errors.New(helper.TimeTypeErr)
		}
		if startAt >= endAt {
			return data, errors.New(helper.QueryTimeRangeErr)
		}

		ex["tm.created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
	}

	//验证是否是渠道登录，是的话查询该渠道的会员列表
	loginUser := GetLoginUser(adminId)

	changephone := 1
	if loginUser.Operator != "" {
		ex["tm.operator_id"] = adminId
	} else if loginUser.Businsess != "" {
		ex["tm.business_id"] = adminId
	} else {
		//总后台登陆
		adminInfo := Admin{}
		query, _, _ := dialect.From(g.T("tbl_admins")).Select(colsAdmin...).Where(g.Ex{"id": adminId}).ToSQL()
		err = meta.MerchantDB.Get(&adminInfo, query)
		if err != nil && err != sql.ErrNoRows {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
		if adminInfo.GroupID == "2" {
			//超级管理员
			changephone = 0
		}

	}
	t := dialect.From(g.T("tbl_member_base").As("tm"))
	if page == 1 {
		query, _, _ := t.Select(g.COUNT("uid")).Where(ex).ToSQL()
		err = meta.MerchantDB.Get(&data.T, query)
		if err != nil && err != sql.ErrNoRows {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}
	offset := (page - 1) * pageSize
	d := []ryrpc.TblMemberBaseTotalNull{}
	query, _, _ := t.Select("tm.*", "mb.brl", "mpa.deposit", "mpa.deposit_times", "mpa.withdraw", "mpa.withdraw_times", "mpa.running", "mpa.game_round", "mpa.game_tax", "mpa.game_winlost", "mpa.turntable_bonus").
		LeftJoin(g.T("tbl_member_balance").As("mb"), g.On(g.Ex{"tm.uid": g.I("mb.uid")})).
		LeftJoin(g.T("tbl_member_accu").As("mpa"), g.On(g.Ex{"tm.uid": g.I("mpa.uid")})).
		Where(ex).Order(g.I("tm.created_at").Desc()).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Select(&d, query)
	if err != nil {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if len(d) == 0 {
		return data, nil
	}
	for _, value := range d {
		if changephone == 1 {
			phone := value.Phone
			if len(phone) > 5 {
				value.Phone = phone[:3] + "****" + phone[len(phone)-5:]
			}
		}
		singleData := ryrpc.TblMemberBaseTotal{
			Uid:               value.Uid,
			Pid:               value.Pid,
			Cid:               value.Cid,
			Username:          value.Username,
			TopID:             value.TopID,
			TopName:           value.TopName,
			GreatGrandID:      value.GreatGrandID,
			GreatGrandName:    value.GreatGrandName,
			GrandID:           value.GrandID,
			GrandName:         value.GrandName,
			ParentID:          value.ParentID,
			ParentName:        value.ParentName,
			Password:          "",
			CreatedAt:         value.CreatedAt,
			CreatedIp:         value.CreatedIp,
			Score:             value.Score,
			KeepScore:         value.KeepScore,
			Vip:               value.Vip,
			Phone:             value.Phone,
			Email:             value.Email,
			InviteRegisterNum: value.InviteRegisterNum,
			InviteNum:         value.InviteNum,
			LastTreasure:      value.LastTreasure,
			Tester:            value.Tester,
			Avatar:            value.Avatar,
			InviteCode:        value.InviteCode,
			PayPassword:       "",
			Telegram:          value.Telegram,
			PhoneVerify:       value.PhoneVerify,
			EmailVerify:       value.EmailVerify,
			BankcardTotal:     value.BankcardTotal,
			CanBonus:          value.CanBonus,
			CanRebate:         value.CanRebate,
			State:             value.State,
			Remake:            value.Remake,
			Prefix:            value.Prefix,
			Invited:           value.Invited,
			OperatorId:        value.OperatorId,
			BusinessId:        value.BusinessId,
			WinFlowMultiple:   value.WinFlowMultiple,
			LoseFlowMultiple:  value.LoseFlowMultiple,
			Fb:                value.Fb,
			Google:            value.Google,
			CancelAgent:       value.CancelAgent,
			IsBloger:          value.IsBloger,
			DepositAmount:     value.DepositAmount,
			Deposit:           value.Deposit.Float64,
			DepositTimes:      value.DepositTimes.Int32,
			Withdraw:          value.Withdraw.Float64,
			WithdrawTimes:     value.WithdrawTimes.Int32,
			Running:           value.Running.Float64,
			GameRound:         value.GameRound.Int32,
			GameWinlost:       value.GameWinlost.Float64,
			GameTax:           value.GameTax.Float64,
			TurntableBonus:    value.TurntableBonus.Float64,
			Brl:               value.Brl.Float64,
		}
		data.D = append(data.D, singleData)
	}
	data.S = uint(pageSize)

	return data, nil
}

func MemberFlushMulti(uids []string) error {

	var data []ryrpc.TblMemberBase

	ex := g.Ex{
		"uid": uids,
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(ex).ToSQL()
	err := meta.MerchantDB.Select(&data, query)
	if err != nil {
		fmt.Println("MemberFlush err = ", err.Error())
		return err
	}

	b, err := helper.JsonMarshal(data)
	if err != nil {
		fmt.Println("MemberFlush JsonMarshal err = ", err.Error())
		return err
	}

	index := meta.Meili.Index("members")
	index.DeleteDocuments(uids)
	_, err = index.AddDocuments(b, "uid")
	if err != nil {
		fmt.Println("MemberFlush AddDocuments err = ", err.Error())
		return err
	}
	return nil
}

func MemberFlushAll() {

	var data []ryrpc.TblMemberBase

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).ToSQL()
	err := meta.MerchantDB.Select(&data, query)
	if err != nil {
		fmt.Println("MemberFlushAll err = ", err.Error())
		return
	}

	fmt.Println("MemberFlushAll len(data) = ", len(data))
	filterable := []string{
		"username",
		"phone",
		"email",
	}
	sortable := []string{
		"created_at",
	}
	searchable := []string{
		"username",
	}

	b, err := helper.JsonMarshal(data)
	if err != nil {
		fmt.Println("MemberFlushAll JsonMarshal err = ", err.Error())
		return
	}

	meta.Meili.DeleteIndex("members")
	index := meta.Meili.Index("members")
	index.UpdateFilterableAttributes(&filterable)
	index.UpdateSortableAttributes(&sortable)
	index.UpdateSearchableAttributes(&searchable)

	_, err = index.AddDocuments(b, "uid")
	if err != nil {
		fmt.Println("MemberFlushAll AddDocuments err = ", err.Error())
		return
	}
	fmt.Println("MemberFlushAll ok")
}

func MemberInsert(username, password, parentId, operatorId, businessId string, tester int, prefix string) error {

	if MemberExist(username) {
		return errors.New(helper.UsernameExist)
	}
	var phone, email string

	if strings.Contains(username, "@") {
		email = username
	}
	if helper.CtypeDigit(username) {
		phone = username
	}

	sid, err := meta.MerchantRedis.Incr(ctx, "id").Result()
	if err != nil {
		return pushLog(err, helper.RedisErr)
	}

	uid := fmt.Sprintf("%d", sid)
	myCode, err := shortURLGen(uid)
	if err != nil {
		return errors.New(helper.ServerErr)
	}

	ex := g.Ex{}
	if phone != "" {
		ex["phone"] = phone
		if MemberBindCheck(ex) {
			return errors.New(helper.PhoneExist)
		}
	}
	if email != "" {
		ex["email"] = email
		if MemberBindCheck(ex) {
			return errors.New(helper.EmailExist)
		}
	}

	parent := ryrpc.TblMemberBase{
		ParentID: "0",
		GrandID:  "0",
		Uid:      "0",
		TopID:    "0",
	}
	if parentId != "" {
		parent, err = MemberFindByUid(parentId)
		if err != nil {
			return errors.New(helper.UIDErr)
		}
	}
	createdAt := uint32(time.Now().Unix())
	paypwd := fmt.Sprintf("%d", pwdMurmurHash("123456", createdAt))

	pwd := fmt.Sprintf("%d", pwdMurmurHash(password, createdAt))
	m := ryrpc.TblMemberBase{
		Prefix:           prefix,
		Uid:              uid,
		Username:         username,
		Phone:            phone,
		Email:            email,
		TopID:            parent.TopID,
		TopName:          parent.TopName,
		GreatGrandID:     parent.GrandID,
		GreatGrandName:   parent.GrandName,
		GrandID:          parent.ParentID,
		GrandName:        parent.ParentName,
		ParentID:         parent.Uid,
		ParentName:       parent.Username,
		Password:         pwd,
		CreatedIp:        "",
		Vip:              0,
		Score:            0,
		CreatedAt:        createdAt,
		InviteCode:       myCode,
		Tester:           tester,
		PayPassword:      paypwd,
		Telegram:         "",
		CanBonus:         1,
		CanRebate:        1,
		Invited:          0,
		OperatorId:       "0",
		BusinessId:       "0",
		WinFlowMultiple:  "0",
		LoseFlowMultiple: "0",
	}
	if operatorId != "" {
		m.OperatorId = operatorId
	}
	if businessId != "" {
		m.BusinessId = businessId
	}
	if phone != "" {
		m.PhoneVerify = 1
		m.EmailVerify = 0
	}
	if email != "" {
		m.PhoneVerify = 0
		m.EmailVerify = 1
	}
	tx, err := meta.MerchantDB.Begin() // 开启事务
	if err != nil {
		return pushLog(err, helper.DBErr)
	}

	query, _, _ := dialect.Insert("tbl_member_base").Rows(&m).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	b := TblMemberBalance{
		Prefix:            prefix,
		Uid:               uid,
		Brl:               0,
		LockAmount:        0,
		UnlockAmount:      0,
		AgencyAmount:      0,
		DepositLockAmount: 0,
		AgencyLockAmount:  0,
	}
	query, _, _ = dialect.Insert("tbl_member_balance").Rows(&b).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	sign := PromoSignRecord{
		UID:       uid,
		Username:  username,
		Vip:       0,
		Sign1:     "0",
		Sign2:     "0",
		Sign3:     "0",
		Sign4:     "0",
		Sign5:     "0",
		Sign6:     "0",
		Sign7:     "0",
		Sign8:     "0",
		Sign9:     "0",
		Sign10:    "0",
		Sign11:    "0",
		Sign12:    "0",
		Sign13:    "0",
		Sign14:    "0",
		Sign15:    "0",
		Sign16:    "0",
		Sign17:    "0",
		Sign18:    "0",
		Sign19:    "0",
		Sign20:    "0",
		Sign21:    "0",
		Sign22:    "0",
		Sign23:    "0",
		Sign24:    "0",
		Sign25:    "0",
		Sign26:    "0",
		Sign27:    "0",
		Sign28:    "0",
		Sign29:    "0",
		Sign30:    "0",
		SignWeek1: "0",
		SignWeek2: "0",
		SignWeek3: "0",
		SignWeek4: "0",
		SignWeek5: "0",
		SignWeek6: "0",
		SignWeek7: "0",
	}
	query, _, _ = dialect.Insert("tbl_promo_sign_record").Rows(&sign).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	query = MemberClosureInsert(m.Uid, m.ParentID)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if parent.Uid != "" && parent.Uid != "0" {

		inviteRecord := tblPromoInviteRecord{
			Id:             helper.GenId(),
			Uid:            parent.Uid,
			Username:       parent.Username,
			Lvl:            1,
			ChildUid:       uid,
			ChildUsername:  username,
			FirstDepositAt: 0,
			DepositAmount:  0,
			BonusAmount:    0,
			CreatedAt:      createdAt,
			State:          1,
		}
		query, _, _ = dialect.Insert("tbl_promo_invite_record").Rows(&inviteRecord).ToSQL()

		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}
		pt := g.Record{
			"invite_register_num": g.L("invite_register_num+1"),
		}
		query, _, _ = dialect.Update("tbl_member_base").Set(pt).Where(g.Ex{"uid": parent.Uid}).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}

		ut := g.Record{
			"invited": 1,
		}
		update, _, _ := dialect.Update("tbl_member_base").Set(ut).Where(g.Ex{"uid": uid}).ToSQL()
		fmt.Println(update)
		_, err = tx.Exec(update)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}
		//查询后台转盘开关是否开启
		if TurntableSwitch() {
			// 邀请成功,赠送一次转盘机会
			info := g.Record{
				"unuse_chance": g.L(fmt.Sprintf("unuse_chance+%d", 1)),
			}
			update, _, _ = dialect.Update("tbl_pdd_turntable_info").Set(info).Where(g.Ex{"uid": parent.Uid}).ToSQL()
			fmt.Println(update)
			_, err = tx.Exec(update)
			if err != nil {
				_ = tx.Rollback()
				return pushLog(err, helper.DBErr)
			}
		}
	}

	//查询后台转盘开关是否开启
	if TurntableSwitch() {
		//注册成功插入转盘信息
		p := ryrpc.TblPddTurntableInfo{
			Uid:         uid,
			UnuseChance: 1,
			UsedChance:  0,
			Amount:      0,
			HandAmount:  0,
			TotAmount:   0,
			Enabled:     1,
			CreatedAt:   int64(int(createdAt)),
			UpdatedAt:   int64(int(createdAt)),
		}
		query, _, _ = dialect.Insert("tbl_pdd_turntable_info").Rows(&p).ToSQL()
		_, err = tx.Exec(query)
		if err != nil {
			_ = tx.Rollback()
			return pushLog(err, helper.DBErr)
		}
	}
	err = tx.Commit()

	err = BankcardInsert(username, uid, "PIX")
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

// 检测会员账号是否已存在
func MemberExist(username string) bool {

	key := "member:" + username
	exists := meta.MerchantRedis.Exists(ctx, key).Val()
	if exists == 0 {
		return false
	}

	return true
}

// shortURLGen uri 原始url,返回短url
func shortURLGen(uri string) (string, error) {

	//fmt.Println("uri:", uri)
	for i := 0; i < 100; i++ {

		shortCode := shortUrlGenCode()
		key := fmt.Sprintf("shortcode:%s", shortCode)

		err := meta.MerchantRedis.SetNX(ctx, key, uri, 0).Err()
		if err != nil {
			fmt.Println(err)
			continue
			//return "", errors.New(helper.RedisErr)
		}

		return shortCode, nil
	}
	return "", errors.New(helper.RedisErr)
}

// shortUrlGenCode 传入ID生成短码
func shortUrlGenCode() string {

	hd := hashids.NewData()
	hd.Salt = helper.GenId()
	hd.MinLength = 6
	h, _ := hashids.NewWithData(hd)
	shortCode, _ := h.EncodeInt64([]int64{time.Now().UnixMilli()})
	return shortCode
}

func MemberBindCheck(ex g.Ex) bool {

	var id string

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select("uid").Where(ex).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&id, query)
	if err == sql.ErrNoRows {
		return false
	}

	return true
}

func MemberClosureInsert(nodeID, targetID string) string {

	if targetID == "0" {
		targetID = nodeID
	}
	t := "SELECT ancestor, " + nodeID + ",lvl+1 FROM tbl_members_tree WHERE descendant = " + targetID + " UNION SELECT " + nodeID + "," + nodeID + ",0"
	query := "INSERT INTO tbl_members_tree (ancestor, descendant,lvl) (" + t + ")"

	return query
}

func MemberUpdateBonus(uid string, canBonus int) error {

	record := g.Record{
		"can_bonus": canBonus,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdateBloger(uid string, IsBloger int) error {

	record := g.Record{
		"is_bloger": IsBloger,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdatePassword(uid string, Password string) error {

	record := g.Record{
		"password": Password,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdateRebate(uid string, canRebate int) error {

	record := g.Record{
		"can_rebate": canRebate,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdateState(uid string, state int, remake string) error {

	record := g.Record{
		//"state":  state,
		//"remake": remake,
	}
	if state != 0 {
		record["state"] = state
	}
	if remake != "" {
		record["remake"] = remake
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdateAgentChannel(uid, cid string) error {

	member, err := MemberFindByUid(uid)
	if err != nil {
		return err
	}
	if member.Cid != "0" {
		return errors.New(helper.IsAgentUserAlready)
	}

	ac := TblOperatorInfo{}

	t := dialect.From("tbl_operator_info")
	query, _, _ := t.Select(colsAgentChannel...).Where(g.Ex{"id": cid}).Limit(1).ToSQL()
	err = meta.MerchantDB.Get(&ac, query)
	if ac.State != "0" {
		return errors.New(helper.ParentTenantNotExistErr)
	}

	record := g.Record{
		"cid": cid,
	}
	query, _, _ = dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

func MemberUpdateFlowMultiple(uid, winFlowMultiple, loseFlowMultiple string) error {

	record := g.Record{
		"win_flow_multiple":  winFlowMultiple,
		"lose_flow_multiple": loseFlowMultiple,
	}
	query, _, _ := dialect.Update("tbl_member_base").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err := meta.MerchantDB.Exec(query)
	if err != nil {
		return pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return nil
}

// 通过用户名获取用户在redis中的数据
func MemberCache(fctx *fasthttp.RequestCtx, name string) (ryrpc.TblMemberBase, error) {

	m := ryrpc.TblMemberBase{}
	if name == "" {
		value := fctx.UserValue("token")
		if value == nil {
			return m, errors.New(helper.UsernameErr)
		}
		name = string(value.([]byte))
		if name == "" {
			return m, errors.New(helper.UsernameErr)
		}
	}

	t := dialect.From("tbl_member_base")
	query, _, _ := t.Select(colsMember...).Where(g.Ex{"username": name}).Limit(1).ToSQL()
	err := meta.MerchantDB.Get(&m, query)
	if err != nil && err != sql.ErrNoRows {
		return m, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	if err == sql.ErrNoRows {
		return m, errors.New(helper.UsernameErr)
	}

	if m.InviteCode == "" {
		mycode, err := shortURLGen(m.Uid)
		if err == nil {
			// 更新会员信息
			query, _, _ = dialect.Update("tbl_member_base").Set(g.Record{"invite_code": mycode}).Where(g.Ex{"uid": m.Uid}).ToSQL()
			_, err = meta.MerchantDB.Exec(query)
		}
	}

	return m, nil
}

func MemberAddBonus(uid, username, amount string) error {
	if meta.WalletMode == "1" {
		return errors.New(helper.WalletTypeErr)
	}
	memBalance, err := MemberBalanceFindOne(uid)
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	addAmount, _ := decimal.NewFromString(amount)
	money := addAmount.Add(decimal.NewFromFloat(memBalance.AgencyLockAmount))
	record := g.Record{
		"brl":           g.L(fmt.Sprintf("brl+%s", money.String())),
		"agency_amount": g.L(fmt.Sprintf("agency_amount+%s", money.String())),
	}
	tx, err := meta.MerchantDB.Begin()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	query, _, _ := dialect.Update("tbl_member_balance").Set(&record).Where(g.Ex{"uid": uid}).ToSQL()
	fmt.Println(query)
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	// 获取钱包余额
	mb, err := MemberBalanceFindOne(uid)
	if err != nil {
		return err
	}
	var balanceAfter, balance decimal.Decimal
	balance = decimal.NewFromFloat(mb.Brl)
	balanceAfter = balance.Add(money)
	//4、新增账变记录
	trans := MemberTransaction{
		AfterAmount:  balanceAfter.String(),
		Amount:       money.String(),
		BeforeAmount: balance.String(),
		BillNo:       helper.GenId(),
		CreatedAt:    time.Now().UnixMilli(),
		ID:           helper.GenId(),
		CashType:     helper.TransactionAgencyAmount,
		UID:          uid,
		Username:     username,
	}

	query, _, _ = dialect.Insert("tbl_balance_transaction").Rows(trans).ToSQL()
	_, err = tx.Exec(query)
	if err != nil {
		_ = tx.Rollback()
		return pushLog(err, helper.DBErr)
	}

	err = tx.Commit()
	if err != nil {
		return pushLog(err, helper.DBErr)
	}
	return nil
}

func MemberAddReward(uid string, amount float64) error {
	//查询后台转盘开关是否开启
	if TurntableSwitch() {
		info, err := GetTurntableInfo(uid)
		if err != nil {
			return pushLog(err, helper.DBErr)
		}

		tx, err := meta.MerchantDB.Begin()
		if err != nil {
			return pushLog(err, helper.DBErr)
		}

		totAmount := info.Amount + info.HandAmount
		balAfter := totAmount + amount
		history := ryrpc.TblPddTurntableHistory{
			Id:           helper.GenId(),
			Uid:          uid,
			Amount:       amount,
			BeforeAmount: totAmount,
			AfterAmount:  balAfter,
			CreatedAt:    time.Now().Unix(),
			Type:         9,
			Remark:       "后台手动增加",
		}
		query, _, _ := dialect.Insert("tbl_pdd_turntable_history").Rows(history).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}

		//更新转盘信息表
		ex := g.Ex{
			"uid": uid,
		}
		record := g.Record{
			"hand_amount": info.HandAmount + amount,
			"updated_at":  time.Now().Unix(),
		}
		query, _, _ = dialect.Update("tbl_pdd_turntable_info").Set(record).Where(ex).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
			return err
		}

		_ = tx.Commit()
	}
	return nil
}

// GiveSpinChance 定时任务
// 1.注册开始计算 三天后将奖励字段清零，同时停止参加活动
// 2.每日赠送一次转盘机会
func GiveSpinChance() {
	//查询后台转盘开关是否开启
	if TurntableSwitch() {
		members, err := MemberFindByBusinessId("")
		if err != nil {
			fmt.Println(helper.DBErr)
		}
		if len(members) > 0 {
			for _, mb := range members {
				// 注册三天后清零
				if time.Since(time.Unix(int64(mb.CreatedAt), 0)).Hours()/24 >= 3 {
					info := g.Record{
						"amount":      0,
						"hand_amount": 0,
						"enabled":     0,
					}
					query, _, _ := dialect.Update("tbl_pdd_turntable_info").Set(info).Where(g.Ex{"uid": mb.Uid}).ToSQL()
					fmt.Println(query)
					_, err = meta.MerchantDB.Exec(query)
					if err != nil {
						fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
					}
				} else {
					// 活动有效期内,每日赠送一次转盘机会
					info := g.Record{
						"unuse_chance": g.L(fmt.Sprintf("unuse_chance+%d", 1)),
					}
					query, _, _ := dialect.Update("tbl_pdd_turntable_info").Set(info).Where(g.Ex{"uid": mb.Uid}).ToSQL()
					fmt.Println(query)
					_, err = meta.MerchantDB.Exec(query)
					if err != nil {
						fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
					}
				}
			}
		}
	}
}

// 查询后台转盘开关是否开启
func TurntableSwitch() bool {
	cfgVal := myConfig.GetGameCfgInt(myConfig.CfgTurntableSwitch)

	if cfgVal == 1 {
		return true
	} else {
		return false
	}
}

// /测试号
type MemberTestPage struct {
	T int64                 `json:"t"`
	D []ryrpc.TblMemberTest `json:"d"`
	S uint                  `json:"s"`
}

// 测试号列表
func MemberTestList(page, pageSize int, startTime, endTime, adminId string, ex g.Ex) (MemberTestPage, error) {

	data := MemberTestPage{}
	var err error
	if startTime != "" && endTime != "" {

		startAt, err := helper.TimeToLoc(startTime, loc)
		if err != nil {
			return data, errors.New(helper.TimeTypeErr)
		}

		endAt, err := helper.TimeToLoc(endTime, loc)
		if err != nil {
			return data, errors.New(helper.TimeTypeErr)
		}
		if startAt >= endAt {
			return data, errors.New(helper.QueryTimeRangeErr)
		}

		ex["created_at"] = g.Op{"between": exp.NewRangeVal(startAt, endAt)}
	}

	//验证是否是渠道登录，是的话查询该渠道的会员列表
	agentData := OperatorInfoData{}

	query, _, _ := dialect.From("tbl_operator_info").Select(g.COUNT("id")).Where(ex).ToSQL()
	fmt.Println(query)
	err = meta.MerchantDB.Get(&agentData.T, query)
	if err != nil && err != sql.ErrNoRows {
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	if agentData.T == 1 {
		ex["operator_id"] = adminId
	}

	t := dialect.From("tbl_member_test")
	if page == 1 {
		query, _, _ = t.Select(g.COUNT("uid")).Where(ex).ToSQL()
		err = meta.MerchantDB.Get(&data.T, query)
		if err != nil && err != sql.ErrNoRows {
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}
	offset := (page - 1) * pageSize
	var d []ryrpc.TblMemberTest
	query, _, _ = t.Select(colsMemberTest...).Where(ex).Order(g.I("create_at").Desc()).Offset(uint(offset)).Limit(uint(pageSize)).ToSQL()
	err = meta.MerchantDB.Select(&d, query)
	if err != nil {

		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}
	if len(d) == 0 {
		return data, nil
	}
	data.D = d
	data.S = uint(pageSize)

	return data, nil
}

// 测试号生成
func GetMembetTestMaxId(amount float64, total int, operator_id int) error {
	maxid := 0
	t := dialect.From("tbl_member_test")
	query, _, _ := t.Select(g.MAX("uid")).ToSQL()
	err := meta.MerchantDB.Get(&maxid, query)
	if err == sql.ErrNoRows {
		return err
	}
	fmt.Println(maxid)
	if maxid == 0 {
		maxid = 1000001
	}
	tx, err := meta.MerchantDB.Begin()
	for i := 0; i <= total; i++ {
		maxid = maxid + 1
		gameid := strconv.Itoa(maxid)
		timenow := time.Now().Format("2006-01-02 15:04:05")
		passwd := GetRandStr()
		passwd_md5 := MD5(passwd)
		member_test := ryrpc.TblMemberTest{
			Id:          0,
			Uid:         gameid,
			OperatorId:  operator_id,
			AccountName: gameid,
			Password:    passwd,
			CreateAt:    timenow,
		}
		query, _, _ := dialect.Insert("tbl_member_test").Rows(member_test).ToSQL()
		fmt.Println(query)
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
		}

		member_base := ryrpc.TblMemberBase{
			Prefix:           "",
			Uid:              gameid,
			Username:         "55" + gameid,
			Phone:            "55" + gameid,
			Email:            "",
			TopID:            "0",
			TopName:          "",
			GreatGrandID:     "0",
			GreatGrandName:   "",
			GrandID:          "0",
			GrandName:        "",
			ParentID:         "0",
			ParentName:       "",
			Password:         passwd_md5,
			Vip:              0,
			Score:            0,
			CreatedAt:        uint32(time.Now().Unix()),
			CreatedIp:        "127.0.0.1",
			InviteCode:       "",
			Tester:           2,
			PayPassword:      strconv.Itoa(123456),
			Telegram:         "",
			CanBonus:         1,
			CanRebate:        1,
			Invited:          0,
			OperatorId:       "0",
			BusinessId:       "0",
			WinFlowMultiple:  "0",
			LoseFlowMultiple: "0",
			Pid:              helper.GenId(),
		}
		query, _, _ = dialect.Insert("tbl_member_base").Rows(member_base).ToSQL()
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
		}
		//增加金额

		var member_balance = TblMemberBalance{
			Prefix:            "",
			Uid:               gameid,
			BrlAmount:         0,
			Brl:               amount,
			UnlockAmount:      0,
			LockAmount:        0,
			AgencyAmount:      0,
			DepositAmount:     0,
			DepositBalance:    0,
			DepositLockAmount: 0,
			AgencyBalance:     0,
			AgencyLockAmount:  0,
		}
		query, _, _ = dialect.Insert("tbl_member_balance").Rows(member_balance).ToSQL()
		_, err = tx.Exec(query)
		if err != nil {
			fmt.Println(fmt.Errorf("query : %s, error : %s", query, err))
			_ = tx.Rollback()
		}
	}
	_ = tx.Commit()
	return nil
}
func MD5(str string) string {
	data := []byte(str) //切片
	has := md5.Sum(data)
	md5str := fmt.Sprintf("%x", has) //将[]byte转成16进制
	return md5str
}

func GetRandStr() string {

	rand.Seed(time.Now().UnixNano())

	// 生成前三位随机字母
	randomLetters := make([]rune, 3)
	for i := range randomLetters {
		randomLetters[i] = rune('a') + rune(rand.Intn(int('z'-'a'+1)))
	}
	// 生成后三位随机数字
	randomDigits := rand.Intn(1000)
	// 组合成最终字符串
	randomString := fmt.Sprintf("%s%03d", string(randomLetters), randomDigits)
	return randomString
}

func GetMemberController(uid int, adminId string) (MemberControllerButton, error) {
	data := MemberControllerButton{}
	userInfo, err := MemberFindByUid(strconv.Itoa(int(uid)))
	if err != nil {
		return data, err
	}
	loginUser := GetLoginUser(adminId)
	if loginUser.Operator != "" {
		if userInfo.OperatorId != loginUser.Operator {
			return data, errors.New(helper.MethodNoPermission)
		}
	} else if loginUser.Businsess != "" {
		if userInfo.BusinessId != loginUser.Businsess {
			return data, errors.New(helper.MethodNoPermission)
		}
	}
	query := fmt.Sprintf("select * from tbl_member_controller_button where uid=%d", uid)
	fmt.Println(query)
	err = meta.MerchantDB.Get(&data, query)
	if err != nil {
		fmt.Println(err)
		return data, errors.New(helper.DBErr)
	}
	return data, nil
}

func UpdateMemberController(param MemberControllerButton, adminId string) error {
	userInfo, err := MemberFindByUid(strconv.Itoa(int(param.Uid)))
	if err != nil {
		return err
	}
	loginUser := GetLoginUser(adminId)
	if loginUser.Operator != "" {
		if userInfo.OperatorId != loginUser.Operator {
			return errors.New(helper.MethodNoPermission)
		}
	} else if loginUser.Businsess != "" {
		if userInfo.BusinessId != loginUser.Businsess {
			return errors.New(helper.MethodNoPermission)
		}
	}
	query, _, _ := dialect.Update("tbl_member_controller_button").Set(param).Where(g.Ex{"uid": param.Uid}).ToSQL()
	_, err = meta.MerchantDB.Exec(query)
	if err != nil {
		return errors.New(helper.DBErr)
	}
	return nil
}

func MemberLoginLog(param MemberLoginLogParam, adminid string) (MemberLoginLogRetData, error) {
	data := MemberLoginLogRetData{}
	where := "1=1"
	oderBy := "login_time"
	oderType := "desc"
	if param.Uid != 0 {
		where += " and a.uid=" + strconv.Itoa(param.Uid)
	}
	if param.LoginIp != "" {
		where += " and a.login_ip=" + param.LoginIp
	}
	loginUser := GetLoginUser(adminid)
	if loginUser.Operator != "" {
		where += " and c.id=" + loginUser.Operator
	}
	if loginUser.Businsess != "" {
		where += " and d.id=" + loginUser.Businsess
	}
	if param.OderBY != "" {
		oderBy = param.OderBY
	}
	if param.OderType != "" {
		oderType = param.OderType
	}
	join := " left join tbl_member_base as b on b.uid=a.uid"          //玩家表
	join += " left join tbl_operator_info as c on c.id=b.operator_id" //渠道表
	join += " left join tbl_business_info as d on d.id=b.business_id" //业务员表

	order := oderBy + " " + oderType

	offset := (param.Page - 1) * param.PageSize
	if param.Page == 1 {
		count := "select count(1) from tbl_member_login_log as a " + join + " where " + where
		fmt.Println(count)
		err := meta.MerchantDB.Get(&data.T, count)
		if err != nil {
			fmt.Println(err)
			return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), count), helper.DBErr)
		}

		if data.T == 0 {
			return data, nil
		}
	}
	field := "a.*," +
		"ifnull(b.username,'') username," +
		"ifnull(c.id,'') operator_id," +
		"ifnull(c.operator_name,'') operator_name," +
		"ifnull(d.id,'') business_id," +
		"ifnull(d.account_name,'') as business_name"
	query := "select " + field + " from tbl_member_login_log  as a " + join + " where " + where + " order by " + order + " limit " + strconv.Itoa(param.PageSize) + " offset " + strconv.Itoa(offset)
	fmt.Println(query)
	err := meta.MerchantDB.Select(&data.D, query)
	if err != nil && err != sql.ErrNoRows {
		fmt.Println(err)
		return data, pushLog(fmt.Errorf("%s,[%s]", err.Error(), query), helper.DBErr)
	}

	return data, nil
}
